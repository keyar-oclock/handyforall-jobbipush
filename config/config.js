var path = require('path');
var fs = require('fs');

var config = JSON.parse(fs.readFileSync(path.join(__dirname, "/config.json"), 'utf8'));

var CONFIG = {};
CONFIG.ENV = (process.env.NODE_ENV || 'development');
CONFIG.PORT = (process.env.VCAP_APP_PORT || config.port);
CONFIG.DB_URL = 'mongodb://' + config.mongodb.host + ':' + config.mongodb.port + '/' + config.mongodb.database;

CONFIG.DIRECTORY_USERS = config.directory.users;
CONFIG.DIRECTORY_TASKERS = config.directory.taskers;

CONFIG.USER_PROFILE_IMAGE_DEFAULT = 'uploads/default/user.jpg';
CONFIG.USER_PROFILE_IMAGE = 'uploads/images/users/';
CONFIG.CATEGORY_DEFAULT_IMAGE = 'uploads/default/category.jpg';

CONFIG.SECRET_KEY = '16f198404de4bb7b994f16b84e30f14f';

CONFIG.GCM_KEY = '';
CONFIG.GOOGLE_MAP_API_KEY = 'AIzaSyC5YIg8-Yk_zqjzWpFyZrgYuzzjTCBJV7k';

CONFIG.APNS = {};
CONFIG.APNS.MODE = true; // Production = true or Development = false
CONFIG.APNS.BUNDLE_ID_USER = 'com.casperon.quickrabitUser';
CONFIG.APNS.BUNDLE_ID_TASKER = 'com.casperon.quickrabbitpartner';
CONFIG.APNS.CERT_TASKER = path.join(__dirname, "/apns/handypartnerdist.pem");
CONFIG.APNS.CERT_USER = path.join(__dirname, "/apns/handyuserDist.pem");
CONFIG.APNS.KEY = path.join(__dirname, "/apns/handyuserDist.pem");

CONFIG.SOCIAL_NETWORKS = {
    'facebookAuth': {
        'clientID': '1699671247014568',
        'clientSecret': 'ca21cf003efc524a7ffb8af9c380f547',
        'callbackURL': 'http://maidac.casperon.co/auth/facebook/callback'
    },
};

//Export Module
module.exports = CONFIG;
