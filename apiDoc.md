# API Documentation for Mobile App

## Employer

### Registration

```
- POST /api/siteregister
	(Need to add `Content-Type` to the header)
	```
	Content-Type : application/x-www-form-urlencoded
	```
	* Params
		-username(required & unique)
		-firstname(required)
		-lastname(required)
		-email(required & unique)
		-pwd(required)
		-confirm_pwd(required)
		-role(value is `user`)(required)
		-today(Current Date, Format is 2017-01-30 20:00:00)
		-location[lng]
		-location[lat]
		-address[city](required)
		-address[line1]
		-address[line2]
		-address[state]
		-address[country]
		-address[zipcode]
		-phone[code](required & unique)
		-phone[number](required)
	* Response
		- if has missing fields
			[
				{
					"param": "username",
					"msg": " Username is required"
				}
			]
		- if email or username or phonenumber exists
			Email Id Or User name already exists
		- if failure
			That email or username is already taken.
		- if success
			{
				"user": "test",
				"email": "test@test.com",
				"user_id": "588f9183426529f8080b6975",
				"token": "",
				"user_type": "user",
				"status": 1
			}
```

## Employee

### Registration

```
- POST /api/taskerregister
	Need to send the type as JSON type.
	* Request
	```
		{
			"gender":"male",
			"birthdate":{"year":1980,"month":12,"date":12},
			"working_days":[
				{
					"day":"Sunday",
					"hour":{"morning":true,"afternoon":false,"evening":false},
					"not_working":false
				},
				{
					"day":"Monday",
					"hour":{"morning":false,"afternoon":false,"evening":true},
					"not_working":false
				}
			],
			"avatar":{},
			"next":"step8",
			"taskerskills":[
				{
					"categoryid":"5800d4b9b264e42c0fe70ba6",
					"childid":"5800e0acc38fa2a812e0569f",
					"terms":false,
					"quick_pitch":"test",
					"hour_rate":123,
					"experience":"57c669d74e820c780cf78105",
					"file":"",
					"skills":[]
				}
			],
			"Map":[],
			"vehicle":[],
			"name":{
				"first_name":"test",
				"last_name":"test"
			},
			"username":"test",
			"email":"test@test.com",
			"phone":{
				"code":"+1",
				"number":"2015555555"
			},
			"pwd":"Test123",
			"confirm_pwd":"Test123",
			"address":{
				"address":{
					"line1":"Research Boulevard, Austin, TX, United States"
				},
				"line1":"Research Boulevard",
				"line2":"",
				"city":"Austin",
				"state":"Texas",
				"country":"United States",
				"zipcode":"15460"
			},
			"tasker_area":{
				"lng":-97.7466834,
				"lat":30.415755
			},
			"location":{
				"lng":-97.7466834,
				"lat":30.415755
			},
			"radius":12,
			"taskerfile":[],
			"files":[],
			"ethnicity":"Caucasian",
			"disability":"Disabled veteran",
			"disability_reason":"",
			"lift_fifty":true,
			"work_in_us":false
		}
	```
	* Response
		- if has missing fields
			[
				{
					"param": "username",
					"msg": " Username is required"
				}
			]
		- if email or username or phonenumber exists
			exists
		- if failure
			Unable save your data
		- if success
			{
				"updatedAt": "2017-02-01T12:59:25.267Z",
				"createdAt": "2017-02-01T12:59:25.267Z",
				"username": "test",
				"email": "test@test.com",
				"avatar": "",
				"password": "$2a$08$xZP6I6Bm/iutwjN7eWcxne7Vz.Pt37MTImXK2CkXSvvWEMrJhep9q",
				"role": "tasker",
				"_id": "5891dbad67a53b08123aa09f",
				"availability": 1,
				"working_days": [],
				"taskerskills": [],
				"profile_details": [],
				"shipping_address": [],
				"geo": [],
				"tasker_status": 1,
				"status": 3,
				"verification_code": [],
				"refer_history": [],
				"activity": {
					"last_active_time": "2017-02-01T12:59:25.258Z",
					"last_logout": "2017-02-01T12:59:25.258Z",
					"last_login": "2017-02-01T12:59:25.257Z"
				},
				"addressList": [],
				"total_review": 0,
				"avg_review": 0,
				"phone": {
					"code": "1",
					"number": "1"
				}
			}
```

### Upload Driver License

```
- POST /api/taskerimprove
	Need to send the type as JSON type.
	* Request
	```
		{
			"username":"test",
			"driver_license":file
		}
	```
	* Response
		- if failure
			Unable save your data
		- if success
			{
				_id" : ObjectId("58fe6b417f6b1ca013772e02"),
			    "updatedAt" : ISODate("2017-04-24T21:17:26.680Z"),
			    "createdAt" : ISODate("2017-04-24T21:16:49.728Z"),
			    "gender" : "male",
			    "avatar" : "",
			    "ethnicity" : "African American",
			    "disability" : "Disabled veteran",
			    "disability_reason" : "",
			    "lift_fifty" : true,
			    "work_in_us" : false,
			    "username" : "test123q",
			    "email" : "test123qq@mail.com",
			    "radius" : 123,
			    "password" : "$2a$08$wHB3eI.gAtcEa0rR2cNqeuy/ujaHFcIuK6wrrSC6S1sc4lGxLrOiG",
			    "role" : "tasker",
			    "availability" : 1,
			    "working_days" : [ 
			        {
			            "day" : "Sunday",
			            "hour" : {
			                "evening" : true,
			                "afternoon" : false,
			                "morning" : false
			            }
			        }
			    ],
			    "taskerskills" : [ 
			        {
			            "categoryid" : ObjectId("5800d4b9b264e42c0fe70ba6"),
			            "childid" : ObjectId("5800e0acc38fa2a812e0569f"),
			            "terms" : false,
			            "quick_pitch" : "test",
			            "hour_rate" : 122,
			            "experience" : ObjectId("57c669d74e820c780cf78105"),
			            "status" : 1
			        }
			    ],
			    "profile_details" : [],
			    "birthdate" : {
			        "year" : 1987,
			        "month" : 12,
			        "date" : 12
			    },
			    "shipping_address" : [],
			    "geo" : [],
			    "tasker_status" : 1,
			    "status" : 3,
			    "verification_code" : [],
			    "refer_history" : [],
			    "activity" : {
			        "last_active_time" : ISODate("2017-04-24T21:16:49.720Z"),
			        "last_logout" : ISODate("2017-04-24T21:16:49.720Z"),
			        "last_login" : ISODate("2017-04-24T21:16:49.720Z")
			    },
			    "addressList" : [],
			    "address":{
					"line1":"Research Boulevard",
					"line2":"",
					"city":"Austin",
					"state":"Texas",
					"country":"United States",
					"zipcode":"15460"
				},
				"tasker_area":{
					"lng":-97.7466834,
					"lat":30.415755
				},
				"location":{
					"lng":-97.7466834,
					"lat":30.415755
				},
			    "total_review" : 0,
			    "avg_review" : 0,
			    "name" : {
			        "first_name" : "test",
			        "last_name" : "test"
			    },
			    "phone" : {
			        "code" : "+1",
			        "number" : "2015555555"
			    },
			    "driver_license" : "uploads/images/driver_license/Beauty-of-nature-random-4884759-1280-800.jpg"
			}
```

### Update Availability(Working Days, Location and Radius)

```
- POST /mobile/provider/availability/save
	Need to send the type as JSON type.
	* Request
	```
		{
			"_id": "58a37da655fa005e1f9dbe1a",
			"working_days":[
				{
					"day":"Sunday",
					"hour":{"morning":true,"afternoon":false,"evening":false},
					"not_working":false
				},
				{
					"day":"Monday",
					"hour":{"morning":false,"afternoon":false,"evening":true},
					"not_working":false
				}
			],
			"location":{
				"lng":-97.7466834,
				"lat":30.415755
			},
			"radius":12
		}
	```
	* Response
		- if failure
			Unable save your data
		- if success
			{
				"ok": 1,
				"nModified": 1,
				"n": 1
			}
```

### Get Availability

```
- POST /mobile/provider/availability/get
	Need to send the type as JSON type.
	* Request
	```
		{
			"_id": "58a37da655fa005e1f9dbe1a"
		}
	```
	* Response
		- if failure
			Unable get your data
		- if success
			{
				"working_days": [
					{
						"day": "Sunday",
						"hour": {
							"evening": false,
							"afternoon": false,
							"morning": true
						}
					},
					{
						"day": "Monday",
						"hour": {
							"evening": true,
							"afternoon": false,
							"morning": false
						}
					}
				],
				"location": {
					"lat": 30.415755,
					"lng": -97.7466834
				},
				"radius": 12
			}
```


## Common

### Checking for User
(Need to add `Content-Type` to the header)
```
Content-Type : application/x-www-form-urlencoded
```
```
- POST /api/checkInfo
	* Params
		-email(required)
		-username(required)
		-phone[code](required & unique)
		-phone[number](required)
		-type(`user` or `tasker`)(required)
	* Response
		- if the Info exists in db
			{
				"success": "true",
				"message": "Info Exists"
			}
		- if the Info doesn't exist in db
			{
				"success": "true",
				"message": "No Exists"
			}
```

### Get Categories

```
- GET /site/category/getCategoryList
	* Params
	* Response
		[
			{
				"_id": "5800d4b9b264e42c0fe70ba6",
				"name": "Cleaning",
				"slug": "cleaning",
				"skills": [],
				"category": [
					{
						"_id": "5800e140c38fa2a812e056a1",
						"updatedAt": "2016-10-14T13:44:32.734Z",
						"createdAt": "2016-10-14T13:44:32.734Z",
						"name": "Deep Home Cleaning",
						"slug": "Deep Home Cleaning",
						"parent": "5800d4b9b264e42c0fe70ba6",
						"commision": 25,
						"status": 1,
						"image": "uploads/images/category/12.jpg",
						"img_name": "12.jpg",
						"img_path": "uploads/images/category/",
						"seo": {
						"keyword": "Deep Home Cleaning",
						"description": "Deep Home Cleaning",
						"title": "Deep Home Cleaning"
					},
					"skills": []
					}
				]
			}
		]
```

### Get Default Currency

```
- GET /site/landing/getDefaultCurrency
	* Params
		-name(undefined)
	* Response
		[
			{
				"_id": "57356148c7b8d55c0753143c",
				"updatedAt": "2016-12-30T05:12:46.460Z",
				"createdAt": "2016-05-13T05:08:24.318Z",
				"name": "Dollar",
				"code": "USD",
				"symbol": "$",
				"value": "1",
				"featured": "0",
				"status": 1,
				"default_currency": "0",
				"default": 1
			}
		]
```

### Get Default Language

```
- GET /site/landing/getDefaultLanguage
	* Params
		-name(undefined)
	* Response
		[
			{
				"_id": "5735881a57a1b1100f988dcb",
				"updatedAt": "2016-11-15T12:38:41.346Z",
				"createdAt": "2016-05-13T07:54:02.388Z",
				"name": "English",
				"code": "en",
				"status": 1,
				"default": 1
			}
		]
```

### Get Experiences

```
- POST /site/account/categories/get-experience
	* Params
	* Response
		[
			{
				"_id": "57c669d74e820c780cf78105",
				"updatedAt": "2016-10-14T12:51:18.613Z",
				"createdAt": "2016-08-31T05:23:35.177Z",
				"name": "Fresher",
				"status": 1
			},
			{
				"_id": "57d26b23bed0d96c026e5517",
				"updatedAt": "2016-09-09T07:56:19.878Z",
				"createdAt": "2016-09-09T07:56:19.878Z",
				"name": "Mid level",
				"status": 1
			}
		]
```
