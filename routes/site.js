"use strict";

/** Dependency Injection */

var express = require('express');
var path = require('path');
var multer = require('multer');
var jwt = require('jsonwebtoken');
var middlewares = require('../model/middlewares.js');
var dynamicTime = '';
var fs = require('fs');
/*
function newAuth(req, res, next) {
    var token = req.headers.authorization;
    if (token) {
        jwt.verify(token, 'token_with_username_and_password', function (err, decoded) {
            if (err) {
                return res.send('wrong');
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        res.send('wrong');
    }
}
*/

module.exports = function (app, io) {
    try {
        /** ROUTERS for site **/
        var siteUsers = require('../controller/site/users.js')(app);
        var landing = require('../controller/site/landing.js')(app);
        var category = require('../controller/site/category.js')(app);
        var pages = require('../controller/site/pages.js')(app);
        var task = require('../controller/site/task.js')(io, app);
        var account = require('../controller/site/account.js')(io);
        var remita = require('../controller/site/remita.js')(io);
        var dashboard = require('../controller/site/dashboard.js')(app);
        var messages = require('../controller/site/messages.js')(app);
        var notifications = require('../controller/site/notifications.js')(app);
        var contactus = require('../controller/site/contactus.js')(app);

        app.get('/site/users/getusers', siteUsers.getusers);
        //app.post('/site/users/LoginUser', newAuth, siteUsers.validationUser, siteUsers.LoginUser);
        app.post('/site/users/checkEmail', siteUsers.checkEmail);
        app.post('/site/users/addnewuser', siteUsers.validationLoginUser, siteUsers.addnewuser);
        app.post('/site/users/currentUser', siteUsers.currentUser);
        app.post('/site/users/currentTasker', siteUsers.currentTasker);
        app.post('/site/users/save', siteUsers.save);
        app.post('/site/users/edit', siteUsers.edit);
        app.get('/site/users/allUsers', siteUsers.allUsers);
        app.post('/site/users/delete', siteUsers.delete);
        app.post('/site/users/changePassword', siteUsers.changePassword);
        app.post('/site/users/checkreferal', siteUsers.checkreferal);
        app.post('/site/users/facebooksiteregister', siteUsers.facebooksiteregister);
        app.post('/site/users/checkemail', siteUsers.checkemail);
        app.post('/site/users/checktaskeremail', siteUsers.checktaskeremail);
        app.post('/site/users/taskerRegister', middlewares.commonUpload('./uploads/images/tasker/').single('avatars'), siteUsers.taskerRegister);
        app.post('/site/users/taskerImprove', middlewares.commonUpload('./uploads/images/driver_license/').single('driver_license'), siteUsers.taskerImprove);

        /* Slider page*/
        app.get('/slider/list', landing.list);
        app.post('/site/main', landing.getMainData);

        /** Landing Page  **/
        app.post('/site/landing/landingdata', landing.getlandingdata);
        app.post('/site/landing/getmorecategory', landing.getmorecategory);
        app.post('/site/landing/search-suggestions', landing.searchSuggestions);
        app.post('/site/landing/search-childSuggestions', landing.childSuggestions);
        app.post('/site/landing/subscription', landing.subscription);
        app.get('/site/landing/getLanguage', landing.getLanguage);
        app.get('/site/landing/getBgimage', landing.getBgimage);
        app.get('/site/landing/gettaskersignupimage', landing.gettaskersignupimage);
        app.get('/site/landing/getsetting', landing.getSetting);
        app.get('/site/landing/getDefaultLanguage', landing.getDefaultLanguage);
        app.get('/site/landing/getSocialNetworks', landing.getSocialNetworks);
        app.get('/site/landing/getDefaultCurrency', landing.getDefaultCurrency);
        app.get('/site/landing/getCurrency', landing.getCurrency);
        app.get('/site/landing/getseosetting', landing.getseosetting);
        app.get('/site/landing/getwidgets', landing.getwidgets);

        app.post('/site/category/getcategory', category.getsubcategory);
        app.get('/site/category/getCategoryList', category.getcategorylist);
        app.post('/site/category/getsubcategory', category.getsubcategoryfordropdown);

        /** Footer Page**/
        app.post('/site/pages/getpage', pages.getpage);
        app.get('/site/faq/getfaq', pages.getfaq);

        /** Dashboard Page  **/
        app.post('/site/dashboard/dashboarddata', dashboard.dashboarddata);

        /** Task Step Page  **/
        app.post('/site/task/taskbaseinfo', task.taskbaseinfo);
        app.get('/site/task/taskeravailabilitybyWorkingArea', task.taskerAvailabilitybyWorkingArea);
        app.get('/site/task/taskerAvailabilitybyWorkingAreaCount', task.taskerAvailabilitybyWorkingAreaCount);
        app.post('/site/task/gettaskuser', task.gettaskuser);
        app.post('/site/task/taskprofileinfo', task.taskprofileinfo);
        app.post('/site/task/taskerreviews', task.taskerreviews);
        app.post('/site/task/taskerprofile', task.taskerprofile);
        app.post('/site/task/search-tasker', task.searchTasker);
        app.post('/site/task/addnewtask', task.addnewtask);
        app.post('/site/task/gettaskdetailsbyid', task.gettaskdetailsbyid);
        app.post('/site/task/confirmtask', task.confirmtask)
        app.post('/site/task/deleteaddress', task.deleteaddress);
        app.post('/site/task/addaddress', task.addaddress);
        app.post('/site/task/getaddressdata', task.getaddressdata);
        app.post('/site/task/getuserdata', task.getuserdata);
        app.post('/site/task/addressStatus', task.addressStatus);
        app.get('/site/task/taskerCount', task.taskerCount);

        /** Account Page  **/
        app.post('/site/account/settings/save', middlewares.commonUpload('uploads/images/users/').single('avatar'), account.saveAccount);
        app.post('/site/account/password/save', account.savePassword);
        app.post('/site/account/availability/save', account.saveAvailability);
        app.post('/site/account/availability/update', account.updateAvailability);
        app.post('/site/account/updatetaskstatus', account.updatetaskstatus);
        app.post('/site/account/categories/get', account.getCategories);
        app.post('/site/account/categories/getchild', account.getchild);
        app.post('/site/account/categories/get-experience', account.getExperience);
        //app.post('/site/account/updateStripeError', account.updateStripeError);

        app.post('/site/account/getwalletdetails', account.getwalletdetails);
        app.post('/site/account/paybywallet', account.paybywallet);
        app.post('/site/account/couponCompletePayment', account.couponCompletePayment);
        app.get('/site/account/question/getQuestion', account.getQuestion);
        app.post('/site/account/getsettings', account.getsettings);
        //app.post('/site/account/getstripe', account.getstripe);
        app.post('/site/account/updateprofiledetails', account.updateprofiledetails);
        app.post('/site/account/getTaskList', account.getTaskList);
        app.post('/site/account/getTaskDetailsByStaus', account.getTaskDetailsByStaus);
        app.post('/site/account/getUserTaskDetailsByStaus', account.getUserTaskDetailsByStaus);
        app.post('/site/account/getTaskDetailsBytaskid', account.getTaskDetailsBytaskid);
        app.post('/site/account/getcategoriesofuser', account.getusercategories);
        app.post('/site/account/updateTask', account.updateTask);
        app.post('/site/account/updateTaskcompletion', account.updateTaskcompletion);
        app.post('/site/account/insertaskerReview', account.insertaskerReview);
        app.post('/site/account/transcationhis', account.transcationhis);
        app.post('/site/account/usertranscation', account.usertranscation);
        app.post('/site/account/gettaskreview', account.gettaskreview);
        app.post('/site/account/edit', account.edit);
        app.post('/site/account/taskinfo', account.taskinfo);
        app.post('/site/account/gettaskbyid', account.gettaskbyid);
        app.post('/site/account/confirmtask', account.confirmtask);

        app.post('/site/account/paypalPayment', account.paypalPayment);
        app.post('/site/account/paymentmode', account.paymentmode);
        app.get('/site/account/paypal-execute', account.paypalExecute);
        app.get('/site/account/downloadPdf', account.downloadPdf);
        app.get('/site/account/userdownloadPdf', account.userdownloadPdf);

        app.post('/site/account/taskerconfirmtask', account.taskerconfirmtask);
        app.post('/site/account/apply-coupon', account.applyCoupon);
        app.post('/site/account/remove-coupon', account.removeCoupon);
        app.post('/site/account/getcancelreason', account.getcancelreason);



        app.post('/site/account/disputeupdateTask', account.disputeupdateTask);

        /** Account Page Tasker  **/
        app.post('/site/account/tasker/settings/save', middlewares.commonUpload('uploads/images/account/').single('avatar'), account.saveTaskerAccount);
        app.post('/site/account/tasker/question/save', account.saveTaskerQuestion);
        app.post('/site/account/tasker/document/save', middlewares.commonUpload('uploads/images/driver_license/').single('driver_license'), account.saveTaskerDocument);
        app.post('/site/account/tasker/password/save', account.saveTaskerPassword);
        app.post('/site/account/deactivateTaskertAccount', account.deactivateTasker);



        app.post('/site/account/usercanceltask', account.usercanceltask);
        app.post('/site/account/ignoreTask', account.ignoreTask);
        app.post('/site/account/updatecategoryinfo', middlewares.commonUpload('./uploads/images/general/').single('file'), account.updateCategory);
        app.post('/site/account/deleteCategory', account.deleteCategory);
        app.post('/site/account/deactivateAccount', account.deactivate);
        app.post('/site/account/getReview', account.getReview);
        app.post('/site/account/getuserReview', account.getuserReview);
        app.post('/site/account/addReview', account.addReview);
        app.post('/site/account/getTaskDetails', account.getTaskDetails);

        app.post('/site/account/saveaccountinfo', account.saveaccountinfo);


        /*  app.post('/site/account/updatewalletdata', remita.updatewalletdata);
          app.post('/site/account/remitaPayment', remita.remitaPayment);
          app.get('/site/account/samplereceipt', remita.samplereceipt);
          app.get('/site/account/payementwallet', remita.payementwallet);*/

        app.post('/site/account/updatewalletdata', account.updatewalletdata);
        //app.post('/site/account/remitaPayment', account.remitaPayment);
        //app.get('/site/account/samplereceipt', account.samplereceipt);
        // app.get('/site/account/payementwallet', account.payementwallet);
        /** Messages Page **/

        app.post('/site/chat/save', messages.save);
        app.post('/site/chat/getmessage', messages.getmessage);
        app.post('/site/chat/unreadmsg', messages.unreadmsg);
        app.post('/site/chat/deleteConversation', messages.deleteConversation);
        app.post('/site/chat/chathistory', messages.chathistory);
        app.post('/site/chat/msgcount', messages.msgcount);
        app.post('/site/contact/savecontactusmessage', contactus.save);
        app.post('/site/saveforgotpasswordinfo', account.saveforgotpasswordinfo);
        app.post('/site/saveforgotpwduser', account.saveforgotpassworduser);
        app.post('/site/forgotpwdmailuser', account.saveforgotpwdusermail);
        app.post('/site/forgotpwdmailtasker', account.saveforgotpwdtaskermail);
        app.post('/site/notifications/count', notifications.getCount);
        app.post('/site/notifications/list', notifications.getList);

    } catch (e) {
        console.log('error in site.js---------->>>>', e);
    }
};
