var assert = require('chai').assert;
var nodemailer = require('nodemailer');

describe('Library', function () {

	describe('Mail', function () {
		it('send mail', function (done) {
			var data = {
				from: 'sankar@teamtweaks.com',
				to: 'sankar@teamtweaks.com',
				subject: 'Hi',
				text: 'Hello',
				html: 'Hello'
			};

			var smtp_host = 'smtp.gmail.com';
			var smtp_port = '465';
			var smtp_username = '';
			var smtp_password = '';

			var transporter = nodemailer.createTransport({
				host: smtp_host,
				port: smtp_port,
				secure: true, // use SSL
				auth: {
					user: smtp_username,
					pass: smtp_password
				}
			});
			transporter.sendMail(data, function (error, info) {
				done();
			});
		});
	});
});