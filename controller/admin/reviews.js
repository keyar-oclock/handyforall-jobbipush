"use strict";
module.exports = function () {

    var db = require('../../controller/adaptor/mongodb.js')

    var controller = {};

    controller.list = function (req, res) {
        var errors = req.validationErrors();
        if (errors) {
            res.send(errors, 400);
            return;
        }

        if (req.body.sort) {
            var sorted = req.body.sort.field;
        }


        var usersQuery = [
            { "$match": { status: { $ne: 0 } } },
            { "$lookup": { from: "users", localField: "user", foreignField: "_id", as: "user" } },
            { "$unwind": "$user" },
            { "$lookup": { from: "tasker", localField: "tasker", foreignField: "_id", as: "tasker" } },
            { "$unwind": "$tasker" },
            { "$lookup": { from: "task", localField: "task", foreignField: "_id", as: "task" } },
            {
                "$project": {
                    rating: 1, user: 1, task: 1, tasker: 1, type: 1, booking_id: 1, usertasker:
                    {
                        $cond: { if: { $eq: ["$type", 'user'] }, then: '$user', else: '$tasker' }
                    }
                }
            },
            { "$project": { document: "$$ROOT" } },
            { "$group": { "_id": null, "count": { "$sum": 1 }, "documentData": { $push: "$document" } } }
        ];
        var sorting = {};
        var searchs = '';


        var condition = { status: { $ne: 0 } };
        usersQuery.push({ $unwind: { path: "$documentData", preserveNullAndEmptyArrays: true } });

        if (req.body.search) {

            //condition['user.username'] = { $regex: new RegExp('^' + req.body.search, 'i') };
            var searchs = req.body.search;
            usersQuery.push({
                "$match": {
                    $or: [
                        { "documentData.usertasker.username": { $regex: searchs + '.*', $options: 'si' } },
                        { "documentData.task.booking_id": { $regex: searchs + '.*', $options: 'si' } },
                        { "documentData.type": { $regex: searchs + '.*', $options: 'si' } }
                    ]
                }

            });

        }

        var sorting = {};
        if (req.body.sort) {
            var sorter = 'documentData.' + req.body.sort.field;
            sorting[sorter] = req.body.sort.order;
            usersQuery.push({ $sort: sorting });
        } else {
            sorting["documentData.createdAt"] = -1;
            usersQuery.push({ $sort: sorting });
        }

        if ((req.body.limit && req.body.skip >= 0) && !req.body.search) {
            usersQuery.push({ '$skip': parseInt(req.body.skip) }, { '$limit': parseInt(req.body.limit) });
        }
        usersQuery.push({ $group: { "_id": null, "count": { "$first": "$count" }, "documentData": { $push: "$documentData" } } });

        db.GetAggregation('review', usersQuery, function (err, docdata) {
            if (err || docdata.length <= 0) {
                res.send([0, 0]);
            } else {
                res.send([docdata[0].documentData, docdata[0].count]);
            }
        });
    }

    controller.edit = function (req, res) {
        var options = {};
        options.populate = 'tasker user task category';
        var errors = req.validationErrors();
        if (errors) {
            res.send(errors, 400);
            return;
        }
        db.GetDocument('review', { _id: req.body.id }, {}, options, function (err, data) {

            if (err) {
                res.send(err);
            } else {
                res.send(data);
            }
        });
    }

    controller.save = function (req, res) {


        var data = {};
        data.task = req.body.task;
        data.user = req.body.user;
        data.tasker = req.body.tasker;
        data.rating = req.body.rating;
        data.comments = req.body.comments;

        if (req.body._id) {
            db.UpdateDocument('review', { _id: { $in: req.body._id } }, data, function (err, result) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(result);
                }
            });
        } else {
            db.InsertDocument('review', data, function (err, result) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(result);
                }
            });
        }
    }

    controller.deletereviews = function (req, res) {
        req.checkBody('delData', 'Invalid delData').notEmpty();
        var errors = req.validationErrors();
        if (errors) {
            res.send(errors, 400);
            return;
        }

        db.UpdateDocument('review', { _id: { $in: req.body.delData } }, { status: 0 }, { multi: true }, function (err, docdata) {
            if (err) {
                res.send(err);
            } else {
                res.send(docdata);
            }
        });
    }
    return controller;
}
