"use strict";

module.exports = function() {
	var mongoose				= require('mongoose');
	var db 							= require('../../controller/adaptor/mongodb.js');
	var attachment			= require('../../model/attachments.js');
  var Jimp 						= require("jimp");

  var controller = {};
   controller.save = function(req,res) {
        req.checkBody('status', 'Invalid status').notEmpty();
        req.checkBody('name', 'Invalid name').notEmpty();
        req.checkBody('slug', 'Invalid slug').notEmpty();
        req.checkBody('seo.title', 'Invalid title').notEmpty();
        req.checkBody('seo.keyword', 'Invalid keyword').notEmpty();
        //req.checkBody('seo.description', 'Invalid description').notEmpty();

       if(req.body.parent){
           req.checkBody('commision', 'Invalid Commision').notEmpty();
       }


        var errors = req.validationErrors();
        if (errors) {
            res.send(errors);
            return;
        }


   if(req.file) {
        req.body.image = attachment.get_attachment(req.file.destination, req.file.filename);
            req.body.img_name = encodeURI(req.file.filename);
            req.body.img_path = req.file.destination.substring(2);
            Jimp.read(req.file.path).then(function (lenna) {
                lenna.resize(Jimp.AUTO,150)            // resize
                    .quality(100)                 // set JPEG quality
                    .write('./uploads/images/category/thumbs/'+req.file.filename); // save
            }).catch(function (err) {
            });
    }


		if(req.body._id) {
            db.UpdateDocument('category',{_id:req.body._id}, req.body,{multi:true},function(err,docdata){
                if(err){
                    res.send(err);
                }else {
                    res.send(docdata);
                }
            });
		}else {
			db.InsertDocument('category', req.body, function(err,result){
				if(err){
					res.send(err);
				}else {
					res.send(result);
				}
			});
		}
	}

  controller.edit = function(req,res){
		db.GetOneDocument('category', {_id:req.body.id}, {}, {}, function(err,docdata){
			if(err){
				res.send(err);
			}else {
				res.send(docdata);
			}
		});
	};


	  controller.getSetting = function(req,res){
			db.GetDocument('settings', {alias: 'general'}, {}, {}, function(err,docdata){
				if(err){
					res.send(err);
				}else {
					res.send(docdata);
				}
			});
		};

	controller.list = function(req,res){
      if(req.query.sort!=""){
            var sorted = req.query.sort;
        }
        var categoryQuery = [{
            "$match":{status: { $ne: 0 }, parent: { $exists: false } }
        },{
            $project: {
                name: 1,
                image:1,
                status:1,
                dname:{$toLower: '$'+sorted}
            }
        },{
            $project: {
                name: 1,
                document: "$$ROOT"
            }
        },{
            $group:{"_id":null,"count":{"$sum":1},"documentData":{$push:"$document"}}
        }];

        var sorting = {};
        var searchs = '';

        var condition = {status: { $ne: 0 }};

        if(Object.keys(req.query).length!=0) {
            categoryQuery.push({$unwind: { path: "$documentData", preserveNullAndEmptyArrays: true }});

            if (req.query.search != '' && req.query.search != 'undefined' && req.query.search) {
                condition['name'] = {$regex: new RegExp('^' + req.query.search, 'i')};
                searchs = req.query.search;
                categoryQuery.push({ "$match":{"documentData.name":{$regex:  searchs+'.*' ,$options: 'si'}}});
            }
            if (req.query.sort !== '' && req.query.sort) {
                sorting = {};
                if (req.query.status == 'false') {
                    sorting["documentData.dname"] = -1;
                    categoryQuery.push({ $sort : sorting });
                } else {
                    sorting["documentData.dname"] = 1;
                    categoryQuery.push({ $sort : sorting });
                }
            }
            if (req.query.limit != 'undefined' && req.query.skip != 'undefined') {
                categoryQuery.push({'$skip':parseInt(req.query.skip)},{'$limit':parseInt(req.query.limit)});
            }
            categoryQuery.push({$group:{"_id":null,"count":{"$first":"$count"},"documentData":{$push:"$documentData"}}});
        }


        db.GetAggregation('category',categoryQuery, function (err,docdata){
            if(err){
                res.send(err);
            }else {
                if(docdata.length!=0){
                    res.send([docdata[0].documentData,docdata[0].count]);
                }else{
                    res.send([0,0]);
                }
            }
        });
}

controller.subcategorylist = function(req,res){
      if(req.query.sort!=""){
            var sorted = req.query.sort;
        }
        var categoryQuery = [{
            "$match":{status: { $ne: 0 }, parent: { $exists: true } }
        },
        {$lookup:{from: 'categories',localField: "parent", foreignField: "_id",as: "categoryName"}},
        {
            $project: {
                name: 1,
                image:1,
                status:1,
                dname:{$toLower: '$'+sorted},
                categoryName:1
            }
        },{
            $project: {
                name: 1,
                document: "$$ROOT"
            }
        },{
            $group:{"_id":null,"count":{"$sum":1},"documentData":{$push:"$document"}}
        }];

        var sorting = {};
        var searchs = '';

        var condition = {status: { $ne: 0 }};

        if(Object.keys(req.query).length!=0) {
            categoryQuery.push({$unwind: { path: "$documentData", preserveNullAndEmptyArrays: true }});

            if (req.query.search != '' && req.query.search != 'undefined' && req.query.search) {
                condition['name'] = {$regex: new RegExp('^' + req.query.search, 'i')};
                searchs = req.query.search;
                categoryQuery.push({ "$match":{"documentData.name":{$regex:  searchs+'.*' ,$options: 'si'}}});
            }
            if (req.query.sort !== '' && req.query.sort) {
                sorting = {};
                if (req.query.status == 'false') {
                    sorting["documentData.dname"] = -1;
                    categoryQuery.push({ $sort : sorting });
                } else {
                    sorting["documentData.dname"] = 1;
                    categoryQuery.push({ $sort : sorting });
                }
            }
            if (req.query.limit != 'undefined' && req.query.skip != 'undefined') {
                categoryQuery.push({'$skip':parseInt(req.query.skip)},{'$limit':parseInt(req.query.limit)});
            }
            categoryQuery.push({$group:{"_id":null,"count":{"$first":"$count"},"documentData":{$push:"$documentData"}}});
        }


        db.GetAggregation('category',categoryQuery, function (err,docdata){
            if(err){
                res.send(err);
            }else {
                if(docdata.length!=0){
                    res.send([docdata[0].documentData,docdata[0].count]);
                }else{
                    res.send([0,0]);
                }
            }
        });
}

controller.getcatlistdropdown = function(req,res) {
        db.GetDocument('category', {status: {$ne:0} }, {}, {},function(err, docdata) {
            if (err) {
                res.send(err);
            } else {
                res.send(docdata);
            }
        });
    }

controller.getsubcatlistdropdown = function(req,res) {
        db.GetDocument('category', {status: {$ne:0},parent: { $exists: false } }, {}, {},function(err, docdata) {
            if (err) {
                res.send(err);
            } else {
                res.send(docdata);
            }
        });
    }

		controller.deletepage = function(req,res) {
    	req.checkBody('delData', 'Invalid delData').notEmpty();
            var errors = req.validationErrors();
        if (errors) {
            res.send(errors, 400);
            return;
        }
				db.UpdateDocument('category',{_id:{$in:req.body.delData}}, {'status':0},{multi:true},function(err,docdata){

    // db.DeleteDocument('category', {_id:{$in:req.body.delData}},function(err,docdata){
        if(err){
            res.send(err);
        }else {

            res.send(docdata);
        }
    });
  }

	controller.deleteMaincategory = function(req,res) {
		req.checkBody('delData', 'Invalid delData').notEmpty();
					var errors = req.validationErrors();
			if (errors) {
					res.send(errors, 400);
					return;
			}
			db.UpdateDocument('category',{_id:{$in:req.body.delData}}, {'status':0},{multi:true},function(err,docdata){

	// db.DeleteDocument('category', {_id:{$in:req.body.delData}},function(err,docdata){
	    if(err){
	        res.send(err);
	    }else {
				db.UpdateDocument('category',{parent:{$in:req.body.delData}}, {'status':0},{multi:true},function(err,docdata){

				// db.DeleteDocument('category', {parent:{$in:req.body.delData}},function(err,docdata){
						if(err){
								res.send(err);
						}else {

	        res.send(docdata);
	    }
	});
}
});
}

    controller.allCategories = function allCategories(req, res) {
          var errors = req.validationErrors();
        if (errors) {
            res.send(errors, 400);
            return;
        }

        if (req.body.sort) {
            var sorted = req.body.sort.field;
        }


        var categoryQuery = [{
    				"$match":{status: { $ne: 0 }, parent: { $exists: false } }
        }, {
                $project: {
									name: 1,
									image:1,
									status:1,
									dname:{$toLower: '$'+sorted}
                }
            }, {
                $project: {
                    name: 1,
                    document: "$$ROOT"
                }
            }, {
                $group: { "_id": null, "count": { "$sum": 1 }, "documentData": { $push: "$document" } }
            }];


        var condition = { status: { $ne: 0 } };
        categoryQuery.push({ $unwind: { path: "$documentData", preserveNullAndEmptyArrays: true } });

        if (req.body.search) {
            condition['name'] = { $regex: new RegExp('^' + req.body.search, 'i') };
            var searchs = req.body.search;
            categoryQuery.push({ "$match": { "documentData.name": { $regex: searchs + '.*', $options: 'si' } } });
        }

        var sorting = {};
        if (req.body.sort) {
            var sorter = 'documentData.' + req.body.sort.field;
            sorting[sorter] = req.body.sort.order;
            categoryQuery.push({ $sort: sorting });
        } else {
            sorting["documentData.createdAt"] = -1;
            categoryQuery.push({ $sort: sorting });
        }

        if ((req.body.limit && req.body.skip >= 0) && !req.body.search) {
            categoryQuery.push({ '$skip': parseInt(req.body.skip) }, { '$limit': parseInt(req.body.limit) });
        }
        categoryQuery.push({ $group: { "_id": null, "count": { "$first": "$count" }, "documentData": { $push: "$documentData" } } });

        db.GetAggregation('category', categoryQuery, function (err, docdata) {
            /*if (err || docdata.length <= 0) {
                res.send([0, 0]);
            } else {

                res.send([docdata[0].documentData, docdata[0].count]);
            }*/
            if (err) {
                res.send(err);
            } else {

                if (docdata.length != 0) {
                    res.send([docdata[0].documentData, docdata[0].count]);
                } else {
                    res.send([0, 0]);
                }
            }
        });
    };



		    controller.allSubCategories = function allSubCategories(req, res) {

		          var errors = req.validationErrors();
		        if (errors) {
		            res.send(errors, 400);
		            return;
		        }

		        if (req.body.sort) {
		            var sorted = req.body.sort.field;
		        }


		        var categoryQuery = [{
  							"$match":{status: { $ne: 0 }, parent: { $exists: true } }
		        },
                  {$lookup:{from: 'categories',localField: "parent", foreignField: "_id",as: "categoryName"}},
                {
		                $project: {
											name: 1,
											image:1,
											status:1,
											dname:{$toLower: '$'+sorted},
											categoryName:1
		                }
		            }, {
		                $project: {
		                    name: 1,
		                    document: "$$ROOT"
		                }
		            }, {
		                $group: { "_id": null, "count": { "$sum": 1 }, "documentData": { $push: "$document" } }
		            }];


		        var condition = { status: { $ne: 0 } };
		        categoryQuery.push({ $unwind: { path: "$documentData", preserveNullAndEmptyArrays: true } });

		        if (req.body.search) {
		            condition['name'] = { $regex: new RegExp('^' + req.body.search, 'i') };
		            var searchs = req.body.search;
		            categoryQuery.push({

                             "$match": {
                                    $or: [
                                            { "documentData.name": { $regex: searchs + '.*', $options: 'si' } } ,
                                            { "documentData.categoryName.name": { $regex: searchs + '.*', $options: 'si' } },
                                        ]
                                         }

                    });
		        }

		        var sorting = {};
		        if (req.body.sort) {
		            var sorter = 'documentData.' + req.body.sort.field;
		            sorting[sorter] = req.body.sort.order;
		            categoryQuery.push({ $sort: sorting });
		        } else {
		            sorting["documentData.createdAt"] = -1;
		            categoryQuery.push({ $sort: sorting });
		        }

		        if ((req.body.limit && req.body.skip >= 0) && !req.body.search) {
		            categoryQuery.push({ '$skip': parseInt(req.body.skip) }, { '$limit': parseInt(req.body.limit) });
		        }
		        categoryQuery.push({ $group: { "_id": null, "count": { "$first": "$count" }, "documentData": { $push: "$documentData" } } });

		        db.GetAggregation('category', categoryQuery, function (err, docdata) {
		            if (err) {
		                res.send(err);
		            } else {

		                if (docdata.length != 0) {
		                    res.send([docdata[0].documentData, docdata[0].count]);
		                } else {
		                    res.send([0, 0]);
		                }
		            }
		        });
		    };

return controller;
}
