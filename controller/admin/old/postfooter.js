"use strict";

module.exports = function() {

	var mongoose			= require('mongoose');
	var db = require('../../controller/adaptor/mongodb.js');
	var attachment		= require('../../model/attachments.js');
    var Jimp = require("jimp");


  var controller = {};
   controller.save = function(req,res) {

           req.checkBody('title', 'Invalid title').notEmpty();
           req.checkBody('status', 'Invalid status').notEmpty();
           req.checkBody('description', 'Invalid description').notEmpty();
            var errors = req.validationErrors();
        if (errors) {
            res.send(errors, 400);
            return;
        }



            var errors = req.validationErrors();
        if (errors) {
            res.send(errors, 400);
            return;
        }
        
        var data = {};
        data.title = req.body.title;
        data.status = req.body.status;
        data.description= req.body.description;
        if(req.file) {
            data.image = attachment.get_attachment(req.file.destination, req.file.filename)
            data.img_name = encodeURI(req.file.filename);
            data.img_path = req.file.destination.substring(2);
                Jimp.read(req.file.path).then(function (lenna) {
                lenna.resize(100, 100)            // resize
                    .quality(60)                 // set JPEG quality
                    .write('./uploads/images/postfooter/thumbs/'+req.file.filename); // save
            }).catch(function (err) {
           
            });
        }

        
        if(req.body._id) {
            db.UpdateDocument('postfooter', {_id:{$in:req.body._id}}, data,function(err,result){
            if(err){
                res.send(err);
            }else
            {
                res.send(result);
            }
        });
        } else {

        data.status = req.body.status;
        db.InsertDocument('postfooter',data, function(err,result) {

                    if (err) {
                        res.send(err);
                    } else {
                        res.send(result);                  
                    }
                });
        }

	}

  controller.edit = function(req,res){

		db.GetOneDocument('postfooter', {_id:req.body.id}, {}, {}, function(err,docdata){
			if(err){
				res.send(err);
			}else {
				res.send(docdata);
			}
		});
	}

	controller.list = function(req,res){
           if(req.query.sort!=""){
            var sorted = req.query.sort;
        }
        var bannerQuery = [{
            "$match":{status: { $ne: 0 } }
        },{
            $project: {
                title: 1,
                image:1,
                status:1,
                description:1,     
                dname:{$toLower: '$'+sorted}

            }
        },{
            $project: {
                name: 1,
                document: "$$ROOT"
            }
        },{
            $group:{"_id":null,"count":{"$sum":1},"documentData":{$push:"$document"}}
        }];

        var sorting = {};
        var searchs = '';

        var condition = {status: { $ne: 0 }};

        if(Object.keys(req.query).length!=0) {
            bannerQuery.push({$unwind: { path: "$documentData", preserveNullAndEmptyArrays: true }});

            if (req.query.search != '' && req.query.search != 'undefined' && req.query.search) {
                condition['title'] = {$regex: new RegExp('^' + req.query.search, 'i')};
                searchs = req.query.search;
                bannerQuery.push({ "$match":{"documentData.title":{$regex:  searchs+'.*' ,$options: 'si'}}});
            }
            if (req.query.sort !== '' && req.query.sort) {
                sorting = {};
                if (req.query.status == 'false') {
                    sorting["documentData.dname"] = -1;
                    bannerQuery.push({ $sort : sorting });
                } else {
                    sorting["documentData.dname"] = 1;
                    bannerQuery.push({ $sort : sorting });
                }
            }
            if (req.query.limit != 'undefined' && req.query.skip != 'undefined') {
                bannerQuery.push({'$skip':parseInt(req.query.skip)},{'$limit':parseInt(req.query.limit)});
            }
            bannerQuery.push({$group:{"_id":null,"count":{"$first":"$count"},"documentData":{$push:"$documentData"}}});
        }


        db.GetAggregation('postfooter',bannerQuery, function (err,docdata){

            if(err){
                res.send(err);
            }else {

                if(docdata.length!=0){
                    res.send([docdata[0].documentData,docdata[0].count]);
                }else{
                    res.send([0,0]);
                }
            }
        });
};

controller.deletepostfooter = function(req,res){
           req.checkBody('delData', 'Invalid delData').notEmpty();
            var errors = req.validationErrors();
        if (errors) {
            res.send(errors, 400);
            return;
        }



        db.UpdateDocument('postfooter', {_id:{$in:req.body.delData}},{status:0},{multi:true},function(err,docdata){
            if(err){
                res.send(err);
            }else {
                res.send(docdata);
            }
        });
}

return controller;
}
