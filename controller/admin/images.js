"use strict";
module.exports = function () {
    var express = require('express');
    var multer = require('multer');
    var fs = require('fs');  /*dest: './static/banner' */
    var dynamicTime = '';
    var router = express.Router();
    var db = require('../../controller/adaptor/mongodb.js')
        , CONFIG = require('../../config/config');
    var attachment = require('../../model/attachments.js');
    // var middlewares  = require('../../model/middlewares.js');
    var Jimp = require("jimp");
    var controller = {};

    controller.list = function (req, res) {
        if (req.query.sort != "") {
            var sorted = req.query.sort;
        }
        var imagesQuery = [{
            "$match": { status: { $ne: 0 } }
        }, {
                $project: {
                    imagefor: 1,
                    image: 1,
                    status: 1,
                    dname: { $toLower: '$' + sorted }

                }
            }, {
                $project: {
                    name: 1,
                    document: "$$ROOT"
                }
            }, {
                $group: { "_id": null, "count": { "$sum": 1 }, "documentData": { $push: "$document" } }
            }];

        var sorting = {};
        var searchs = '';

        var condition = { status: { $ne: 0 } };

        if (Object.keys(req.query).length != 0) {
            imagesQuery.push({ $unwind: { path: "$documentData", preserveNullAndEmptyArrays: true } });

            if (req.query.search != '' && req.query.search != 'undefined' && req.query.search) {
                condition['imagefor'] = { $regex: new RegExp('^' + req.query.search, 'i') };
                searchs = req.query.search;
                imagesQuery.push({ "$match": { "documentData.imagefor": { $regex: searchs + '.*', $options: 'si' } } });
            }
            if (req.query.sort !== '' && req.query.sort) {
                sorting = {};
                if (req.query.status == 'false') {
                    sorting["documentData.dname"] = -1;
                    imagesQuery.push({ $sort: sorting });
                } else {
                    sorting["documentData.dname"] = 1;
                    imagesQuery.push({ $sort: sorting });
                }
            }
            if (req.query.limit != 'undefined' && req.query.skip != 'undefined') {
                imagesQuery.push({ '$skip': parseInt(req.query.skip) }, { '$limit': parseInt(req.query.limit) });
            }
            imagesQuery.push({ $group: { "_id": null, "count": { "$first": "$count" }, "documentData": { $push: "$documentData" } } });
        }
        db.GetAggregation('images', imagesQuery, function (err, docdata) {

            if (err) {
                res.send(err);
            } else {

                if (docdata.length != 0) {
                    res.send([docdata[0].documentData, docdata[0].count]);
                } else {
                    res.send([0, 0]);
                }
            }
        });
    }

    controller.edit = function (req, res) {

        db.GetDocument('images', { _id: req.body.id }, {}, {}, function (err, data) {
            if (err) {
                res.send(err);
            } else {
                res.send(data);

            }
        });
    }

    controller.save = function (req, res) {
        req.checkBody('imagefor', 'Invalid guideline name').notEmpty();
        // req.checkBody('status', 'guideline status is invalid').notEmpty();
        var errors = req.validationErrors();
        if (errors) {
            res.send(errors, 400);
            return;
        }

        var data = {};
        data.imagefor = req.body.imagefor;
        data.status = req.body.status;
        if (req.file) {
            data.image = attachment.get_attachment(req.file.destination, req.file.filename)
            data.img_name = encodeURI(req.file.filename);
            data.img_path = req.file.destination.substring(2);
            Jimp.read(req.file.path).then(function (lenna) {
                lenna.resize(100, 100)            // resize
                    .quality(60)                 // set JPEG quality
                    .write('./uploads/images/images/thumbs/' + req.file.filename); // save
            }).catch(function (err) {

            });
        }


        if (req.body._id) {
            db.UpdateDocument('images', { _id: { $in: req.body._id } }, data, function (err, result) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(result);

                }
            });
        } else {
            data.status = req.body.status;
            db.InsertDocument('images', data, function (err, result) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(result);

                }
            });
        }
    }

    controller.deleteimages = function (req, res) {

        req.checkBody('delData', 'Invalid images delData').notEmpty();
        var errors = req.validationErrors();
        if (errors) {
            res.send(errors, 400);
            return;
        }

        db.UpdateDocument('images', { _id: { $in: req.body.delData } }, { status: 0 }, { multi: true }, function (err, docdata) {
            if (err) {
                res.send(err);
            } else {
                res.send(docdata);
            }
        });
    }
    return controller;
    /*module.exports = router;*/
}
