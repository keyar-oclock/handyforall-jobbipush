"use strict";

module.exports = function() {

	var mongoose	= require('mongoose');
	var db = require('../../controller/adaptor/mongodb.js');


  var controller = {};

  controller.submitmainpage = function(req,res) {
		var data = {};
		data.seo = {}
		data.name											=req.body.data.name;
		data.description							=req.body.data.description;
		data.css_script								=req.body.data.css_script	;
		data.seo.title								=req.body.data.seo.title ;
		data.seo.keyword							=req.body.data.seo.keyword;
		data.seo.description					=req.body.data.seo.description;
		data.slug											=req.body.data.slug;
		data.parent										=req.body.data.parent;
		data.status										=req.body.data.status;

	  if(req.body.data._id){
        db.UpdateDocument('pages', { _id : new mongoose.Types.ObjectId(req.body.data._id)} , data,function(err,result){
            if(err) {
                res.send(err);
            } else {
                //res.send(result);
								res.send({ message: 'Pages Updated Successfully' });
            }
        });
    } else {
        db.InsertDocument('pages',data, function(err,result){
            if(err){
                res.send(err);
            }else {
                //res.send(result);
								res.send({ message: 'Pages Added Successfully' });
            }
        });
    }
}


controller.getlist = function(req,res) {
			var errors = req.validationErrors();
			if (errors) {
					res.send(errors, 400);
					return;
			}

			if (req.body.sort) {
					var sorted = req.body.sort.field;
			}


			var pagesQuery = [{
					"$match": { status: { $ne: 0 } }
			}, {
							$project: {
								name: 1,
	            createdAt:1,
	            status:1,
	            dname:{$toLower: '$'+sorted}
							}
					}, {
							$project: {
									name: 1,
									document: "$$ROOT"
							}
					}, {
							$group: { "_id": null, "count": { "$sum": 1 }, "documentData": { $push: "$document" } }
					}];


			var condition = { status: { $ne: 0 } };
			pagesQuery.push({ $unwind: { path: "$documentData", preserveNullAndEmptyArrays: true } });

			if (req.body.search) {
					condition['name'] = { $regex: new RegExp('^' + req.body.search, 'i') };
					var searchs = req.body.search;
					pagesQuery.push({ "$match": { "documentData.name": { $regex: searchs + '.*', $options: 'si' } } });
			}

			var sorting = {};
			if (req.body.sort) {
					var sorter = 'documentData.' + req.body.sort.field;
					sorting[sorter] = req.body.sort.order;
					pagesQuery.push({ $sort: sorting });
			} else {
					sorting["documentData.createdAt"] = -1;
					pagesQuery.push({ $sort: sorting });
			}

			if ((req.body.limit && req.body.skip >= 0) && !req.body.search) {
					pagesQuery.push({ '$skip': parseInt(req.body.skip) }, { '$limit': parseInt(req.body.limit) });
			}
			pagesQuery.push({ $group: { "_id": null, "count": { "$first": "$count" }, "documentData": { $push: "$documentData" } } });

			db.GetAggregation('pages', pagesQuery, function (err, docdata) {
					if (err) {
							res.send(err);
					} else {

							if (docdata.length != 0) {
									res.send([docdata[0].documentData, docdata[0].count]);
							} else {
									res.send([0, 0]);
							}
					}
			});
}


controller.deletepage = function(req,res) {

    db.UpdateDocument('pages', {_id:{$in:req.body.delData}},{status:0},{ multi: true },function(err,docdata) {
        req.checkBody('delData', 'Invalid delData').notEmpty();
        var errors = req.validationErrors();
        if (errors) {
            res.send(errors, 400);
            return;
        }
        if(err){
            res.send(err);
        }else {
            res.send(docdata);
        }
    });
}


controller.editpage = function(req,res) {
    db.GetDocument('pages',{status: { $ne: 0 }, _id:req.body.id},{},{} ,function(err,docdata){
        if(err){
            res.send(err);
        }else {
            res.send(docdata);
        }
    });
}

controller.getlistdropdown = function(req,res) {

        db.GetDocument('pages', {status: {$ne:0} }, {}, {},function(err, docdata) {
            if (err) {
                res.send(err);
            } else {
                res.send(docdata);
            }
        });
    }


return controller;
}
