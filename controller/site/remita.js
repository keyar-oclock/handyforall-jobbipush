module.exports = function (io) {

	var express = require('express');
	var bcrypt = require('bcrypt-nodejs');
	var db = require('../../controller/adaptor/mongodb.js')
	var attachment = require('../../model/attachments.js');
	var middlewares = require('../../model/middlewares.js');
	var mail = require('../../model/mail.js');
	var mailcontent = require('../../model/mailcontent.js');
	//var upload = middlewares.upload();
	var async = require('async');
	var request = require('request');
	var mongoose = require('mongoose');
	var objectID = require('mongodb').ObjectID;
	var twilio = require('../../model/twilio.js');
	var library = require('../../model/library.js');
	var paypal = require('paypal-rest-sdk');
	var CONFIG = require('../../config/config');
	var moment = require("moment");
	var pdf = require('html-pdf');
	var fs = require('fs');  /*dest: './static/banner' */
	var pug = require('pug');  /*dest: './static/banner' */
	var push = require('../../model/pushNotification.js')(io);
	var querystring = require('querystring');
	var sha512 = require('sha512');
	var http = require('http');

	var controller = {};


	controller.updatewalletdata = function updatewalletdata(req, res) {
		var request = {};
		request.user = req.body.user;
		request.total_amount = parseFloat(req.body.data.walletamount).toFixed(2);
		request.payername = req.body.data.walletrecharge.card.payername;
		request.payeremail = req.body.data.walletrecharge.card.payeremail;
		request.payerphone = req.body.data.walletrecharge.card.payerphone;
		var transaction = {
			'user': request.user,
			'type': 'remita',
			'amount': request.total_amount,
			'status': 1
		};
		db.InsertDocument('transaction', transaction, function (err, transaction) {
			request.transaction_id = transaction._id;
			request.trans_date = transaction.createdAt;
			request.avail_amount = transaction.amount;
			request.credit_type = transaction.type;
			if (err) {
				res.send(err);
			} else {
				async.waterfall([
					function (callback) {
						db.GetOneDocument('paymentgateway', { status: { $ne: 0 }, alias: 'remita' }, {}, {}, function (err, paymentgateway) {
							if (err) {
								res.redirect("/payment-failed/" + request.user);
							} else {
								callback(err, paymentgateway);
							}
						});
					},
					function (paymentgateway, callback) {
						db.GetOneDocument('transaction', { '_id': request.transaction_id }, {}, {}, function (err, transaction) {
							if (err || !transaction) {
								res.redirect("/payment-failed/" + request.user);
							} else {
								callback(err, paymentgateway, transaction);
							}
						});
					},
					function (paymentgateway, transaction, callback) {
						db.GetOneDocument('settings', { 'alias': 'general' }, {}, {}, function (err, settings) {
							if (err || !settings) {
								res.redirect("/payment-failed/" + request.user);
							} else {
								callback(err, paymentgateway, transaction, settings.settings);
							}
						});
					},
					function (paymentgateway, transaction, settings, callback) {
						db.GetDocument('emailtemplate', { name: { $in: ['PaymentDetailstoAdmin', 'PaymentDetailstoTasker', 'PaymentDetailstoUser'] }, 'status': { $ne: 0 } }, {}, {}, function (err, template) {
							if (err || !template) {
								res.redirect("/payment-failed/" + request.user);
							} else {
								callback(err, paymentgateway, transaction, settings, template);
							}
						});
					}
				], function (err, paymentgateway, transaction, settings, template) {
					if (err) {
						res.redirect("/payment-failed/" + request.user);
					} else {
						var data = {};
						var currentdate = new Date();
						data.status = 1;
						data.merchantId = paymentgateway.settings.merchant_id;
						data.serviceTypeId = paymentgateway.settings.service_typeid;
						data.api_key = paymentgateway.settings.api_key;
						data.mode = paymentgateway.settings.mode;
						data.amt = request.total_amount;
						data.payerName = request.payername;
						data.payerEmail = request.payeremail;
						data.payerPhone = request.payerphone;
						data.orderId = currentdate.getTime();
						data.responseurl = settings.site_url + "site/account/payementwallet?transaction=" + transaction._id + "&user_id=" + request.user;
						var concatString = data.merchantId + data.serviceTypeId + data.orderId + data.amt + data.responseurl + data.api_key;
						var hashing = sha512(concatString);
						data.hash = hashing.toString('hex');
						data = querystring.stringify(data);
						var url = "http://www.remitademo.net/remita/ecomm" + '/' + data.merchantId + '/' + data.orderId + '/' + data.hash + '/' + 'orderstatus.reg';

						var options = {
							protocol: 'http:',
							host: 'www.remitademo.net',
							path: '/remita/ecomm/init.reg',
							method: 'POST',
							headers: {
								'Content-Type': 'application/x-www-form-urlencoded',
								'Content-Length': Buffer.byteLength(data)
							}
						};

						var output = {};
						var req = http.request(options, (result) => {
							output.redirectUrl = result.headers.location;
							output.status = 1;
							output.payment_mode = 'remita';
							output.user = request.user;
							result.setEncoding('utf8');

							result.on('data', (chunk) => {
							});

							result.on('end', () => {
								res.send(output);
							});
						});
						req.on('error', (e) => {
						});
						req.write(data);
						req.end();
					}
				});
			}
		});
	}


	controller.payementwallet = function payementwallet(req, res) {
		var data = {};
		data.status = 0;
		var request = {};
		request.transaction = req.query.transaction[0];
		request.user_id = req.query.user_id[0];
		async.waterfall([
			function (callback) {
				db.GetOneDocument('paymentgateway', { status: { $ne: 0 }, alias: 'remita' }, {}, {}, function (err, paymentgateway) {
					if (err || !paymentgateway) {
						res.redirect("/payment-failed/" + request.user_id);
					} else {
						callback(err, paymentgateway);
					}
				});
			},
			function (paymentgateway, callback) {
				db.GetOneDocument('transaction', { _id: request.transaction }, {}, {}, function (err, transaction) {
					request.transaction_id = transaction._id;
					request.trans_id = transaction._id;
					request.trans_date = transaction.createdAt;
					request.avail_amount = transaction.amount;
					request.credit_type = transaction.type;
					if (err || !transaction) {
						res.redirect("/payment-failed/" + request.user_id);
					} else {
						callback(err, paymentgateway, transaction);
					}
				});
			},
			function (paymentgateway, transaction, callback) {
				db.GetOneDocument('settings', { 'alias': 'general' }, {}, {}, function (err, settings) {
					if (err || !settings) {
						res.redirect("/payment-failed/" + request.user_id);
					}
					else { callback(err, paymentgateway, transaction, settings.settings); }
				});
			},
			function (paymentgateway, transaction, settings, callback) {
				db.GetDocument('emailtemplate', { name: { $in: ['PaymentDetailstoAdmin', 'PaymentDetailstoTasker', 'PaymentDetailstoUser'] }, 'status': { $ne: 0 } }, {}, {}, function (err, template) {
					if (err || !template) {
						res.redirect("/payment-failed/" + request.user_id);
					}
					else { callback(err, paymentgateway, transaction, settings, template); }
				});
			}
		], function (err, paymentgateway, transaction, settings, template) {

			if (err) {
				res.redirect("/payment-failed/" + request.user_id);
			} else {
				data.merchantId = paymentgateway.settings.merchant_id;
				data.serviceTypeId = paymentgateway.settings.service_typeid;
				data.api_key = paymentgateway.settings.api_key;
				var concatString = req.query.orderID + data.api_key + data.merchantId;
				var hashing = sha512(concatString);
				data.hash = hashing.toString('hex');
				var url = 'http://www.remitademo.net/remita/ecomm' + '/' + data.merchantId + '/' + req.query.orderID + '/' + data.hash + '/' + 'orderstatus.reg';

				return http.get({
					protocol: 'http:',
					host: 'www.remitademo.net',
					path: url
				}, function (response) {
					var body = '';
					response.on('data', function (d) {
						body += d;
					});
					response.on('end', function () {
						var parsed = JSON.parse(body);
						if (parsed.status == '00' || parsed.status == '01') {
							var transactionsData = [{
								'gateway_response': parsed
							}];
							db.UpdateDocument('transaction', { '_id': request.transaction_id }, { 'transactions': transactionsData }, {}, function (err, transactionUpdate) {
								if (err) {
									res.redirect("/payment-failed/" + request.user_id);
								} else {
									db.GetOneDocument('walletReacharge', { "user_id": request.user_id }, {}, {}, function (err, wallet) {
										var transactions = {
											'trans_id': request.trans_id,
											'trans_date': request.trans_date,
											'trans_amount': request.avail_amount,
											'avail_amount': 0,
											'type': 'CREDIT'
										};
										if (wallet) {
											transactions.avail_amount = parseInt(wallet.total) + parseInt(request.avail_amount);
											db.UpdateDocument('walletReacharge', { 'user_id': request.user_id }, { total: transactions.avail_amount, $push: { 'transactions': transactions } }, {}, function (err, docdata) {
												if (err || docdata.nModified == 0) {
													res.redirect("/payment-failed/" + request.user_id);
												} else {
													var payment = {};
													payment.user = request.user_id;
													payment.transaction = request.transaction_id;
													payment.image = settings.site_url + 'app/mobile/images/success.png';
													res.redirect("/wallet-success");
												}
											});
										} else {
											transactions.avail_amount = request.avail_amount;
											var insertdata = {};
											insertdata.user_id = request.user_id;
											insertdata.total = request.avail_amount;
											insertdata.transactions = [];
											insertdata.transactions.push(transactions);
											db.InsertDocument('walletReacharge', insertdata, function (err, docdata) {
												if (err || docdata.nModified == 0) {
													res.redirect("/payment-failed/" + request.user_id);
												} else {
													var payment = {};
													payment.user = request.user_id;
													payment.transaction = request.transaction_id;
													payment.image = settings.site_url + 'app/mobile/images/success.png';
													res.redirect("/wallet-success");
												}
											});
										}
									});
								}
							});
						} else {
							res.redirect("/payment-failed/" + request.user_id);
						}
					});
				});
			}
		});
	}




	controller.remitaPayment = function remitaPayment(req, res) {
		var request = {};
		request.task = req.body.task;
		request.user = req.body.user;
		request.amount = req.body.amount;
		request.payername = req.body.payername;
		request.payeremail = req.body.payeremail;
		request.payerphone = req.body.payerphone;
		var options = {};
		options.populate = 'user category tasker';
		db.GetOneDocument('task', { _id: request.task }, {}, options, function (err, task) {
			if (err || !task) {
				res.send(err);
			} else {
				async.waterfall([
					function (callback) {
						db.GetOneDocument('paymentgateway', { status: { $ne: 0 }, alias: 'remita' }, {}, {}, function (err, paymentgateway) {
							callback(err, paymentgateway);
						});
					},
					function (paymentgateway, callback) {
						var transaction = {};
						transaction.user = task.user;
						transaction.tasker = task.tasker;
						transaction.task = request.task;
						transaction.type = 'remita';
						if (transaction.amount = task.invoice.amount.balance_amount) {
							transaction.amount = task.invoice.amount.balance_amount;
						} else {
							transaction.amount = task.invoice.amount.grand_total;
						}
						transaction.amount = task.invoice.amount.balance_amount;
						transaction.task_date = task.createdAt;
						transaction.status = 1;
						db.InsertDocument('transaction', transaction, function (err, transaction) {
							request.transaction_id = transaction._id;
							request.trans_date = transaction.createdAt;
							request.avail_amount = transaction.amount;
							request.credit_type = transaction.type;
							callback(err, paymentgateway, transaction);
						});
					},
					function (paymentgateway, transaction, callback) {
						db.GetOneDocument('settings', { 'alias': 'general' }, {}, {}, function (err, settings) {
							if (err || !settings) { data.response = 'Configure your website settings'; res.send(data); }
							else { callback(err, paymentgateway, transaction, settings.settings); }
						});
					},
					function (paymentgateway, transaction, settings, callback) {
						db.GetDocument('emailtemplate', { name: { $in: ['PaymentDetailstoAdmin', 'PaymentDetailstoTasker', 'PaymentDetailstoUser'] }, 'status': { $ne: 0 } }, {}, {}, function (err, template) {
							if (err || !template) { data.response = 'Unable to get email template'; res.send(data); }
							else { callback(err, paymentgateway, transaction, settings, template); }
						});
					}
				], function (err, paymentgateway, transaction, settings, template) {
					if (err) {
						res.status(400).send(err);
					} else {
						var data = {};
						var currentdate = new Date();
						data.status = 1;
						data.merchantId = paymentgateway.settings.merchant_id;
						data.serviceTypeId = paymentgateway.settings.service_typeid;
						data.api_key = paymentgateway.settings.api_key;
						data.mode = paymentgateway.settings.mode;
						data.amt = request.amount;
						data.payerName = request.payername;
						data.payerEmail = request.payeremail;
						data.payerPhone = request.payerphone;
						data.orderId = currentdate.getTime();
						data.responseurl = settings.site_url + "site/account/samplereceipt?task=" + request.task + "&transaction=" + transaction._id;
						var concatString = data.merchantId + data.serviceTypeId + data.orderId + data.amt + data.responseurl + data.api_key;
						var hashing = sha512(concatString);
						data.hash = hashing.toString('hex');
						data = querystring.stringify(data);
						var url = "http://www.remitademo.net/remita/ecomm" + '/' + data.merchantId + '/' + data.orderId + '/' + data.hash + '/' + 'orderstatus.reg';

						var options = {
							protocol: 'http:',
							host: 'www.remitademo.net',
							path: '/remita/ecomm/init.reg',
							method: 'POST',
							headers: {
								'Content-Type': 'application/x-www-form-urlencoded',
								'Content-Length': Buffer.byteLength(data)
							}
						};
						var output = {};
						var req = http.request(options, (result) => {
							output.redirectUrl = result.headers.location;
							output.status = 1;
							output.payment_mode = 'remita';
							output.task = request.task;
							output.user = request.user;
							result.setEncoding('utf8');

							result.on('data', (chunk) => {
							});

							result.on('end', () => {
								res.send(output);
							});

						});
						req.on('error', (e) => {
						});
						req.write(data);
						req.end();
					}
				});
			}
		});
	}

	controller.samplereceipt = function samplereceipt(req, res) {
		var data = {};
		data.status = 0;
		var request = {};
		request.task = req.query.task[0];
		request.transaction_id = req.query.transaction[0];
		request.orderID = req.query.orderID;

		var options = {};
		options.populate = 'tasker user task';
		db.GetOneDocument('transaction', { _id: request.transaction_id }, {}, options, function (err, transaction) {
			if (err) {
				res.send(err);
			} else {
				async.waterfall([
					function (callback) {
						db.GetOneDocument('paymentgateway', { status: { $ne: 0 }, alias: 'remita' }, {}, {}, function (err, paymentgateway) {
							if (err || !paymentgateway) { data.response = 'Configure your website settings'; res.send(data); }
							else { callback(err, paymentgateway); }
						});
					},
					function (paymentgateway, callback) {
						db.GetOneDocument('transaction', { _id: request.transaction_id }, {}, options, function (err, transaction) {
							if (err || !transaction) { data.response = 'Configure your website settings'; res.send(data); }
							else { callback(err, paymentgateway, transaction); }
						});
					},
					function (paymentgateway, transaction, callback) {
						db.GetOneDocument('settings', { 'alias': 'general' }, {}, {}, function (err, settings) {
							if (err || !settings) { data.response = 'Configure your website settings'; res.send(data); }
							else { callback(err, paymentgateway, transaction, settings.settings); }
						});
					},
					function (paymentgateway, transaction, settings, callback) {
						db.GetDocument('emailtemplate', { name: { $in: ['PaymentDetailstoAdmin', 'PaymentDetailstoTasker', 'PaymentDetailstoUser'] }, 'status': { $ne: 0 } }, {}, {}, function (err, template) {
							if (err || !template) { data.response = 'Unable to get email template'; res.send(data); }
							else { callback(err, paymentgateway, transaction, settings, template); }
						});
					}
				], function (err, paymentgateway, transaction, settings, template) {
					if (err) {
						res.status(400).send(err);
					} else {
						data.merchantId = paymentgateway.settings.merchant_id;
						data.serviceTypeId = paymentgateway.settings.service_typeid;
						data.api_key = paymentgateway.settings.api_key;
						var concatString = req.query.orderID + data.api_key + data.merchantId;
						var hashing = sha512(concatString);
						data.hash = hashing.toString('hex');
						var url = 'http://www.remitademo.net/remita/ecomm' + '/' + data.merchantId + '/' + req.query.orderID + '/' + data.hash + '/' + 'orderstatus.reg';

						return http.get({
							protocol: 'http:',
							host: 'www.remitademo.net',
							path: url
						}, function (response) {
							var body = '';
							response.on('data', function (d) {
								body += d;
							});
							response.on('end', function () {
								try {
									var parsed = JSON.parse(body);
									if (parsed.status == '00' || parsed.status == '01') {
										var dataToUpdate = {};
										dataToUpdate.status = 7;
										dataToUpdate.invoice = transaction.task.invoice;
										dataToUpdate.invoice.status = 1;
										dataToUpdate.payee_status = 0;
										dataToUpdate.invoice.amount.balance_amount = parseFloat(transaction.task.invoice.amount.balance_amount) - parseFloat(transaction.task.invoice.amount.balance_amount);
										dataToUpdate.payment_type = 'remita';

										var transactionsData = [{
											'gateway_response': parsed
										}];
										db.UpdateDocument('transaction', { '_id': request.transaction_id }, { 'transactions': transactionsData }, {}, function (err, transactionUpdate) {
											if (err) {
												res.status(400).send(err);
											} else {
												db.UpdateDocument('task', { _id: transaction.task._id }, dataToUpdate, function (err, docdata) {
													if (err) {
														res.status(400).send(err);
													} else {
														var options = {};
														options.populate = 'tasker task user';
														db.GetOneDocument('task', { _id: request.task }, {}, options, function (err, docdata) {
															if (err || !docdata) {
																res.send(err);
															} else {
																var notifications = { 'job_id': docdata.booking_id, 'user_id': docdata.tasker._id };
																var message = 'Your billing amount paid successfully';
																push.sendPushnotification(docdata.tasker._id, message, 'payment_paid', 'ANDROID', notifications, 'PROVIDER', function (err, response, body) { });
																var html = template[0].email_content;
																html = html.replace(/{{t_firstname}}/g, transaction.tasker.name.first_name);
																html = html.replace(/{{t_lastname}}/g, transaction.tasker.name.last_name);
																html = html.replace(/{{taskeraddress}}/g, transaction.tasker.address.line1);
																html = html.replace(/{{taskeraddress1}}/g, transaction.tasker.address.city);
																html = html.replace(/{{taskeraddress2}}/g, transaction.tasker.address.state);
																html = html.replace(/{{bookingid}}/g, transaction.task.booking_id);
																html = html.replace(/{{u_firstname}}/g, transaction.user.name.first_name);
																html = html.replace(/{{u_lastname}}/g, transaction.user.name.last_name);
																html = html.replace(/{{useraddress}}/g, transaction.user.address.line1);
																html = html.replace(/{{useraddress1}}/g, transaction.user.address.city);
																html = html.replace(/{{useraddress2}}/g, transaction.user.address.state);
																html = html.replace(/{{categoryname}}/g, transaction.task.booking_information.work_type);
																html = html.replace(/{{hourlyrates}}/g, transaction.task.invoice.amount.task_cost);
																html = html.replace(/{{totalhour}}/g, transaction.task.invoice.worked_hours);
																html = html.replace(/{{totalamount}}/g, transaction.task.invoice.amount.grand_total);
																html = html.replace(/{{amount}}/g, transaction.task.invoice.amount.grand_total);
																html = html.replace(/{{adminamount}}/g, transaction.task.invoice.amount.admin_commission);
																html = html.replace(/{{privacy}}/g, settings.site_url + 'pages/privacypolicy');
																html = html.replace(/{{terms}}/g, settings.site_url + '/pages/termsandconditions');
																html = html.replace(/{{senderemail}}/g, template[0].sender_email);
																html = html.replace(/{{logo}}/g, settings.site_url + settings.logo);
																html = html.replace(/{{site_title}}/g, settings.site_title);
																var mailOptions = {
																	from: template[0].sender_email,
																	to: template[0].sender_email,
																	subject: template[0].email_subject,
																	text: html,
																	html: html
																};
																mail.send(mailOptions, function (err, response) { });

																var html1 = template[1].email_content;
																html1 = html1.replace(/{{firstname}}/g, transaction.tasker.name.first_name);
																html1 = html1.replace(/{{lastname}}/g, transaction.tasker.name.last_name);
																html1 = html1.replace(/{{taskeraddress}}/g, transaction.tasker.address.line1);
																html1 = html1.replace(/{{taskeraddress1}}/g, transaction.tasker.address.city);
																html1 = html1.replace(/{{taskeraddress2}}/g, transaction.tasker.address.state);
																html1 = html1.replace(/{{bookingid}}/g, transaction.task.booking_id);
																html1 = html1.replace(/{{categoryname}}/g, transaction.task.booking_information.work_type);
																html1 = html1.replace(/{{hourlyrates}}/g, transaction.task.invoice.amount.task_cost);
																html1 = html1.replace(/{{totalhour}}/g, transaction.task.invoice.worked_hours);
																html1 = html1.replace(/{{totalamount}}/g, transaction.task.invoice.amount.grand_total);
																html1 = html1.replace(/{{amount}}/g, (transaction.task.invoice.amount.grand_total - transaction.task.invoice.amount.admin_commission));
																html1 = html1.replace(/{{admincommission}}/g, transaction.task.invoice.amount.admin_commission);
																html1 = html1.replace(/{{privacy}}/g, settings.site_url + 'pages/privacypolicy');
																html1 = html1.replace(/{{terms}}/g, settings.site_url + '/pages/termsandconditions');
																html1 = html1.replace(/{{senderemail}}/g, template[0].sender_email);
																html1 = html1.replace(/{{logo}}/g, settings.site_url + settings.logo);
																html1 = html1.replace(/{{site_title}}/g, settings.site_title);
																var mailOptions1 = {
																	from: template[1].sender_email,
																	to: transaction.tasker.email,
																	subject: template[1].email_subject,
																	text: html1,
																	html1: html1
																};
																mail.send(mailOptions1, function (err, response) { });

																var html2 = template[2].email_content;
																html2 = html2.replace(/{{bookingid}}/g, transaction.task.booking_id);
																html2 = html2.replace(/{{firstname}}/g, transaction.user.name.first_name);
																html2 = html2.replace(/{{lastname}}/g, transaction.user.name.last_name);
																html2 = html2.replace(/{{useraddress}}/g, transaction.user.address.line1);
																html2 = html2.replace(/{{useraddress1}}/g, transaction.user.address.city);
																html2 = html2.replace(/{{useraddress2}}/g, transaction.user.address.state);
																html2 = html2.replace(/{{categoryname}}/g, transaction.task.booking_information.work_type);
																html2 = html2.replace(/{{hourlyrates}}/g, transaction.task.invoice.amount.task_cost);
																html2 = html2.replace(/{{totalhour}}/g, transaction.task.invoice.worked_hours);
																html2 = html2.replace(/{{totalamount}}/g, transaction.task.invoice.amount.grand_total);
																html2 = html2.replace(/{{amount}}/g, transaction.task.invoice.amount.grand_total);
																html2 = html2.replace(/{{privacy}}/g, settings.site_url + 'pages/privacypolicy');
																html2 = html2.replace(/{{terms}}/g, settings.site_url + '/pages/termsandconditions');
																html2 = html2.replace(/{{senderemail}}/g, template[0].sender_email);
																html2 = html2.replace(/{{logo}}/g, settings.site_url + settings.logo);
																html2 = html2.replace(/{{site_title}}/g, settings.site_title);
																var mailOptions2 = {
																	from: template[2].sender_email,
																	to: transaction.user.email,
																	subject: template[2].email_subject,
																	text: html2,
																	html2: html2
																};
																mail.send(mailOptions2, function (err, response) { });
																res.redirect("/payment-success");
															}
														});
													}
												});
											}
										});
									}
									else {
										res.redirect("/payment-failed/" + request.task);
									}
								} catch (e) {
									res.redirect("/payment-failed/" + request.task);
								}
							});
						});
					}
				});
			}
		});
	}


	return controller;
};
