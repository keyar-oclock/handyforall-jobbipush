"use strict";
module.exports = function () {
    var fs = require('fs');  /*dest: './static/banner' */
    var mail = require('../../model/mail.js');
    var db = require('../../controller/adaptor/mongodb.js');
    var async = require('async');
    var controller = {};

    controller.list = function (req, res) {
        if (req.query.sort != "") {
            var sorted = req.query.sort;
        }
        var bannerQuery = [{
            "$match": { status: { $ne: 0 } }
        }, {
                $project: {
                    name: 1,
                    email: 1,
                    mobile: 1,
                    subject: 1,
                    message: 1,
                    createdon: 1,
                }
            }, {
                $project: {
                    name: 1,
                    document: "$$ROOT"
                }
            }, {
                $group: { "_id": null, "count": { "$sum": 1 }, "documentData": { $push: "$document" } }
            }];

        var sorting = {};
        var searchs = '';

        var condition = { status: { $ne: 0 } };

        if (Object.keys(req.query).length != 0) {
            bannerQuery.push({ $unwind: { path: "$documentData", preserveNullAndEmptyArrays: true } });

            if (req.query.search != '' && req.query.search != 'undefined' && req.query.search) {
                condition['name'] = { $regex: new RegExp('^' + req.query.search, 'i') };
                searchs = req.query.search;
                bannerQuery.push({ "$match": { "documentData.name": { $regex: searchs + '.*', $options: 'si' } } });
            }
            if (req.query.sort !== '' && req.query.sort) {
                sorting = {};
                if (req.query.status == 'false') {
                    sorting["documentData.dname"] = -1;
                    bannerQuery.push({ $sort: sorting });
                } else {
                    sorting["documentData.dname"] = 1;
                    bannerQuery.push({ $sort: sorting });
                }
            }
            if (req.query.limit != 'undefined' && req.query.skip != 'undefined') {
                bannerQuery.push({ '$skip': parseInt(req.query.skip) }, { '$limit': parseInt(req.query.limit) });
            }
            bannerQuery.push({ $group: { "_id": null, "count": { "$first": "$count" }, "documentData": { $push: "$documentData" } } });
        }


        db.GetAggregation('contact', bannerQuery, function (err, docdata) {

            if (err) {
                res.send(err);
            } else {

                if (docdata.length != 0) {
                    res.send([docdata[0].documentData, docdata[0].count]);
                } else {
                    res.send([0, 0]);
                }
            }
        });
    }

    controller.edit = function (req, res) {

        db.GetDocument('contact', { _id: req.body.id }, {}, {}, function (err, data) {
            if (err) {
                res.send(err);
            } else {
                res.send(data);
            }
        });
    }

    controller.save = function (req, res) {

        var data = {};
        data.name = req.body.username;
        data.email = req.body.email;
        data.mobile = req.body.mobile;
        data.subject = req.body.subject;
        data.message = req.body.message;


        if (req.body._id) {
            db.UpdateDocument('contact', { _id: { $in: req.body._id } }, data, function (err, result) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(result);
                }
            });
        } else {

            data.status = req.body.status;
            db.InsertDocument('contact', data, function (err, result) {
                if (err) {
                    res.send(err);
                } else {
                    res.send(result);
                }
            });
        }


        /*			Contact Email template*/

        async.waterfall([
            function (callback) {
                db.GetOneDocument('settings', { 'alias': 'general' }, {}, {}, function (err, settings) {

                    if (err || !settings) { data.response = 'Configure your website settings'; res.send(data); }
                    else { callback(err, settings.settings); }
                });
            },
            function (settings, callback) {

                db.GetDocument('emailtemplate', { name: { $in: ['contactusmessagetosender', 'Contactusmessagetoadmin'] }, 'status': { $eq: 1 } }, {}, {}, function (err, template) {
                    if (err || !template) { data.response = 'Unable to get email template'; res.send(data); }
                    else { callback(err, settings, template); }
                });
            }
        ], function (err, settings, template) {
            var html = template[1].email_content;
            html = html.replace(/{{username}}/g, data.name);
            html = html.replace(/{{message}}/g, data.message);
            html = html.replace(/{{subject}}/g, data.subject);
            html = html.replace(/{{privacy}}/g, settings.site_url + 'pages/privacypolicy');
            html = html.replace(/{{terms}}/g, settings.site_url + '/pages/termsandconditions');
            html = html.replace(/{{senderemail}}/g, template[1].sender_email);
            html = html.replace(/{{logo}}/g, settings.site_url + settings.logo);
            html = html.replace(/{{site_title}}/g, settings.site_title);
            html = html.replace(/{{email}}/g, data.email);
            var mailOptions = {
                from: template[1].sender_email,
                to: data.email,
                subject: template[1].email_subject,
                text: html,
                html: html
            };

            mail.send(mailOptions, function (err, response) { });

            var html1 = template[0].email_content;
            html1 = html1.replace(/{{username}}/g, data.name);
            html1 = html1.replace(/{{contactemail}}/g, data.email);
            html1 = html1.replace(/{{mobile}}/g, data.mobile);
            html1 = html1.replace(/{{sendersubject}}/g, data.subject);
            html1 = html1.replace(/{{message}}/g, data.message);
            html1 = html1.replace(/{{privacy}}/g, settings.site_url + 'pages/privacypolicy');
            html1 = html1.replace(/{{terms}}/g, settings.site_url + '/pages/termsandconditions');
            html1 = html1.replace(/{{senderemail}}/g, template[0].sender_email);
            html1 = html1.replace(/{{logo}}/g, settings.site_url + settings.logo);
            html1 = html1.replace(/{{site_title}}/g, settings.site_title);

            var mailOptions1 = {
                from: data.email,
                to: template[0].sender_email,
                subject: template[0].email_subject,
                text: html1,
                html1: html1
            };

            mail.send(mailOptions1, function (err, response) { });
        });
        /*		End	[Email template]	*/
    }

    controller.deletecontact = function (req, res) {
        req.checkBody('delData', 'Invalid contact delData').notEmpty();
        var errors = req.validationErrors();
        if (errors) {
            res.send(errors, 400);
            return;
        }

        db.UpdateDocument('contact', { _id: { $in: req.body.delData } }, { status: 0 }, { multi: true }, function (err, docdata) {
            if (err) {
                res.send(err);
            } else {
                res.send(docdata);
            }
        });


    }
    return controller;
}
