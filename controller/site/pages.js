var express = require('express');
var bcrypt = require('bcrypt-nodejs');
var db = require('../../controller/adaptor/mongodb.js');
var attachment = require('../../model/attachments.js');
var middlewares = require('../../model/middlewares.js');
var async = require('async');

module.exports = function () {
	var router = {};
	router.getpage = function getpage(req, res) {
		db.GetOneDocument('pages', { slug: req.body.slug, status: 1 }, {}, {}, function (err, docdata) {
			if (err || !docdata) {
				res.send(err);
			} else {
				res.send(docdata);
			}
		});
	}

	router.getfaq = function getfaq(req, res) {
		db.GetDocument('faq', { $and: [{ status: { $ne: 0 } }, { status: { $ne: 2 } }] }, {}, {}, function (err, docdata) {
			if (err) {
				res.send(err);
			} else {
				res.send(docdata);
			}
		});
	};

	return router;
};
