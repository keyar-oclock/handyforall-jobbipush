module.exports = function (io) {

    var moment = require("moment");
    var db = require('../adaptor/mongodb.js');
    var push = require('../../model/pushNotification.js')(io);
    var multer = require('multer');
    var async = require("async");
    var mail = require('../../model/mail.js');
    var mongoose = require("mongoose");
    var fs = require('fs');
    var CONFIG = require('../../config/config');
    var stripe = require('stripe')('sk_test_1aQzKO9htQAEqlFPvigo717t');
    var url = require('url');
    var twilio = require('../../model/twilio.js');
    var library = require('../../model/library.js');


    var controller = {};
    controller.byCash = function (req, res) {
        var data = {};
        data.status = '0';
        var errors = [];
        req.checkBody('job_id', 'Job ID is Required').notEmpty();
        req.checkBody('user_id', 'User ID is Required').notEmpty();
        errors = req.validationErrors();
        if (errors) {
            res.send({
                "status": "0",
                "errors": errors[0].msg
            });
            return;
        }
        var user_id = req.body.user_id;
        var job_id = req.body.job_id;
        db.GetDocument('users', { '_id': user_id }, {}, {}, function (usersErr, usersRespo) {
            if (usersErr || !usersRespo) {
                data.response = 'Invalid  User';
                res.send(data);
            } else {
                db.GetDocument('task', { 'booking_id': job_id, 'user': user_id, "status": 6 }, {}, {}, function (bookErr, bookRespo) {
                    if (bookErr || bookRespo.length == 0) {
                        data.response = 'Payment is already completed';
                        res.send(data);
                    } else {
                        db.GetDocument('tasker', { '_id': bookRespo[0].tasker }, {}, {}, function (proErr, proRespo) {
                            if (proErr || proRespo.length == 0) {
                                data.response = 'Invalid Tasker';
                                res.send(data);
                            } else {
                                db.GetOneDocument('currencies', { 'default': 1 }, {}, {}, function (err, currencies) {
                                    if (err || !currencies) {
                                        res.send({
                                            "status": 0,
                                            "message": 'Error'
                                        });
                                    }
                                    else {
                                        db.GetOneDocument('settings', { 'alias': 'sms' }, {}, {}, function (err, settings) {
                                            if (err || !settings) {
                                                data.response = 'Configure your website settings';
                                                res.send(data);
                                            } else {
                                                if (bookRespo[0].invoice.amount.grand_total) {
                                                    if (bookRespo[0].invoice.amount.balance_amount) {
                                                        amount_to_receive = (bookRespo[0].invoice.amount.balance_amount).toFixed(2);
                                                    }
                                                    else {
                                                        amount_to_receive = (bookRespo[0].invoice.amount.grand_total).toFixed(2);
                                                    }
                                                }
                                                var transaction = {
                                                    'user': user_id,
                                                    'tasker': bookRespo[0].tasker,
                                                    'task': bookRespo[0]._id,
                                                    'type': 'pay by cash',
                                                    'amount': amount_to_receive,
                                                    'task_date': bookRespo[0].createdAt,
                                                    'status': 1
                                                };
                                                db.InsertDocument('transaction', transaction, function (err, transaction) {
                                                    if (err || transaction.nModified == 0) { data.response = 'Error in data, Please check your data'; res.send(data); }
                                                    else {
                                                        var transactions = [transaction._id];

                                                        db.UpdateDocument('task', { 'booking_id': job_id, 'user': user_id }, { $push: { transactions }, 'invoice.status': 1, 'status': 7, 'payment_type': 'cash' }, {}, function (err, upda) {
                                                            var provider_id = bookRespo[0].tasker;
                                                            var message = 'Payment Completed';
                                                            var amount_to_receive = 0.00;
                                                            var currency = currencies.code;
                                                            var options = {
                                                                'job_id': req.body.job_id,
                                                                'provider_id': provider_id,
                                                                'amount': amount_to_receive,
                                                                'currency': currency
                                                            };

                                                            push.sendPushnotification(bookRespo[0].user, message, 'payment_paid', 'ANDROID', options, 'USER', function (err, Response, body) { });
                                                            push.sendPushnotification(bookRespo[0].tasker, message, 'payment_paid', 'ANDROID', options, 'PROVIDER', function (err, Response, body) { });

                                                            data.status = '1';
                                                            data.response = 'Pay your bill by cash';
                                                            res.send(data);
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }



    controller.stripePaymentProcess = function (req, res) {

        var data = {};
        data.status = '0';
        var errors = [];
        req.checkBody('task_id', 'Job ID is Required').notEmpty();
        req.checkBody('user_id', 'User ID is Required').notEmpty();
        req.checkBody('card_number', 'Card_number is Required').notEmpty();
        req.checkBody('exp_month', 'Exp_month  is Required').notEmpty();
        req.checkBody('exp_year', 'Exp_year is Required').notEmpty();
        req.checkBody('cvc_number', 'Card_cvv no is Required').notEmpty();
        req.checkBody('transaction_id', 'Transaction_id no is Required').notEmpty();
        errors = req.validationErrors();
        if (errors) {
            res.send({
                "status": "0",
                "errors": errors[0].msg
            });
            return;
        }
        req.sanitizeBody('task_id').trim();
        req.sanitizeBody('user_id').trim();
        req.sanitizeBody('card_number').trim();
        req.sanitizeBody('exp_month').trim();
        req.sanitizeBody('exp_year').trim();
        req.sanitizeBody('cvc_number').trim();
        req.sanitizeBody('transaction_id').trim();
        var request = {};

        request.task = req.body.task_id.replace(/^"(.*)"$/, '$1');
        request.user = req.body.user_id.replace(/^"(.*)"$/, '$1');
        request.transaction_id = req.body.transaction_id.replace(/^"(.*)"$/, '$1');
        var card = {};
        card.number = req.body.card_number;
        card.exp_month = req.body.exp_month;
        card.exp_year = req.body.exp_year;
        card.cvc = req.body.cvc_number;
        async.waterfall([
            function (callback) {
                db.GetOneDocument('task', { '_id': request.task, 'status': 6 }, {}, {}, function (err, task) {
                    if (err || !task) {
                        data.response = 'Payment is already completed'; res.send(data);
                    }
                    else { callback(err, task); }
                });
            },
            function (task, callback) {
                db.GetOneDocument('tasker', { '_id': task.tasker }, {}, {}, function (err, tasker) {
                    if (err || !tasker) {
                        data.response = 'Invalid Tasker'; res.send(data);
                    }
                    else { callback(err, task, tasker); }
                });
            },
            function (task, tasker, callback) {
                db.GetOneDocument('users', { '_id': request.user }, {}, {}, function (err, user) {
                    if (err || !user) {
                        data.response = 'Invalid User'; res.send(data);
                    }
                    else { callback(err, task, tasker, user); }
                });
            },
            function (task, tasker, user, callback) {
                stripe.tokens.create({ card: card }, function (err, token) {
                    if (err || !token) {
                        res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                    }
                    else { callback(err, token, task, tasker); }
                });
            },
            function (token, task, tasker, callback) {
                var amount_to_receive = 0;
                if (task.invoice.amount.grand_total) {
                    if (task.invoice.amount.balance_amount) {
                        amount_to_receive = parseFloat(task.invoice.amount.balance_amount).toFixed(2);
                    }
                    else {
                        amount_to_receive = parseFloat(task.invoice.amount.grand_total).toFixed(2);
                    }
                }

                var test = parseInt(amount_to_receive * 100);
                stripe.charges.create({
                    amount: test,
                    currency: "usd",
                    source: token.id,
                    description: "Payment From User",
                }, function (err, charges) {
                    if (err || !charges) {
                        data.response = 'Error in stripe charge creation'; res.send(data);
                    }
                    else { callback(err, task, tasker, token, charges); }
                });
            },
            function (task, tasker, token, charges, callback) {
                var transactions = [{
                    'gateway_response': charges
                }];
                db.UpdateDocument('transaction', { '_id': request.transaction_id }, { 'transactions': transactions, 'status': 1 }, {}, function (err, transaction) {
                    if (err || transaction.nModified == 0) { data.response = 'Error in saving your data'; res.send(data); }
                    else { callback(err, task, tasker, token, charges); }
                });
            }
        ], function (err, task, tasker, token, charges) {
            if (err) {
                if (err) { data.response = 'Error in saving your data'; res.send(data); }
            } else {
                var dataToUpdate = {};
                dataToUpdate.status = 7;
                dataToUpdate.invoice = task.invoice;
                dataToUpdate.invoice.status = 1;
                dataToUpdate.invoice.amount.balance_amount = 0;
                dataToUpdate.payment_type = 'stripe';

                db.UpdateDocument('task', { _id: task._id }, dataToUpdate, function (err, docdata) {
                    if (err || docdata.nModified == 0) { data.response = 'Error in saving your data'; res.send(data); }
                    else {
                        var transactions = [request.transaction_id];
                        db.UpdateDocument('task', { _id: task._id }, { $push: { transactions } }, function (err, docdata) {
                            if (err || docdata.nModified == 0) { res.redirect("http://" + req.headers.host + '/mobile/payment/pay-failed'); }
                            else {
                                var message = 'Payment Completed';
                                var options = { 'job_id': task.booking_id, 'provider_id': task.tasker };
                                push.sendPushnotification(task.user, message, 'payment_paid', 'ANDROID', options, 'USER', function (err, Response, body) { });
                                push.sendPushnotification(task.tasker, message, 'payment_paid', 'ANDROID', options, 'PROVIDER', function (err, Response, body) { });
                                res.redirect("http://" + req.headers.host + '/mobile/payment/pay-completed/bycard');
                            }
                        });
                    }
                });
            }
        });
    }

    controller.byWallet = function (req, res) {
        var errors = [];
        req.checkBody('job_id', 'Job ID is Required').notEmpty();
        req.checkBody('user_id', 'User ID is Required').notEmpty();
        errors = req.validationErrors();
        if (errors) {
            res.send({ "status": "0", "errors": errors[0].msg });
            return;
        }
        var data = {};
        data.status = '0';
        var request = {};
        request.user_id = req.body.user_id;
        request.job_id = req.body.job_id;
        db.GetOneDocument('users', { '_id': request.user_id }, {}, {}, function (err, users) {
            if (err || users.length == 0) { data.response = 'Invalid users, Please check your data'; res.send(data); }
            else {
                db.GetOneDocument('currencies', { 'default': 1 }, {}, {}, function (err, currencies) {
                    if (err || !currencies) {
                        res.send({
                            "status": 0,
                            "message": 'Error'
                        });
                    }
                    else {
                        db.GetOneDocument('task', { 'booking_id': request.job_id, 'user': request.user_id, "status": 6 }, {}, {}, function (err, Bookings) {
                            if (err || !Bookings) { data.response = 'Job Invalid'; res.send(data); }
                            else {
                                db.GetOneDocument('walletReacharge', { "user_id": request.user_id }, {}, {}, function (err, wallet) {
                                    if (err || !wallet) { data.response = 'Invalid users, Please check your data'; res.send(data); }
                                    else {
                                        if (wallet.total == 0) {
                                            res.send({ 'status': '0', 'response': 'Sorry insufficient amount please recharge your wallet amount', 'Amount neeeds': Bookings.invoice.amount.total });
                                        } else if (wallet.total < Bookings.invoice.amount.grand_total) {
                                            var provider_id = Bookings.tasker;
                                            var wallet_amount = 0.00;
                                            var job_charge = 0.00;
                                            if (wallet.total) { wallet_amount = parseFloat(wallet.total); }
                                            if (Bookings.invoice.amount.grand_total) { job_charge = parseFloat(Bookings.invoice.amount.grand_total); }
                                            var balanceamount = {};
                                            balanceamount = job_charge - wallet_amount;
                                            var walletArr = {
                                                'type': 'DEBIT',
                                                'debit_type': 'payment',
                                                'ref_id': req.body.job_id,
                                                'trans_amount': parseFloat(wallet.total),
                                                'avail_amount': 0,
                                                'due_amount': job_charge - wallet_amount,
                                                'trans_date': new Date(),
                                                'trans_id': mongoose.Types.ObjectId()
                                            };
                                            db.UpdateDocument('walletReacharge', { 'user_id': req.body.user_id }, { $push: { transactions: walletArr }, $set: { "total": 0 } }, { multi: true }, function (walletUErr, walletURespo) {

                                                if (walletUErr || walletURespo.nModified == 0) {
                                                    data.response = 'Error in data, Please check your data'; res.send(data);
                                                }
                                                else {
                                                    db.UpdateDocument('task', { "booking_id": request.job_id }, { "invoice.amount.balance_amount": balanceamount, "invoice.amount.grand_total": balanceamount, "payment_type": "wallet-gateway" }, function (err, docdata) {

                                                        if (err || docdata.nModified == 0) { data.response = 'Error data, Please check your data'; res.send(data); }
                                                        else {
                                                            var transaction = {
                                                                'user': request.user_id,
                                                                'tasker': Bookings.tasker,
                                                                'task': Bookings._id,
                                                                'type': 'wallet-gateway',
                                                                'amount': wallet.total,
                                                                //'amount':  Bookings.invoice.amount.grand_total,
                                                                'task_date': Bookings.createdAt,
                                                                'status': 1
                                                            };


                                                            db.InsertDocument('transaction', transaction, function (err, transaction) {
                                                                if (err || transaction.nModified == 0) { data.response = 'Error in data, Please check your data'; res.send(data); }
                                                                else {
                                                                    var message = 'Payment Completed';
                                                                    var options = { 'job_id': request.job_id, 'provider_id': provider_id };

                                                                    // push.sendPushnotification(Bookings.user, message, 'payment_paid', 'ANDROID', options, 'USER', function (err, Response, body) { });
                                                                    // push.sendPushnotification(provider_id, message, 'payment_paid', 'ANDROID', options, 'PROVIDER', function (err, Response, body) { });
                                                                    res.send({
                                                                        'status': '1',
                                                                        'response': 'Transaction partially completed due to insufficient balance in your wallet account,Complete the transaction by recharging the wallet account or by using credit card.!!',
                                                                        'due_amount': ((job_charge - wallet_amount) * currencies.value).toFixed(2),
                                                                        'used_amount': (wallet_amount * currencies.value).toFixed(2),
                                                                        'available_wallet_amount': '0'
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        } else {
                                            var provider_id = Bookings.tasker;
                                            var walletArr = {
                                                'type': 'DEBIT',
                                                'debit_type': 'payment',
                                                'ref_id': req.body.job_id,
                                                'trans_amount': parseFloat(Bookings.invoice.amount.grand_total),
                                                'avail_amount': wallet.total - Bookings.invoice.amount.grand_total,
                                                'trans_date': new Date(),
                                            };
                                            var totalwallet = wallet_amount - job_charge;
                                            db.UpdateDocument('walletReacharge', { 'user_id': request.user_id }, { $push: { transactions: walletArr }, $set: { "total": parseFloat(wallet.total - Bookings.invoice.amount.grand_total) } }, { multi: true }, function (walletUErr, walletURespo) {
                                                if (walletUErr || walletURespo.nModified == 0) {

                                                    data.response = 'Error in data, Please check your data'; res.send(data);
                                                }
                                                else {
                                                    var transaction = {
                                                        'user': request.user_id,
                                                        'tasker': Bookings.tasker,
                                                        'task': Bookings._id,
                                                        'type': 'wallet-gateway',
                                                        'amount': Bookings.invoice.amount.grand_total,
                                                        'task_date': Bookings.createdAt,
                                                        'status': 1
                                                    };
                                                    db.InsertDocument('transaction', transaction, function (err, transaction) {
                                                        if (err || transaction.nModified == 0) {
                                                            data.response = 'Error in data, Please check your data'; res.send(data);
                                                        }
                                                        else {
                                                            var transactions = [transaction._id];
                                                            db.UpdateDocument('task', { "booking_id": req.body.job_id }, { $push: { transactions }, 'invoice.status': '1', 'status': '7', 'payment_type': 'wallet' }, function (err, docdata) {
                                                                if (err || docdata.nModified == 0) {
                                                                    data.response = 'Error in data, Please check your data'; res.send(data);
                                                                }
                                                                else {
                                                                    var message = 'Payment Completed';
                                                                    var options = { 'job_id': request.job_id, 'provider_id': provider_id };
                                                                    push.sendPushnotification(Bookings.user, message, 'payment_paid', 'ANDROID', options, 'USER', function (err, Response, body) { });
                                                                    push.sendPushnotification(provider_id, message, 'payment_paid', 'ANDROID', options, 'PROVIDER', function (err, Response, body) { });
                                                                    res.send({
                                                                        'status': '1',
                                                                        'message': 'Payment Completed Successfully',
                                                                        'response': 'Wallet amount used successfully',
                                                                        'used_amount': (Bookings.invoice.amount.grand_total * currencies.value).toFixed(2),
                                                                        'available_wallet_amount': ((wallet.total - Bookings.invoice.amount.grand_total) * currencies.value).toFixed(2)
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    controller.byGateway = function (req, res) {
        var errors = [];
        req.checkBody('job_id', 'Job ID is Required').notEmpty();
        req.checkBody('user_id', 'User ID is Required').notEmpty();
        req.checkBody('gateway', 'Gateway ID is Required').notEmpty();
        errors = req.validationErrors();
        if (errors) {
            res.send({
                "status": "0",
                "errors": errors[0].msg
            });
            return;
        }
        var data = {};
        data.user_id = req.body.user_id;
        data.job_id = req.body.job_id;
        data.payment = req.body.gateway;

        req.sanitizeBody('job_id').trim();
        req.sanitizeBody('user_id').trim();
        req.sanitizeBody('gateway').trim();

        var request = {};
        request.job_id = req.body.job_id;
        request.user_id = req.body.user_id;
        request.gateway = req.body.gateway;

        var extension = {};
        extension.populate = 'tasker';
        db.GetOneDocument('task', { 'booking_id': request.job_id, 'user': request.user_id, 'status': 6 }, {}, extension, function (err, task) {
            if (err || !task) {
                res.send({
                    'status': '0',
                    'response': 'INVALID DATA'
                });
            } else {
                if (task.tasker) {
                    db.GetDocument('paymentgateway', { 'alias': req.body.gateway, 'status': 1 }, {}, {}, function (paymentErr, paymentRespo) {
                        if (paymentErr || !paymentRespo) {
                            res.send({
                                'status': '0',
                                'response': 'INVALID  DATA'
                            });
                        } else {
                            if (task.invoice.amount.grand_total) {
                                if (task.invoice.amount.balance_amount) {
                                    amount_to_receive = (task.invoice.amount.balance_amount).toFixed(2);
                                }
                                else {
                                    amount_to_receive = (task.invoice.amount.grand_total).toFixed(2);
                                }
                                var transaction = {};
                                transaction.user = request.user_id;
                                transaction.tasker = task.tasker._id;
                                transaction.task = task._id;
                                transaction.type = request.gateway;
                                transaction.amount = amount_to_receive;
                                transaction.task_date = task.createdAt;
                                transaction.status = 2
                                db.InsertDocument('transaction', transaction, function (err, transaction) {
                                    if (err || transaction.nModified == 0) { data.response = 'Error in saving your data'; res.send(data); }
                                    else {
                                        res.send({
                                            'status': '1',
                                            'job_id': request.job_id,
                                            'mobile_id': transaction._id,

                                        });
                                    }
                                });
                                //----------------Transcation status 2
                            }
                        }
                    });
                } else {
                    res.send({
                        'status': '0',
                        'response': 'INVALID DATA'
                    });
                }
            }
        });
    }



    controller.applyCoupon = function (req, res) {
        var status = '0';
        var response = '';
        try {
            var data = {};
            data.user_id = req.body.user_id;
            data.code = req.body.code;
            data.reach_date = req.body.pickup_date;
            var errors = [];
            req.checkBody('user_id', 'User ID is Required').notEmpty();
            req.checkBody('code', 'Coupon code is Required').notEmpty();
            req.checkBody('pickup_date', 'Pick Up Date is Required').notEmpty();
            errors = req.validationErrors();
            if (errors) {
                res.send({
                    "status": "0",
                    "response": errors[0].msg
                });
                return;
            }
            if (Object.keys(req.body).length >= 3) {
                db.GetOneDocument('users', { _id: req.body.user_id }, {}, {}, function (err, userRespo) {
                    if (userRespo) {
                        db.GetOneDocument('coupon', { code: req.body.code }, {}, {}, function (promoErr, promoRespo) {
                            if (err || !promoRespo) {
                                res.send({ "status": "0", "response": "Invalid Coupon" });
                            } else {
                                var valid_from = promoRespo.valid_from;
                                var valid_to = promoRespo.expiry_date;
                                var date_time = new Date(req.body.pickup_date);

                                if ((Date.parse(valid_from) <= Date.parse(date_time)) && (Date.parse(valid_to) >= Date.parse(date_time))) {
                                    //if (promoRespo.total_coupons > promoRespo.per_user) {
                                    /*
                                    var coupon_usage = [];
                                    var coupon_count = 0;
                                    if (promoRespo.usage) {
                                        coupon_usage = promoRespo[0].usage;
                                        for (var i = 0; i < promoRespo[0].usage.length; i++) {
                                            if (promoRespo[0].usage[i].user_id && promoRespo[0].usage[i].user_id == req.body.user_id) {
                                                coupon_count++;
                                            }
                                        }
                                    }
                                    if (coupon_count <= promoRespo[0].user_usage) {
                                    */
                                    res.send({
                                        "status": "1",
                                        "response": [{ "message": "Coupon code applied.", "code": req.body.code }]
                                    });
                                    /*
                                    }
                                    } else {
                                        res.send({ "status": "0", "response": "Coupon Expired" });
                                    }
                                    */
                                } else {
                                    res.send({ "status": "0", "response": "Coupon Expired" });
                                }
                            }
                        });
                    } else {
                        res.send({ "status": "0", "response": "Invalid User" });

                    }
                });
            } else {
                res.send({
                    "status": "0",
                    "response": "Some Parameters are missing"
                });
            }
        } catch (e) {

            res.send({
                "status": status,
                "response": "Error in connection"
            });
        }
    };


    return controller;

}
