module.exports = function (io) {

    var moment = require("moment");
    var db = require('../adaptor/mongodb.js');
    var push = require('../../model/pushNotification.js')(io);
    var async = require("async");
    var mongoose = require("mongoose");
    var CONFIG = require('../../config/config');
    var sha512 = require('sha512');
    var querystring = require('querystring');
    var http = require('http');
    var crypto = require('crypto');


    var controller = {};

    controller.remitaPaymentProcess = function (req, res) {

        var data = {};
        data.status = 0;

        req.checkBody('task_id', 'task_id is Invalid').notEmpty();
        req.checkBody('user_id', 'user_id is Invalid').notEmpty();
        req.checkBody('total_amount', 'total_amount is Invalid').notEmpty();
        req.checkBody('payername', 'payername is Invalid').notEmpty();
        req.checkBody('payeremail', 'payeremail is Invalid').notEmpty();
        req.checkBody('payerphone', 'payerphone is Invalid').notEmpty();

        var errors = req.validationErrors();
        if (errors) { data.response = errors[0].msg; res.send(data); return; }

        req.sanitizeBody('task_id').trim();
        req.sanitizeBody('user_id').trim();
        req.sanitizeBody('total_amount').trim();
        req.sanitizeBody('payername').trim();
        req.sanitizeBody('payeremail').trim();
        req.sanitizeBody('payerphone').trim();

        var request = {};
        request.task = req.body.task_id.replace(/^"(.*)"$/, '$1');
        request.user = req.body.user_id.replace(/^"(.*)"$/, '$1');

        request.amount = req.body.total_amount;
        request.payername = req.body.payername;
        request.payeremail = req.body.payeremail;
        request.payerphone = req.body.payerphone;

        var options = {};
        options.populate = 'user category tasker';
        db.GetOneDocument('task', { '_id': request.task }, {}, options, function (err, task) {
            if (err || task.length == 0) {
                res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
            } else {
                async.waterfall([
                    function (callback) {
                        db.GetOneDocument('paymentgateway', { status: { $ne: 0 }, alias: 'remita' }, {}, {}, function (err, paymentgateway) {
                            if (err) {
                                res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                            }
                            else {
                                callback(err, paymentgateway);
                            }

                        });
                    },
                    function (paymentgateway, callback) {
                        var transaction = {};
                        transaction.user = task.user;
                        transaction.tasker = task.tasker;
                        transaction.task = request.task;
                        transaction.type = 'remita';
                        if (transaction.amount = task.invoice.amount.balance_amount) {
                            transaction.amount = task.invoice.amount.balance_amount;
                        } else {
                            transaction.amount = task.invoice.amount.grand_total;
                        }
                        transaction.amount = task.invoice.amount.balance_amount;
                        transaction.task_date = task.createdAt;
                        transaction.status = 1;
                        db.InsertDocument('transaction', transaction, function (err, transaction) {
                            request.transaction_id = transaction._id;
                            request.trans_date = transaction.createdAt;
                            request.avail_amount = transaction.amount;
                            request.credit_type = transaction.type;
                            callback(err, paymentgateway, transaction);
                        });
                    },
                    function (paymentgateway, transaction, callback) {
                        db.GetOneDocument('settings', { 'alias': 'general' }, {}, {}, function (err, settings) {
                            if (err || !settings) {
                                //  data.response = 'Configure your website settings'; res.send(data);
                            }
                            else { callback(err, paymentgateway, transaction, settings.settings); }
                        });
                    },
                    function (paymentgateway, transaction, settings, callback) {
                        db.GetDocument('emailtemplate', { name: { $in: ['PaymentDetailstoAdmin', 'PaymentDetailstoTasker', 'PaymentDetailstoUser'] }, 'status': { $ne: 0 } }, {}, {}, function (err, template) {
                            if (err || !template) {
                                res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                                // data.response = 'Unable to get email template'; res.send(data);
                            }
                            else { callback(err, paymentgateway, transaction, settings, template); }
                        });
                    }
                ], function (err, paymentgateway, transaction, settings, template) {
                    if (err) {
                        res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                    } else {
                        var data = {};
                        var currentdate = new Date();
                        data.status = 1;
                        data.merchantId = paymentgateway.settings.merchant_id;
                        data.serviceTypeId = paymentgateway.settings.service_typeid;
                        data.api_key = paymentgateway.settings.api_key;
                        data.mode = paymentgateway.settings.mode;
                        data.amt = request.amount;
                        data.payerName = request.payername;
                        data.payerEmail = request.payeremail;
                        data.payerPhone = request.payerphone;
                        data.orderId = currentdate.getTime();
                        data.responseurl = settings.site_url + "mobile/app/payment/remita-execute-payment-process?task=" + request.task + "&transaction=" + transaction._id;
                        console.log(data.responseurl);

                        var concatString = data.merchantId + data.serviceTypeId + data.orderId + data.amt + data.responseurl + data.api_key;
                        var hashing = sha512(concatString);
                        data.hash = hashing.toString('hex');
                        data = querystring.stringify(data);
                        var url = "http://www.remitademo.net/remita/ecomm" + '/' + data.merchantId + '/' + data.orderId + '/' + data.hash + '/' + 'orderstatus.reg';

                        var options = {
                            protocol: 'http:',
                            host: 'www.remitademo.net',
                            path: '/remita/ecomm/init.reg',
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded',
                                'Content-Length': Buffer.byteLength(data)
                            }
                        };

                        var output = {};
                        var req = http.request(options, (result) => {

                            output.redirectUrl = result.headers.location;
                            output.status = 1;
                            output.payment_mode = 'remita';
                            output.task = request.task;
                            output.user = request.user;
                            result.setEncoding('utf8');

                            result.on('data', (chunk) => {
                                //  console.log(`BODY: ${chunk}`);
                            });

                            result.on('end', () => {
                                /*console.log("-------------", output);*/
                                //res.send(output);
                                res.redirect(result.headers.location);
                            });
                        });
                        req.on('error', (e) => {
                            console.log(`problem with request: ${e.message}`);
                        });
                        req.write(data);
                        req.end();
                    }
                });
            }
        });
    }

    controller.executeremita = function executeremita(req, res) {

        var data = {};
        data.status = 0;
        var request = {};
        request.task = req.query.task[0];
        request.transaction = req.query.transaction[0];

        var options = {};
        options.populate = 'tasker user task';
        db.GetOneDocument('transaction', { _id: request.transaction }, {}, options, function (err, transaction) {
            if (err) {
                res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
            } else {
                async.waterfall([
                    function (callback) {
                        db.GetOneDocument('paymentgateway', { status: { $ne: 0 }, alias: 'remita' }, {}, {}, function (err, paymentgateway) {
                            callback(err, paymentgateway);
                        });
                    },
                    function (paymentgateway, callback) {
                        db.GetOneDocument('transaction', { _id: request.transaction }, {}, options, function (err, transaction) {
                            if (err || !transaction) {
                                res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                            }
                            else { callback(err, paymentgateway, transaction); }
                        });
                    },
                    function (paymentgateway, transaction, callback) {
                        db.GetOneDocument('settings', { 'alias': 'general' }, {}, {}, function (err, settings) {
                            if (err || !settings) {
                                res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                            }
                            else { callback(err, paymentgateway, transaction, settings.settings); }
                        });
                    },
                    function (paymentgateway, transaction, settings, callback) {
                        db.GetDocument('emailtemplate', { name: { $in: ['PaymentDetailstoAdmin', 'PaymentDetailstoTasker', 'PaymentDetailstoUser'] }, 'status': { $ne: 0 } }, {}, {}, function (err, template) {
                            if (err || !template) {
                                res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                            }
                            else { callback(err, paymentgateway, transaction, settings, template); }
                        });
                    }
                ], function (err, paymentgateway, transaction, settings, template) {
                    if (err) {
                        res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                    } else {
                        data.merchantId = paymentgateway.settings.merchant_id;
                        data.serviceTypeId = paymentgateway.settings.service_typeid;
                        data.api_key = paymentgateway.settings.api_key;
                        var concatString = req.query.orderID + data.api_key + data.merchantId;
                        var hashing = sha512(concatString);
                        data.hash = hashing.toString('hex');
                        var url = 'http://www.remitademo.net/remita/ecomm' + '/' + data.merchantId + '/' + req.query.orderID + '/' + data.hash + '/' + 'orderstatus.reg';

                        return http.get({
                            protocol: 'http:',
                            host: 'www.remitademo.net',
                            path: url
                        }, function (response) {
                            var body = '';
                            response.on('data', function (d) {
                                body += d;
                            });
                            response.on('end', function () {
                                try {
                                    var parsed = JSON.parse(body);
                                    if (parsed.status == '00' || parsed.status == '01') {
                                        var dataToUpdate = {};
                                        dataToUpdate.status = 7;
                                        dataToUpdate.invoice = transaction.task.invoice;
                                        dataToUpdate.invoice.status = 1;
                                        dataToUpdate.payee_status = 0;
                                        dataToUpdate.invoice.amount.balance_amount = parseFloat(transaction.task.invoice.amount.balance_amount) - parseFloat(transaction.task.invoice.amount.balance_amount);
                                        dataToUpdate.payment_type = 'remitas';

                                        var transactionsData = [{
                                            'gateway_response': parsed
                                        }];

                                        console.log('Let Start Transcation');

                                        db.UpdateDocument('transaction', { '_id': request.transaction_id }, { 'transactions': transactionsData }, {}, function (err, transactionUpdate) {
                                            if (err) {
                                                res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                                            } else {
                                                db.UpdateDocument('task', { _id: transaction.task._id }, dataToUpdate, function (err, docdata) {
                                                    if (err) {
                                                        res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                                                    } else {
                                                        var options = {};
                                                        options.populate = 'tasker task user';
                                                        db.GetOneDocument('task', { _id: request.task }, {}, options, function (err, docdata) {
                                                            if (err || !docdata) {
                                                                res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                                                            } else {
                                                                var notifications = { 'job_id': docdata.booking_id, 'user_id': docdata.tasker._id };
                                                                var message = 'Your billing amount paid successfully';
                                                                push.sendPushnotification(docdata.tasker._id, message, 'payment_paid', 'ANDROID', notifications, 'PROVIDER', function (err, response, body) { });
                                                                res.redirect("http://" + req.headers.host + '/mobile/payment/pay-completed/bycard');
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    } else {
                                        res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                                    }
                                } catch (e) {
                                    res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                                }
                            });
                        });
                    }
                });
            }
        });
    }

    controller.remitapayform = function (req, res) {

        var data = {};
        data.status = '0';
        req.checkQuery('user_id', 'User ID is Required').notEmpty();
        req.checkQuery('total_amount', 'Total Amount is Required').notEmpty();
        var errors = req.validationErrors();

        if (errors) { data.response = errors[0].msg; res.send(data); return; }

        req.sanitizeQuery('user_id').trim();
        req.sanitizeQuery('total_amount').trim();

        var request = {};
        request.user_id = req.query.user_id.replace(/^"(.*)"$/, '$1');
        request.total_amount = req.query.total_amount;

        async.waterfall([
            function (callback) {
                db.GetOneDocument('settings', { "alias": "general" }, { 'settings': 1 }, {}, function (err, settings) {
                    callback(err, settings);
                });
            },
            function (settings, callback) {
                db.GetOneDocument('users', { '_id': request.user_id }, {}, {}, function (err, user) {
                    callback(err, settings, user);
                });
            },
            function (settings, user, callback) {
                var transaction = {
                    'user': request.user_id,
                    'type': 'remita',
                    'amount': request.total_amount,
                    'status': 1
                };
                db.InsertDocument('transaction', transaction, function (err, transaction) {
                    callback(err, settings, user, transaction);
                });
            }
        ], function (err, settings, user, transaction) {
            if (err) {
                res.render('mobile/payment-failed', { image: settings.settings.site_url + 'app/mobile/images/failed.png' });
            } else {
                db.GetOneDocument('currencies', { 'default': 1 }, {}, {}, function (err, currencies) {
                    if (err || !currencies) {
                        res.render('mobile/payment-failed', { image: settings.settings.site_url + 'app/mobile/images/failed.png' });
                    }
                    else {
                        var pug = {};
                        pug.transaction = transaction;
                        pug.currencies = currencies.symbol;
                        pug.site_url = settings.settings.site_url;
                        res.render('mobile/remita_recharge_payment_card', pug);
                    }
                });
            }
        });
    }

    controller.rechargeremitaProcess = function (req, res) {

        var data = {};
        data.status = 0;

        req.checkBody('user_id', 'user_id is Invalid').notEmpty();
        req.checkBody('total_amount', 'total_amount is Invalid').notEmpty();
        req.checkBody('payername', 'payername is Invalid').notEmpty();
        req.checkBody('payeremail', 'payeremail is Invalid').notEmpty();
        req.checkBody('payerphone', 'payerphone is Invalid').notEmpty();

        var errors = req.validationErrors();
        if (errors) { data.response = errors[0].msg; res.send(data); return; }

        req.sanitizeBody('task_id').trim();
        req.sanitizeBody('user_id').trim();
        req.sanitizeBody('total_amount').trim();
        req.sanitizeBody('payername').trim();
        req.sanitizeBody('payeremail').trim();
        req.sanitizeBody('payerphone').trim();

        var request = {};
        request.user = req.body.user_id.replace(/^"(.*)"$/, '$1');
        request.transaction_id = req.body.transaction_id.replace(/^"(.*)"$/, '$1');

        request.amount = req.body.total_amount;
        request.payername = req.body.payername;
        request.payeremail = req.body.payeremail;
        request.payerphone = req.body.payerphone;

        async.waterfall([
            function (callback) {
                db.GetOneDocument('paymentgateway', { status: { $ne: 0 }, alias: 'remita' }, {}, {}, function (err, paymentgateway) {
                    if (err) {
                        res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                    } else {
                        callback(err, paymentgateway);
                    }
                });
            },
            function (paymentgateway, callback) {
                db.GetOneDocument('transaction', { '_id': request.transaction_id }, {}, {}, function (err, transaction) {
                    if (err || !transaction) {
                        //console.log('error--->>2')
                        res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                    }
                    else { callback(err, paymentgateway, transaction); }
                });
            },
            function (paymentgateway, transaction, callback) {
                db.GetOneDocument('settings', { 'alias': 'general' }, {}, {}, function (err, settings) {
                    if (err || !settings) {
                        //  data.response = 'Configure your website settings'; res.send(data);
                        res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                    }
                    else { callback(err, paymentgateway, transaction, settings.settings); }
                });
            },
            function (paymentgateway, transaction, settings, callback) {
                db.GetDocument('emailtemplate', { name: { $in: ['PaymentDetailstoAdmin', 'PaymentDetailstoTasker', 'PaymentDetailstoUser'] }, 'status': { $ne: 0 } }, {}, {}, function (err, template) {
                    if (err || !template) {
                        // console.log('error--->>3')
                        res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');

                    }
                    else { callback(err, paymentgateway, transaction, settings, template); }
                });
            }
        ], function (err, paymentgateway, transaction, settings, template) {
            if (err) {
                // console.log('error--->>4')
                res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
            } else {
                var data = {};
                var currentdate = new Date();
                data.status = 1;
                data.merchantId = paymentgateway.settings.merchant_id;
                data.serviceTypeId = paymentgateway.settings.service_typeid;
                data.api_key = paymentgateway.settings.api_key;
                data.mode = paymentgateway.settings.mode;
                data.amt = request.amount;
                data.payerName = request.payername;
                data.payerEmail = request.payeremail;
                data.payerPhone = request.payerphone;
                data.orderId = currentdate.getTime();
                data.responseurl = settings.site_url + "mobile/app/payment/recharge-remita-execute-payment-process?transaction=" + transaction._id + "&user_id=" + request.user;
                var concatString = data.merchantId + data.serviceTypeId + data.orderId + data.amt + data.responseurl + data.api_key;
                var hashing = sha512(concatString);
                data.hash = hashing.toString('hex');
                data = querystring.stringify(data);
                var url = "http://www.remitademo.net/remita/ecomm" + '/' + data.merchantId + '/' + data.orderId + '/' + data.hash + '/' + 'orderstatus.reg';

                var options = {
                    protocol: 'http:',
                    host: 'www.remitademo.net',
                    path: '/remita/ecomm/init.reg',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Content-Length': Buffer.byteLength(data)
                    }
                };

                var output = {};
                var req = http.request(options, (result) => {
                    output.redirectUrl = result.headers.location;
                    output.status = 1;
                    output.payment_mode = 'remita';
                    output.user = request.user;
                    result.setEncoding('utf8');

                    result.on('data', (chunk) => {
                        //  console.log(`BODY: ${chunk}`);
                    });

                    result.on('end', () => {
                        /*console.log("-------------", output);*/
                        //res.send(output);
                        res.redirect(result.headers.location);
                    });
                });
                req.on('error', (e) => {
                    // console.log(`problem with request: ${e.message}`);
                });
                req.write(data);
                req.end();
            }
        });
    }

    controller.rechargeexecuteremita = function rechargeexecuteremita(req, res) {

        var data = {};
        data.status = 0;
        var request = {};
        request.transaction = req.query.transaction[0];
        request.user_id = req.query.user_id[0];
        async.waterfall([
            function (callback) {
                db.GetOneDocument('paymentgateway', { status: { $ne: 0 }, alias: 'remita' }, {}, {}, function (err, paymentgateway) {
                    if (err || !paymentgateway) {
                        res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                    }
                    else {
                        callback(err, paymentgateway);
                    }
                });
            },
            function (paymentgateway, callback) {
                db.GetOneDocument('transaction', { _id: request.transaction }, {}, {}, function (err, transaction) {
                    request.transaction_id = transaction._id;
                    request.trans_id = transaction._id;
                    request.trans_date = transaction.createdAt;
                    request.avail_amount = transaction.amount;
                    request.credit_type = transaction.type;
                    if (err || !transaction) {
                        //console.log('error--->>6')
                        res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                    }
                    else { callback(err, paymentgateway, transaction); }
                });
            },
            function (paymentgateway, transaction, callback) {
                db.GetOneDocument('settings', { 'alias': 'general' }, {}, {}, function (err, settings) {
                    if (err || !settings) {
                        //console.log('error--->>7')
                        res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                    }
                    else { callback(err, paymentgateway, transaction, settings.settings); }
                });
            },
            function (paymentgateway, transaction, settings, callback) {
                db.GetDocument('emailtemplate', { name: { $in: ['PaymentDetailstoAdmin', 'PaymentDetailstoTasker', 'PaymentDetailstoUser'] }, 'status': { $ne: 0 } }, {}, {}, function (err, template) {
                    if (err || !template) {
                        // console.log('error--->>8')
                        res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                    }
                    else { callback(err, paymentgateway, transaction, settings, template); }
                });
            }
        ], function (err, paymentgateway, transaction, settings, template) {
            if (err) {
                // console.log('error--->>9')
                res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
            } else {
                data.merchantId = paymentgateway.settings.merchant_id;
                data.serviceTypeId = paymentgateway.settings.service_typeid;
                data.api_key = paymentgateway.settings.api_key;
                var concatString = req.query.orderID + data.api_key + data.merchantId;
                var hashing = sha512(concatString);
                data.hash = hashing.toString('hex');
                var url = 'http://www.remitademo.net/remita/ecomm' + '/' + data.merchantId + '/' + req.query.orderID + '/' + data.hash + '/' + 'orderstatus.reg';

                return http.get({
                    protocol: 'http:',
                    host: 'www.remitademo.net',
                    path: url
                }, function (response) {
                    var body = '';
                    response.on('data', function (d) {
                        body += d;
                    });
                    response.on('end', function () {

                        var parsed = JSON.parse(body);
                        if (parsed.status == '00' || parsed.status == '01') {
                            var transactionsData = [{
                                'gateway_response': parsed
                            }];
                            db.UpdateDocument('transaction', { '_id': request.transaction_id }, { 'transactions': transactionsData }, {}, function (err, transactionUpdate) {
                                if (err) {
                                    // console.log('error--->>10')
                                    res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                                } else {
                                    db.GetOneDocument('walletReacharge', { "user_id": request.user_id }, {}, {}, function (err, wallet) {
                                        var transactions = {
                                            'trans_id': request.trans_id,
                                            'trans_date': request.trans_date,
                                            'trans_amount': request.avail_amount,
                                            'avail_amount': 0,
                                            'type': 'CREDIT'
                                        };
                                        if (wallet) {
                                            transactions.avail_amount = parseInt(wallet.total) + parseInt(request.avail_amount);
                                            db.UpdateDocument('walletReacharge', { 'user_id': request.user_id }, { total: transactions.avail_amount, $push: { 'transactions': transactions } }, {}, function (err, docdata) {
                                                if (err || docdata.nModified == 0) {
                                                    res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                                                } else {
                                                    var payment = {};
                                                    payment.user = request.user_id;
                                                    payment.transaction = request.transaction_id;
                                                    payment.image = settings.site_url + 'app/mobile/images/success.png';
                                                    res.redirect("http://" + req.headers.host + '/mobile/payment/pay-completed/remitacard');
                                                }
                                            });
                                        } else {
                                            transactions.avail_amount = request.avail_amount;
                                            var insertdata = {};
                                            insertdata.user_id = request.user_id;
                                            insertdata.total = request.avail_amount;
                                            insertdata.transactions = [];
                                            insertdata.transactions.push(transactions);
                                            db.InsertDocument('walletReacharge', insertdata, function (err, docdata) {
                                                if (err || docdata.nModified == 0) {
                                                    // console.log('error--->>12')
                                                    res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                                                } else {
                                                    var payment = {};
                                                    payment.user = request.user_id;
                                                    payment.transaction = request.transaction_id;
                                                    //  payment.charge = charges.id;
                                                    payment.image = settings.site_url + 'app/mobile/images/success.png';
                                                    res.redirect("http://" + req.headers.host + '/mobile/payment/pay-completed/remitacard');
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        } else {
                            res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
                        }
                    });
                });
            }
        });
    }

    controller.remitacardpaymentsuccess = function (req, res) {
        db.GetOneDocument('settings', { "alias": "general" }, { 'settings': 1 }, {}, function (err, settings) {
            var payment = {};
            payment.image = settings.settings.site_url + 'app/mobile/images/success.png';
            res.render('mobile/remitapayment-successbycard', payment);
        });
    };

    return controller;
}
