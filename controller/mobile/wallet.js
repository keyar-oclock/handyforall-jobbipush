"use strict";

module.exports = function (io) {

	var async = require("async");
	var moment = require("moment");
	var stripe = require('stripe')('sk_test_1aQzKO9htQAEqlFPvigo717t');
	var paypal = require('paypal-rest-sdk');
	var objectID = require('mongodb').ObjectID;
	var mail = require('../../model/mail.js');
	var mailcontent = require('../../model/mailcontent.js');
	var mongoose = require("mongoose");
	var push = require('../../model/pushNotification.js')(io);
	var db = require('../adaptor/mongodb.js');

	var controller = {};

	controller.settings = function (req, res) {
		var data = {};
		data.status = '0';
		db.GetOneDocument('settings', { "alias": "general" }, { 'settings.wallet': 1 }, {}, function (err, wallet) {
			if (err || !wallet.settings.wallet.amount.minimum || !wallet.settings.wallet.amount.maximum) {
				data.response = 'Wallet Money Settings are not Available, Please try Again Later.';
				res.send(data);
			} else {
				data.status = '1';
				data.wallter_money = { 'wallet_min_amount': wallet.settings.wallet.amount.minimum, 'wallet_max_amount': wallet.settings.wallet.amount.maximum };
				res.send(data);
			}
		});
	};

	controller.payform = function (req, res) {

		var data = {};
		data.status = '0';
		req.checkQuery('user_id', 'User ID is Required').notEmpty();
		req.checkQuery('total_amount', 'Total Amount is Required').notEmpty();
		var errors = req.validationErrors();

		if (errors) { data.response = errors[0].msg; res.send(data); return; }

		req.sanitizeQuery('user_id').trim();
		req.sanitizeQuery('total_amount').trim();

		var request = {};
		request.user_id = req.query.user_id.replace(/^"(.*)"$/, '$1');
		request.total_amount = req.query.total_amount;

		async.waterfall([
			function (callback) {
				db.GetOneDocument('settings', { "alias": "general" }, { 'settings': 1 }, {}, function (err, settings) {
					callback(err, settings);
				});
			},
			function (settings, callback) {
				db.GetOneDocument('users', { '_id': request.user_id }, {}, {}, function (err, user) {
					callback(err, settings, user);
				});
			},
			function (settings, user, callback) {
				var transaction = {
					'user': request.user_id,
					'type': 'wallet',
					'amount': request.total_amount,
					'status': 1
				};
				db.InsertDocument('transaction', transaction, function (err, transaction) {
					callback(err, settings, user, transaction);
				});
			}
		], function (err, settings, user, transaction) {
			if (err) {
				res.render('mobile/payment-failed', { image: settings.settings.site_url + 'app/mobile/images/failed.png' });
			} else {
				var pug = {};
				pug.transaction = transaction;
				pug.site_url = settings.settings.site_url;
				res.render('mobile/wallet-stripe-payment-card', pug);
			}
		});
	}

	controller.stripeProcess = function (req, res) {
		var data = {};
		data.status = '0';

		req.checkBody('user_id', 'User ID is Required').notEmpty();
		req.checkBody('transaction_id', 'Transaction ID is Required').optional();
		req.checkBody('stripeEmail', 'Stripe Email is Required').optional();
		req.checkBody('stripeToken', 'Stripe Token is Required').optional();
		req.checkBody('total_amount', 'Total Amount is Required').notEmpty();

		req.checkBody('card_number', 'card_number ID is Required').optional();
		req.checkBody('exp_month', 'exp_month  is Required').optional();
		req.checkBody('exp_year', 'exp_year  is Required').optional();
		req.checkBody('cvc_number', 'cvc_number  is Required').optional();

		errors = req.validationErrors();
		var errors = req.validationErrors();
		if (errors) { data.response = errors[0].msg; res.send(data); return; }

		req.sanitizeBody('user_id').trim();
		//req.sanitizeBody('job_id').trim();
		req.sanitizeBody('transaction_id').trim();
		req.sanitizeBody('stripeEmail').trim();
		req.sanitizeBody('stripeToken').trim();
		req.sanitizeBody('total_amount').trim();
		req.sanitizeBody('card_number').trim();
		req.sanitizeBody('exp_month').trim();
		req.sanitizeBody('exp_year').trim();
		req.sanitizeBody('cvc_number').trim();

		var request = {};
		request.user_id = req.body.user_id.replace(/^"(.*)"$/, '$1');
		//request.job_id = req.body.job_id;
		request.total_amount = req.body.total_amount;
		//request.transaction_id = req.body.transaction_id;
		request.stripeEmail = req.body.stripeEmail;
		request.stripeToken = req.body.stripeToken;
		request.card = {};
		request.card.number = req.body.card_number;
		request.card.exp_month = req.body.exp_month;
		request.card.exp_year = req.body.exp_year;
		request.card.cvc = req.body.cvc_number;


		db.GetOneDocument('users', { '_id': request.user_id }, {}, {}, function (err, user) {
			if (err || !user) {
				res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
			} else {
				request.stripeEmail = request.stripeEmail ? request.stripeEmail : user.email;
				request.stripe_customer_id = user.stripe.customer;

				async.waterfall([
					function (callback) {
						db.GetOneDocument('settings', { "alias": "general" }, { 'settings': 1 }, {}, function (err, settings) {
							callback(err, settings.settings);
						});
					},
					function (settings, callback) {
						if (!request.transaction_id) {
							var transaction = {
								'user': user._id,
								'type': 'wallet',
								'amount': request.total_amount,
								'status': 1
							};
							db.InsertDocument('transaction', transaction, function (err, transaction) {
								request.transaction_id = transaction._id;
								request.trans_id = transaction._id;
								request.trans_date = transaction.createdAt;
								request.avail_amount = transaction.amount;
								request.credit_type = transaction.type;
								callback(err, settings);
							});
						} else {
							callback(null, settings);
						}
					}, function (settings, callback) {
						if (!request.stripeToken || !request.stripe_customer_id) {
							stripe.tokens.create({ card: request.card }, function (err, token) {
								callback(err, settings, token);
							});
						} else {
							callback(null, settings, null);
						}
					},
					function (settings, token, callback) {
						var charge = {};
						charge.amount = request.total_amount * 100;
						charge.currency = 'usd';
						charge.source = token.id;
						charge.description = 'Wallet Recharge';
						stripe.charges.create(charge, function (err, charges) {
							callback(err, settings, charges);
						});
					}, function (settings, charges, callback) {
						if (request.transaction_id) {
							var transactions = [{
								'gateway_response': charges
							}];
							db.UpdateDocument('transaction', { '_id': request.transaction_id }, { 'transactions': transactions }, {}, function (err, transaction) {
								callback(err, settings, charges);
							});
						} else {
							callback(null, settings, null);
						}
					}, function (settings, charges, callback) {
						db.GetOneDocument('walletReacharge', { "user_id": request.user_id }, {}, {}, function (err, wallet) {
							callback(err, settings, charges, wallet);
						});
					}
				], function (err, settings, charges, wallet) {

					if (err) {

						res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
					} else {
						var transactions = {
							'trans_id': request.trans_id,
							'trans_date': request.trans_date,
							'trans_amount': request.avail_amount,
							'avail_amount': 0,
							'type': 'CREDIT'
						};
						var dataToUpdate = {};
						dataToUpdate = { 'task.invoice.status': 0 };
						if (charges.status == 'succeeded') {
							dataToUpdate = { 'task.invoice.status': 1, 'payment_gateway_response': charges };

							if (wallet) {
								transactions.avail_amount = parseInt(wallet.total) + parseInt(request.total_amount);
								db.UpdateDocument('walletReacharge', { 'user_id': request.user_id }, { total: transactions.avail_amount, $push: { 'transactions': transactions } }, {}, function (err, docdata) {
									if (err || charges.status != 'succeeded') {
										res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
									} else {
										var payment = {};
										payment.user = request.user_id;
										payment.transaction = request.transaction_id;
										payment.charge = charges.id;
										payment.image = settings.site_url + 'app/mobile/images/success.png';
										res.redirect("http://" + req.headers.host + '/mobile/payment/pay-completed/bycard');
									}
								});
							} else {
								transactions.avail_amount = request.total_amount;
								var insertdata = {};
								insertdata.user_id = request.user_id;
								insertdata.total = request.total_amount;
								insertdata.transactions = [];
								insertdata.transactions.push(transactions);
								db.InsertDocument('walletReacharge', insertdata, function (err, docdata) {
									if (err || charges.status != 'succeeded') {

										res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
									} else {
										var payment = {};
										payment.user = request.user_id;
										payment.transaction = request.transaction_id;
										payment.charge = charges.id;
										payment.image = settings.site_url + 'app/mobile/images/success.png';
										res.redirect("http://" + req.headers.host + '/mobile/payment/pay-completed/bycard');
									}
								});

							}
						}
					}
				});
			}
		});
	}

	controller.payCancel = function (req, res) {
		db.GetOneDocument('settings', { "alias": "general" }, { 'settings': 1 }, {}, function (err, settings) {
			res.render('mobile/payment-return', { site_url: settings.settings.site_url });
		});
	}

	controller.walletrechargefailed = function (req, res) {
		db.GetOneDocument('settings', { "alias": "general" }, { 'settings': 1 }, {}, function (err, settings) {
			res.render('mobile/payment-failed', { image: settings.settings.site_url + 'app/mobile/images/failed.png' });
		});
	}

	controller.settings = function (req, res) {
		db.GetOneDocument('currencies', { 'default': 1 }, {}, {}, function (err, currencies) {
			if (err || !currencies) {
				res.send({
					"status": 0,
					"message": 'Error'
				});
			}
			else {
				db.GetOneDocument('settings', { "alias": "general" }, { 'settings': 1 }, {}, function (err, settings) {
					if (err || !settings.settings.wallet) {
						res.send({
							'status': '0',
							'message': 'wallet money settings are not available, Please try again later.',
							'response': 'wallet money settings are not available, Please try again later.'
						});
					} else {
						if (settings.settings.wallet.amount.minimum && settings.settings.wallet.amount.maximum) {
							res.send({
								'status': '1',
								'wallter_money': {
									'wallet_min_amount': (settings.settings.wallet.amount.minimum * currencies.value).toFixed(2),
									'wallet_max_amount': (settings.settings.wallet.amount.maximum * currencies.value).toFixed(2)
								}
							});
						} else {
							res.send({
								'status': '0',
								'message': 'wallet money settings are not available, Please try again later.',
								'response': 'wallet money settings are not available, Please try again later.'
							});
						}
					}
				});
			}
		});
	}

	controller.mobpaypalPayment = function mobpaypalPayment(req, res) {

		var data = {};
		data.status = '1';
		req.checkBody('user', 'User ID is Required').notEmpty();
		req.checkBody('task', 'Task ID is Required').notEmpty();
		errors = req.validationErrors();
		var errors = req.validationErrors();
		if (errors) { data.response = errors[0].msg; res.send(data); return; }

		var request = {};
		request.task = req.body.task;
		request.user = req.body.user;


		req.sanitizeBody('user').trim();
		req.sanitizeBody('task').trim();


		db.GetOneDocument('users', { '_id': request.user }, {}, {}, function (err, user) {
			if (err || !user) {
				res.send({
					"status": "0",
					"message": "Invalid user ID, Please try Again Later.!"
				});
			}
			else {
				var options = {};
				options.populate = 'user category tasker';
				db.GetOneDocument('task', { '_id': request.task, 'status': 6 }, {}, options, function (err, task) {
					if (err || !task) {
						res.send({
							"status": "0",
							"message": "Invalid task ID, Please try Again Later.!"
						});
					} else {
						async.waterfall([
							function (callback) {
								db.GetOneDocument('paymentgateway', { status: { $ne: 0 }, alias: 'paypal' }, {}, {}, function (err, paymentgateway) {
									if (err || !paymentgateway) {
										res.send({
											"status": "0",
											"message": "Error, Please try Again Later.!"
										});
									} else {
										callback(err, paymentgateway);
									}

								});
							},
							function (paymentgateway, callback) {
								var transaction = {};
								transaction.user = task.user;
								transaction.tasker = task.tasker;
								transaction.task = request.task;
								transaction.type = 'paypal';
								if (transaction.amount = task.invoice.amount.balance_amount) {
									transaction.amount = task.invoice.amount.balance_amount;
								} else {
									transaction.amount = task.invoice.amount.grand_total;
								}
								transaction.amount = task.invoice.amount.balance_amount;
								transaction.task_date = task.createdAt;
								transaction.status = 1
								db.InsertDocument('transaction', transaction, function (err, transaction) {
									request.transaction_id = transaction._id;
									request.trans_date = transaction.createdAt;
									request.avail_amount = transaction.amount;
									request.credit_type = transaction.type;
									callback(err, paymentgateway, transaction);
								});
							},
							function (paymentgateway, transaction, callback) {
								db.GetOneDocument('settings', { 'alias': 'general' }, {}, {}, function (err, settings) {
									if (err || !settings) {
										res.send({
											"status": "0",
											"message": "Configure your website settings.!"
										});
									}
									else { callback(err, paymentgateway, transaction, settings.settings); }
								});
							},
							function (paymentgateway, transaction, settings, callback) {
								db.GetOneDocument('emailtemplate', { name: { $in: ['PaymentDetailstoAdmin', 'PaymentDetailstoTasker', 'PaymentDetailstoUser'] }, 'status': { $eq: 1 } }, {}, {}, function (err, template) {
									if (err || !template) {
										res.send({
											"status": "0",
											"message": "Unable to get email template.!"
										});
									}
									else { callback(err, paymentgateway, transaction, settings, template); }
								});
							}
						], function (err, paymentgateway, transaction, settings, template) {
							if (err) {
								res.send({
									"status": "0",
									"message": "Error, Please try Again Later.!"
								});
							} else {
								db.GetOneDocument('currencies', { 'default': 1 }, {}, {}, function (err, currencies) {
									if (err || !currencies) {
										res.send({
											"status": "0",
											"response": 'Data Not available'
										});
									}
									else {
										paypal.configure({
											'mode': 'sandbox',
											'client_id': paymentgateway.settings.client_id,
											'client_secret': paymentgateway.settings.client_secret
										});

										var json = {
											"intent": "sale",
											"payer": {
												"payment_method": "paypal"
											},
											"redirect_urls": {},
											"transactions": [{
												"item_list": {
													"items": []
												},
												"amount": {
													"currency": currencies.code,
													"details": {}
												},
												"description": "This is the payment description."
											}]
										};
										var item = {};
										item.name = settings.site_title;
										item.price = task.invoice.amount.grand_total;
										item.currency = currencies.code;
										item.quantity = 1;
										json.transactions[0].item_list.items.push(item);

										json.transactions[0].amount.details.subtotal = task.invoice.amount.grand_total;
										json.transactions[0].amount.details.tax = 0.00;
										json.transactions[0].amount.total = task.invoice.amount.grand_total;
										json.transactions[0].amount.currency = currencies.code;

										json.redirect_urls.return_url = "http://" + req.headers.host + "/mobile/app/payment/paypal-execute?task=" + task._id + "&transaction=" + transaction._id;
										json.redirect_urls.cancel_url = "http://" + req.headers.host + "/checkout/payment/paypal/cancel?task=" + task._id + "&transaction=" + transaction._id;

										paypal.payment.create(json, function (error, payment) {
											if (error) {
												res.send({
													"status": "0",
													"message": "Unable to get email template.!"
												});
											} else {
												for (var i = 0; i < payment.links.length; i++) {
													var link = payment.links[i];
													if (link.method === 'REDIRECT') {
														data.redirectUrl = link.href;
													}
												}
												data.payment_mode = 'paypal';
												res.send(data);
											}
										});


									}
								});

							}
						});
					}
				});
			}
		});
	}
	controller.mobpaypalExecute = function mobpaypalPayment(req, res) {

		var data = {};
		data.status = '1';

		var request = {};
		request.task = req.query.task;
		request.transaction = req.query.transaction;
		request.paymentId = req.query.paymentId;
		request.token = req.query.token;
		request.PayerID = req.query.PayerID;

		var options = {};
		options.populate = 'tasker user task';
		db.GetOneDocument('transaction', { _id: request.transaction }, {}, options, function (err, transaction) {

			if (err || !transaction) {
				res.send({
					"status": "0",
					"message": "Error, Please try Again Later.!"
				});
			} else {
				async.waterfall([
					function (callback) {
						db.GetOneDocument('paymentgateway', { status: { $ne: 0 }, alias: 'paypal' }, {}, {}, function (err, paymentgateway) {
							if (err || !paymentgateway) {
								res.send({
									"status": "0",
									"message": "Configure your website settings.!"
								});
							}
							else {
								callback(err, paymentgateway);
							}
						});
					},
					function (paymentgateway, callback) {
						db.GetOneDocument('settings', { 'alias': 'general' }, {}, {}, function (err, settings) {
							if (err || !settings) {
								res.send({
									"status": "0",
									"message": "Configure your website settings.!"
								});
							}
							else { callback(err, paymentgateway, settings.settings); }
						});
					},
					function (paymentgateway, settings, callback) {
						db.GetDocument('emailtemplate', { name: { $in: ['PaymentDetailstoAdmin', 'PaymentDetailstoTasker', 'PaymentDetailstoUser'] }, 'status': { $ne: 0 } }, {}, {}, function (err, template) {
							if (err || !template) {
								res.send({
									"status": "0",
									"message": "Unable to get email template.!"
								});
							}
							else { callback(err, paymentgateway, settings, template); }
						});
					}
				], function (err, paymentgateway, settings, template) {
					if (err) {
						res.send({
							"status": "0",
							"message": "Configure your website settings.!"
						});
					} else {
						paypal.configure({
							'mode': 'sandbox',
							'client_id': paymentgateway.settings.client_id,
							'client_secret': paymentgateway.settings.client_secret
						});

						paypal.payment.execute(request.paymentId, { "payer_id": request.PayerID }, function (err, result) {
							if (err) {

								//res.redirect("/payment-failed");
								res.redirect("http://" + req.headers.host + '/mobile/mobile/failed');
							} else {
								if (result.transactions[0].related_resources[0].sale.state != 'completed') {
									data.response = 'Transaction Failed';
									res.redirect("mobile/payment-failed");
								} else {
									var dataToUpdate = {};
									dataToUpdate.status = 7;
									dataToUpdate.invoice = transaction.task.invoice;
									dataToUpdate.invoice.status = 1;
									dataToUpdate.payee_status = 0;
									dataToUpdate.invoice.amount.balance_amount = parseFloat(transaction.task.invoice.amount.balance_amount) - parseFloat(transaction.task.invoice.amount.balance_amount);
									dataToUpdate.payment_type = 'paypal';

									var transactionsData = [{
										'gateway_response': result
									}];

									db.UpdateDocument('transaction', { '_id': request.transaction }, { $push: { 'transactions': transactionsData } }, {}, function (err, transactionUpdate) {

										if (err) {
											res.status(400).send(err);
										} else {

											db.UpdateDocument('task', { _id: transaction.task._id }, dataToUpdate, function (err, docdata) {
												if (err) {
													res.status(400).send(err);
												} else {

													var html = template[0].email_content;
													html = html.replace(/{{t_firstname}}/g, transaction.tasker.name.first_name);
													html = html.replace(/{{t_lastname}}/g, transaction.tasker.name.last_name);
													html = html.replace(/{{taskeraddress}}/g, transaction.tasker.address.line1);
													html = html.replace(/{{taskeraddress1}}/g, transaction.tasker.address.city);
													html = html.replace(/{{taskeraddress2}}/g, transaction.tasker.address.state);
													html = html.replace(/{{bookingid}}/g, transaction.task.booking_id);
													html = html.replace(/{{u_firstname}}/g, transaction.user.name.first_name);
													html = html.replace(/{{u_lastname}}/g, transaction.user.name.last_name);
													html = html.replace(/{{useraddress}}/g, transaction.user.address.line1);
													html = html.replace(/{{useraddress1}}/g, transaction.user.address.city);
													html = html.replace(/{{useraddress2}}/g, transaction.user.address.state);
													html = html.replace(/{{categoryname}}/g, transaction.task.booking_information.work_type);
													html = html.replace(/{{hourlyrates}}/g, transaction.task.hourly_rate);
													html = html.replace(/{{totalhour}}/g, transaction.task.invoice.worked_hours);
													html = html.replace(/{{totalamount}}/g, transaction.task.invoice.amount.grand_total);
													html = html.replace(/{{amount}}/g, transaction.task.invoice.amount.grand_total);
													html = html.replace(/{{adminamount}}/g, transaction.task.invoice.amount.admin_commission);
													html = html.replace(/{{privacy}}/g, settings.site_url + 'pages/privacypolicy');
													html = html.replace(/{{terms}}/g, settings.site_url + '/pages/termsandconditions');
													html = html.replace(/{{senderemail}}/g, template[0].sender_email);
													html = html.replace(/{{logo}}/g, settings.site_url + settings.logo);
													html = html.replace(/{{site_title}}/g, settings.site_title);
													var mailOptions = {
														from: template[0].sender_email,
														to: template[0].sender_email,
														subject: template[0].email_subject,
														text: html,
														html: html
													};
													mail.send(mailOptions, function (err, response) { });

													var html1 = template[1].email_content;
													html1 = html1.replace(/{{firstname}}/g, transaction.tasker.name.first_name);
													html1 = html1.replace(/{{lastname}}/g, transaction.tasker.name.last_name);
													html1 = html1.replace(/{{taskeraddress}}/g, transaction.tasker.address.line1);
													html1 = html1.replace(/{{taskeraddress1}}/g, transaction.tasker.address.city);
													html1 = html1.replace(/{{taskeraddress2}}/g, transaction.tasker.address.state);
													html1 = html1.replace(/{{bookingid}}/g, transaction.task.booking_id);
													html1 = html1.replace(/{{categoryname}}/g, transaction.task.booking_information.work_type);
													html1 = html1.replace(/{{hourlyrates}}/g, transaction.task.hourly_rate);
													html1 = html1.replace(/{{totalhour}}/g, transaction.task.invoice.worked_hours);
													html1 = html1.replace(/{{totalamount}}/g, transaction.task.invoice.amount.grand_total);
													html1 = html1.replace(/{{amount}}/g, (transaction.task.invoice.amount.grand_total - transaction.task.invoice.amount.admin_commission));
													html1 = html1.replace(/{{admincommission}}/g, transaction.task.invoice.amount.admin_commission);
													html1 = html1.replace(/{{privacy}}/g, settings.site_url + 'pages/privacypolicy');
													html1 = html1.replace(/{{terms}}/g, settings.site_url + '/pages/termsandconditions');
													html1 = html1.replace(/{{senderemail}}/g, template[0].sender_email);
													html1 = html1.replace(/{{logo}}/g, settings.site_url + settings.logo);
													html1 = html1.replace(/{{site_title}}/g, settings.site_title);
													var mailOptions1 = {
														from: template[1].sender_email,
														to: transaction.tasker.email,
														subject: template[1].email_subject,
														text: html1,
														html1: html1
													};
													mail.send(mailOptions1, function (err, response) { });

													var html2 = template[2].email_content;
													html2 = html2.replace(/{{bookingid}}/g, transaction.task.booking_id);
													html2 = html2.replace(/{{firstname}}/g, transaction.user.name.first_name);
													html2 = html2.replace(/{{lastname}}/g, transaction.user.name.last_name);
													html2 = html2.replace(/{{useraddress}}/g, transaction.user.address.line1);
													html2 = html2.replace(/{{useraddress1}}/g, transaction.user.address.city);
													html2 = html2.replace(/{{useraddress2}}/g, transaction.user.address.state);
													html2 = html2.replace(/{{categoryname}}/g, transaction.task.booking_information.work_type);
													html2 = html2.replace(/{{hourlyrates}}/g, transaction.task.hourly_rate);
													html2 = html2.replace(/{{totalhour}}/g, transaction.task.invoice.worked_hours);
													html2 = html2.replace(/{{totalamount}}/g, transaction.task.invoice.amount.grand_total);
													html2 = html2.replace(/{{amount}}/g, transaction.task.invoice.amount.grand_total);
													html2 = html2.replace(/{{privacy}}/g, settings.site_url + 'pages/privacypolicy');
													html2 = html2.replace(/{{terms}}/g, settings.site_url + '/pages/termsandconditions');
													html2 = html2.replace(/{{senderemail}}/g, template[0].sender_email);
													html2 = html2.replace(/{{logo}}/g, settings.site_url + settings.logo);
													html2 = html2.replace(/{{site_title}}/g, settings.site_title);
													var mailOptions2 = {
														from: template[2].sender_email,
														to: transaction.user.email,
														subject: template[2].email_subject,
														text: html2,
														html2: html2
													};
													mail.send(mailOptions2, function (err, response) { });

													var message = 'Payment Completed';
													var options = { 'job_id': transaction.task.booking_id, 'provider_id': transaction.task.tasker };
													push.sendPushnotification(transaction.task.user, message, 'payment_paid', 'ANDROID', options, 'USER', function (err, Response, body) { });
													push.sendPushnotification(transaction.task.tasker, message, 'payment_paid', 'ANDROID', options, 'PROVIDER', function (err, Response, body) { });

													res.redirect("http://" + req.headers.host + '/mobile/mobile/paypalsucess');
												}
											});
										}
									});
								}
							}
						});
					}
				});
			}
		});
	}
	controller.applyCoupontest = function (req, res) {

		var status = '0';
		var response = {};
		req.checkBody('user_id', 'User ID is Required').notEmpty();
		req.checkBody('code', 'Coupon code is Required').notEmpty();
		req.checkBody('booking_id', 'Booking_id is Required').notEmpty();

		var data = {};
		data.user_id = req.body.user_id;
		data.code = req.body.code;
		data.reach_date = req.body.pickup_date;
		data.booking = req.body.booking_id;

		var errors = [];
		errors = req.validationErrors();
		if (errors) {
			res.send({
				"status": "0",
				"response": errors[0].msg
			});
			return;
		}

		var request = {};
		var date = new Date();
		var isodate = date.toISOString();//new Date("2016-08-30T18:30:00.0Z");;
		request.task = req.body.taskid;
		db.GetDocument('coupon', { status: { $ne: 0 }, code: req.body.code }, {}, {}, function (err, coupondata) {
			if (err || coupondata.length == 0) {
				res.send({ "status": "0", "response": "Invalid Coupon" });
			}
			else {
				db.GetDocument('coupon', { status: { $ne: 0 }, code: req.body.code, "expiry_date": { "$gte": isodate }, "valid_from": { "$lte": isodate } }, {}, {}, function (err, data) {
					if (err || data.length == 0) {
						res.send({ "status": "0", "response": "Coupon Code Date Expired" });
					}
					else {
						db.GetAggregation('task', [
							{ $match: { 'invoice.coupon': req.body.code } },
							{ $group: { _id: "$invoice.coupon", total: { $sum: 1 } } },
						], function (err, taskdata) {

							if (err || !taskdata) {
								res.send({ "status": "0", "response": "Coupon Code Date Expired" });
							} else {
								db.GetDocument('coupon', { status: { $ne: 0 }, code: req.body.code }, {}, {}, function (err, couponlimit) {
									if (err || couponlimit.length == 0) {
										res.send({ "status": "0", "response": "Coupon Code Limit Exceed" });
									} else {
										db.GetAggregation('task', [
											{ $match: { user: new mongoose.Types.ObjectId(req.body.user_id), 'status': 7, 'invoice.status': 1, 'invoice.coupon': req.body.code } },
											{ $group: { _id: "$user", total: { $sum: 1 } } },
										], function (err, usagedata) {
											if (err || !usagedata) {
												res.send({ "status": "0", "response": "Coupon Code Limit Exceed" });
											} else {
												var usage = 0;
												if (usagedata[0]) {
													if (usagedata[0].total) {
														usage = usagedata[0].total;
													}
												}
												db.GetDocument('coupon', { status: { $ne: 0 }, code: req.body.code, 'usage.per_user': { '$gte': usage }, 'usage.total_coupons': { '$gte': 1 } }, {}, {}, function (err, usagelimit) {
													if (err || !usagelimit || usagelimit.length == 0) {
														res.send({ "status": "0", "response": "Coupon Code Limit Exceed" });
													} else {
														db.GetOneDocument('currencies', { 'default': 1 }, {}, {}, function (err, currencies) {
															if (err || !currencies) {
																res.send({
																	"status": 0,
																	"message": 'Error'
																});
															}
															else {
																db.GetDocument('task', { 'booking_id': req.body.booking_id }, {}, {}, function (err, taskdata) {
																	if (err || !taskdata || taskdata.length == 0) {
																		res.send({ "status": "0", "response": "Error In booking_id " });
																	} else {
																		var invoice = {};
																		invoice.amount = {};
																		var discount = 0.00;
																		if (couponlimit[0].discount_type == 'Percentage') {
																			discount = parseInt(taskdata[0].invoice.amount.grand_total * couponlimit[0].amount_percentage) / 100;
																		}
																		else {
																			discount = parseInt(couponlimit[0].amount_percentage);
																		}

																		if (discount >= taskdata[0].invoice.amount.grand_total) {
																			discount = taskdata[0].invoice.amount.grand_total;
																		}

																		var grand_total = parseInt(taskdata[0].invoice.amount.grand_total) - discount;
																		var balance_amount = parseInt(taskdata[0].invoice.amount.balance_amount) - discount;



																		if (grand_total <= 0) {
																			grand_total = 0;
																		}

																		if (balance_amount <= 0) {
																			balance_amount = 0;
																		}
																		var update = { 'invoice.amount.coupon': discount, 'invoice.coupon': req.body.code, 'invoice.amount.discount': discount, 'invoice.amount.grand_total': grand_total, 'invoice.amount.balance_amount': balance_amount };

																		db.UpdateDocument('task', { _id: new mongoose.Types.ObjectId(taskdata[0]._id) }, update, function (err, result) {
																			if (err || result.nModified == 0) {
																				res.send({ "status": "0", "response": "Error In Coupon updation" });
																			}
																			else {
																				var usagelimits = couponlimit[0].usage.total_coupons;
																				var result = usagelimits - 1;
																				if (result <= 0) {
																					result = 0;
																				}
																				db.UpdateDocument('coupon', { status: { $ne: 0 }, code: req.body.code }, { 'usage.total_coupons': result }, function (err, result) {
																					if (err || result.nModified == 0) {
																						res.send({ "status": "0", "response": "Error In Coupon updation" });
																					}
																					else {

																						res.send({ "status": "1", "response": "Coupon used successfully", "discount": (discount * currencies.value).toFixed(2) });
																					}
																				});
																			}
																		});
																	}
																});
															}
														});
													}
												});
											}
										});
									}
								});
							}
						});
					}
				});
			}
		});
	}

	return controller;
};
