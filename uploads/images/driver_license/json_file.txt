[  
   {  
      "Email":"stephanie.schmid17@gmail.com",
      "Phone":"Optional",
      "ShippingMethod":"",
      "ShippingOption":"",
      "ShippingPrice":"",
      "PaymentMethodName":"",
      "PaymentStatus":"",
      "OrderNumber":3164445,
      "OrderDate":"2018-04-26T17:25:17.400",
      "ShippingFirstName":"John",
      "ShippingLastName":"Doe",
      "ShippingCompany":"",
      "ShippingAddress1":"2100 Delgany St Apt 407",
      "ShippingAddress2":"",
      "ShippingCity":"Colorado",
      "ShippingState":" ",
      "ShippingZip":"80202",
      "ShippingCountry":"United States",
      "BillingFirstName":"John",
      "BillingLastName":"Doe",
      "BillingCompany":"",
      "BillingAddress1":"2100 Delgany St Apt 407",
      "BillingAddress2":"",
      "BillingCity":"Colorado",
      "BillingState":"",
      "BillingZip":"80202",
      "BillingCountry":"United States",      
      "cartitems":[  
         {  
            "Quantity":1,
            "Weight":1,
            "ChosenSize":"L",
            "ChosenColor":"True Black",
            "OrderedProductName":"Unisex Audubon Eco-Fleece Hoodie",
            "OrderedProductSKU":"AA9590",
            "Phrase":"Custom Text",
            "OrderedProductImage":"https://domain.com/images/products/medium/S00101BLLG.jpg"

         },
         {  
            "Quantity":1,
            "Weight":1,
            "ChosenSize":"S",
            "ChosenColor":"Grass",
            "OrderedProductName":"Adult Audubon Logo Short Sleeve Tee",
            "OrderedProductSKU":"rs100",
            "Phrase":"Custom Text",
            "OrderedProductImage":"https://domain.com/images/products/medium/S00101BLLG.jpg"

         }
      ]
   }
]