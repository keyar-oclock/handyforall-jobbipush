var api = require('paypal-rest-sdk');


/*db.getDocument('paymentgateway',{ status: { $ne: 0 }, alias:'paypal' }, {}, function(err,shops){
	if(err){
        res.send(err);
	}else {
		api.configure({
			'mode': 'sandbox',
			'client_id': shops[0].settings.client_id,
			'client_secret': shops[0].settings.client_secret
		});
	}
});*/

api.configure({
    'mode': 'sandbox',
    'client_id': "AUXf257GTNSmhEVRFVaZPnRCpRmyrpENt5ElP-bPbsfFy16JyXlTY6Epb2nLV9pFCPrI4rSC3F7TBDmJ",
    'client_secret': "EE20uDY0__FCIAc7erZekndOJYnkjfCULZ3s8w8FvzIwbNBMMla3pMHRZSWWw2lvv6K_2KfQ2lPKorOD"
});

function create(req, res, payment, callback){
	var create_payment_json = {
        "intent": "sale",
        "payer": {
            "payment_method": "paypal"
        },
        "redirect_urls": {
            "return_url":"http://"+req.headers.host+"/mobile/payment/paypal/execute",
            "cancel_url" : "http://"+req.headers.host+"/mobile/failed"
        },
        "transactions": [{
            "amount": {
                "total": parseInt(payment.total).toFixed(2),
                "currency": payment.currency,
                "details": {
                    "subtotal": "4.50",
                    "tax": "0.25",
                    "shipping": "0.25"}
            },
            "description": "This is the payment transaction description."
        }]
    };
	
    api.payment.create(create_payment_json, function (error, payment) {
    if (error) {
        callback(error)
    } else {
		var data = {};
		for(var i=0; i < payment.links.length; i++) {
			var link = payment.links[i];
			if (link.method === 'REDIRECT') {
				data.redirectUrl = link.href;
			}
		}
		data.payment_mode = 'paypal';
		callback(data);
    }
	});
}

function execute(paymentId, details, callback){
  api.payment.execute(paymentId, details, function (error, payment) {
    if (error) {
	  callback(error);
    } else {
	  callback(payment);
    }
  });
}

module.exports  = {
    "create"        :   create,
	"execute"		:	execute
};