var db = require('../../controller/adaptor/mongodb.js');
function create(req, res, order, callback) {
	var token = order.stripeToken;
	db.GetDocument('paymentgateway', { status: { $ne: 0 }, alias: 'stripe' }, {}, {}, function (err, StripeConnectConfig) {
		var data = {};
		if (err) {
			data.status = 2;
			data.errorMsg = err;
			callback(data)
		} else {
			if (StripeConnectConfig.length > 0 && StripeConnectConfig[0].status == 1) {
				var stripe = require('stripe')(StripeConnectConfig[0].settings.secret_key);
				if (typeof order.tasker_stripe_accout != 'undefined' && typeof order.tasker_stripe_accout.stripe_user_id != 'undefined' && order.tasker_stripe_accout.stripe_user_id != '') {
					stripe.charges.create({
						amount: order.total_amount,
						currency: "usd",
						source: token.id,
						description: "Example charge",
						application_fee: order.admin_amount
					},
						{
							stripe_account: order.tasker_stripe_accout.stripe_user_id
						},
						function (err, charge) {
							if (err) {
								data.status = 2;
								data.errorMsg = err;
								callback(data)
							} else {
								data.status = 3;
								data.transactionResponse = charge;
								callback(data)
							}
						}
					);
				} else {
					data.status = 2;
					data.errorMsg = 'Sorry Tasker Not Connect Our Stripe Accout';
					callback(data)
				}
			} else {
				data.status = 2;
				data.errorMsg = 'Sorry... We\'re having technical difficulties and are looking into the problem now.';
				callback(data)
			}
		}
	});
}
module.exports = {
    "create": create
};