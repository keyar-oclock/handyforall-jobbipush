var http = require('http');
var Jimp = require("jimp");
var async = require("async");
var CONFIG = require('../config/config');
var base64 = require('node-base64-image');
var crypto = require('crypto');
var fs = require("fs");

function randomString(length, chars) {
	var mask = '';
    if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
    if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if (chars.indexOf('#') > -1) mask += '0123456789';
    if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
    var result = '';
    for (var i = length; i > 0; --i) result += mask[Math.floor(Math.random() * mask.length)];
    return result;
}

function decodeBase64Image(data, callback) {
    var fileName = crypto.createHash('md5').update(Date.now() + randomString(10, '#') + Date.now()).digest("hex");
    var originalImage = CONFIG.USER_PROFILE_IMAGE + fileName;
    var thumbnailImage = CONFIG.USER_PROFILE_THUMB + fileName;

    var options = { filename: originalImage };
    var dataTemp = data.base64;

	//Finished
    fs.writeFile("123.jpeg", dataTemp, { encoding: 'base64' }, function (err) {
        if (err) { }
        Jimp.read(originalImage).then(function (lenna) {
            lenna.resize(100, 100)            // resize
                .quality(60)                 // set JPEG quality
                .write(thumbnailImage); // save
            /*callback({'status' : 'sucess', 'image':fileName});*/
            callback({ 'status': 'sucess', 'image': fileName + '.jpg' });
        }).catch(function (err) {

        });
    });
}

function timeDifference(a, b) {
	var timediff = {};
	var jobTime = '';
	if (a.diff(b, 'years') != 0) {
		timediff = { type: 'years', value: a.diff(b, 'years') };
	} else if (a.diff(b, 'months') != 0) {
		timediff = { type: 'months', value: a.diff(b, 'months') };
	} else if (a.diff(b, 'days') != 0) {
		timediff = { type: 'days', value: a.diff(b, 'days') };
	} else if (a.diff(b, 'minutes') != 0) {
		timediff = { type: 'minutes', value: a.diff(b, 'minutes') };
	} else {
		timediff = { type: 'seconds', value: a.diff(b, 'seconds') };
	}
	if (timediff.value > 0) {
		timeWord = timediff.value + ' ' + timediff.type + ' later';
	} else {
		timeWord = Math.abs(timediff.value) + ' ' + timediff.type + ' ago';
	}
	return timeWord;
}

function inArray(search, array) {
    var length = array.length;
    for (var i = 0; i < length; i++) {
        if (array[i] == search) return true;
    }
    return false;
}

function exchangeRates(from, to, callback) {
	async.parallel({
		google: function (callback) {
			http.get({
				protocol: 'http:',
				host: 'www.google.com',
				path: '/finance/converter?a=1&from=' + from + '&to=' + to
			}, function (response) {
				var body = '';
				response.on('data', function (d) {
					body += d;
				});
				response.on('end', function () {
					var conversion = body.match(/<span class=bld>(.*)<\/span>/);
					var rate = conversion[1].replace(/[^0-9.]/g, "");
					callback(null, rate);
				})
					.on('error', function (error) {
						callback(error, null);
					});
			});
		},
		yahoo: function (callback) {
			http.get({
				protocol: 'http:',
				host: 'download.finance.yahoo.com',
				path: '/d/quotes.csv?s=' + from + to + '=X&f=l1'
			}, function (response) {
				var body = '';
				response.on('data', function (d) {
					body += d;
				});
				response.on('end', function () {
					var rate = body.replace(/[^0-9.]/g, "");
					callback(null, rate);
				})
					.on('error', function (error) {
						callback(error, null);
					});
			});
		}
	}, function (err, result) {
		callback(err, result);
	});
}

String.prototype.capitalizeFirstLetter = function () {
	return this.charAt(0).toUpperCase() + this.slice(1);
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

module.exports = {
    "randomString": randomString,
	"decodeBase64Image": decodeBase64Image,
	"timeDifference": timeDifference,
	"inArray": inArray,
	"exchangeRates": exchangeRates
};