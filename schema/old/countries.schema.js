var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var COUNTRIES_SCHEMA = {};
COUNTRIES_SCHEMA.COUNTRIES = {
    name:String,
    code:{type: String, upperCase: true, index: {unique: true},trim: true},
    status: { type:Number, default:1 }
};

module.exports = COUNTRIES_SCHEMA;
