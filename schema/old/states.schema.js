var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var STATES_SCHEMA = {};
STATES_SCHEMA.STATES = {
    name:String,
    code:{type: String, upperCase: true, index: {unique: true},trim: true},
    parent: { type: Schema.ObjectId, ref: 'countries' }
};

module.exports = STATES_SCHEMA;
