var mongoose = require("mongoose");
var Schema 	 = mongoose.Schema;

var POSTFOOTER_SCHEMA = {};

POSTFOOTER_SCHEMA.POSTFOOTER = {
										title:String,
										image: String,
										img_name:String,
    									img_path:String,
										description:String,
										status: { type:Number, default:1 }
                 };

module.exports = POSTFOOTER_SCHEMA;
