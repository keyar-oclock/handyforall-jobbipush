var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var LOCATIONS_SCHEMA = {};
LOCATIONS_SCHEMA.LOCATIONS = {
    service_tax: Number,
    site_commission:Number,
    country: {
        id: {type: Schema.ObjectId, ref: 'country'},
        name: String,
        code: String
    },
    city: String,
    location: {
        lng: Number,
        lat: Number
    },
    bounds: {
        southwest: {
            lng: Number,
            lat: Number
        },
        northeast: {
            lng: Number,
            lat: Number
        }
    },
	fare:{},
    currency: String,
    avail_category: [ { type: Schema.ObjectId, ref: 'categories' } ],
    peak_time: String,
    night_charge: String,
    state: String,
    status: Number
};

module.exports = LOCATIONS_SCHEMA;
