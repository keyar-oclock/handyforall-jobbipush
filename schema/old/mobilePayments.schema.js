
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var MOBILE_PAYMENTS_SCHEMA = {};
MOBILE_PAYMENTS_SCHEMA.MOBILE_PAYMENTS = {

    user_id: {type: String, ref: 'users'},
    tasker_id: {type: String, ref: 'tasker'},
    booking_id: String,
    payment_id: String,
    payment: String,
    amount: Number,
    tips_amount: Number,
    dateAdded: Date,
	
};
module.exports = MOBILE_PAYMENTS_SCHEMA;