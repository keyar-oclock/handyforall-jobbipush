var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var CITIES_SCHEMA = {};
CITIES_SCHEMA.CITIES = {
    name:String,
    code:{type: String, upperCase: true, index: {unique: true},trim: true},
    parent: { type: Schema.ObjectId, ref: 'states' }
};

module.exports = CITIES_SCHEMA;
