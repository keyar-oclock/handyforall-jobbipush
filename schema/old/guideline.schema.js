var GUIDELINE_SCHEMA={};

GUIDELINE_SCHEMA.GUIDELINE ={
	title:String,
	guidelinefor:String,
	description:String,
	status :{ type:Number, default:1 }
};
module.exports = GUIDELINE_SCHEMA;