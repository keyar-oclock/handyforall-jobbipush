var app = angular.module('quickRabbit.dashboard');
app.factory('DashboardService', DashboardService);
function DashboardService(Upload, $q) {
    var dashboardService = {
        dashboarddata: dashboarddata
     };
    return dashboardService;
	function dashboarddata() {
        var deferred = $q.defer();
          Upload.upload({
            method: 'POST',
            url: '/site/dashboard/dashboarddata',
            data:{}
        }).success(function (data) {
            deferred.resolve(data);
            }).error(function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

}
