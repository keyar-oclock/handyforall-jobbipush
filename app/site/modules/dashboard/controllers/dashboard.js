angular.module('quickRabbit.dashboard')
    .controller('dashboardCtrl',dashboardCtrl);
dashboardCtrl.$inject = ['MainService', '$rootScope','$location','$scope','CurrentUserResolve','DashboardserviceResolve','$state','$translate'];
function dashboardCtrl(MainService, $rootScope, $location,$scope,CurrentUserResolve,DashboardserviceResolve,$state,$translate) {
      var dbc = this;
      dbc.data = DashboardserviceResolve;
      dbc.activecategory = dbc.data.response.landinginfo[0].ActiveCategory;

	  $rootScope.$on('eventName', function (event, args) {
            dbc.currentUserName = MainService.getCurrentUserValue();
              if(dbc.currentUserName){
                  dbc.avatar=dbc.currentUserName.avatar;
              }
      });

    dbc.currentUser = CurrentUserResolve[0];

  // Start autocomple category and subcategory

  $scope.getLocation  = function getLocation(data) {
      return (MainService.searchSuggestions(data).then(function (response) {
          return response;
      }, function (error) {
          return error;
      }));
  };

  $scope.search = function search(search,item ,label) {
      /*$state.go('category', { 'slug': search.slug })*/
      $state.go('hirestep1', {'slug':search.slug }, {reload: false});
  };

  // End autocomple category and subcategory

}
