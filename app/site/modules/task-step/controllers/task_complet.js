angular.module('quickRabbit.task').controller('taskFilterCtrl', taskFilterCtrl);

taskFilterCtrl.$inject = ['$scope', '$rootScope', '$location', '$stateParams', 'SearchResolve', 'TaskService', 'TaskserviceResolve', 'toastr', '$state', '$filter', 'AuthenticationService', '$modal', 'MainService', 'TaskServiceNewResolve', '$translate', 'ngMeta', 'TaskerCountResolve'];
function taskFilterCtrl($scope, $rootScope, $location, $stateParams, SearchResolve, TaskService, TaskserviceResolve, toastr, $state, $filter, AuthenticationService, $modal, MainService, TaskServiceNewResolve, $translate, ngMeta,TaskerCountResolve) {

	var tfc = this;
	var option = {};

	tfc.search = SearchResolve;
	tfc.taskinfo = TaskServiceNewResolve;
	console.log("tfc.taskinfo",tfc.taskinfo);
	tfc.page=TaskerCountResolve.count;
	if (TaskserviceResolve[0].SubCategoryInfo.name) {
		ngMeta.setTitle(TaskserviceResolve[0].SubCategoryInfo.name);
	}

	var user = AuthenticationService.GetCredentials();
	MainService.getCurrentUsers(user.currentUser.username).then(function (result) {
		tfc.currentUserData = result[0];
	}, function (error) {
		$translate('INIT CURRENT DATA ERROR').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
	});

	MainService.getDefaultCurrency().then(function (response) {
		tfc.DefaultCurrency = response;
	});

	var stateParams = angular.copy($rootScope.currentparams);
	if (angular.isDefined(stateParams.categoryid)) {
        option.category = stateParams.category;
    }
	/*
	if (angular.isDefined(stateParams.lat)) {
        option.lat = stateParams.lat;
    }
	if (angular.isDefined(stateParams.lon)) {
        option.lon = stateParams.lon;
    }
	*/
	if (angular.isDefined(stateParams.task)) {
        option.task = stateParams.task;
    }

	tfc.filter = option;
	tfc.taskbaseinfo = {};
	tfc.currentPage = 1;
	tfc.itemsPerPage = 5;
	tfc.totalItem = tfc.page;
	tfc.format = 'MM/dd/yyyy';
	tfc.logincheck = AuthenticationService.isAuthenticated();
	/*
	var user = AuthenticationService.GetCredentials();
	tfc.LoginUserId = '';
	if (angular.isDefined(user.currentUser) && angular.isDefined(user.currentUser.user_id)) {
		tfc.LoginUserId = user.currentUser.user_id;
	}
	*/
	if (TaskserviceResolve.length > 0) {
		tfc.taskbaseinfo.SubCategoryInfo = TaskserviceResolve[0].SubCategoryInfo;
	} else {
		$translate('WE ARE LOOKING FOR THIS TROUBLE SORRY UNABLE TO FETCH DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
		$state.go('landing', {}, { reload: false });
	}
	if (angular.isDefined(tfc.filter.date) && tfc.filter.date != '') {
		tfc.WorkingDate = new Date(tfc.filter.date);
		if (tfc.WorkingDate == 'Invalid Date') {
			tfc.WorkingDate = new Date();
		}
	} else {
		tfc.WorkingDate = new Date();
	}

	tfc.FullDate = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	tfc.filter.day = tfc.FullDate[tfc.WorkingDate.getDay()];
	tfc.filter.date = $filter('date')(tfc.WorkingDate, tfc.format, '');
	tfc.hours = {
		morning: false,
		afternoon: false,
		evening: false
	};
	if (angular.isDefined(tfc.filter.hour)) {
		if (tfc.filter.hour == "morning") {
			tfc.hours.morning = true;
		} else if (tfc.filter.hour == "afternoon") {
			tfc.hours.afternoon = true;
		} else if (tfc.filter.hour == "evening") {
			tfc.hours.evening = true;
		} else {
			tfc.filter.hour = 'morning';
			tfc.hours.morning = true;
		}
	} else {
		tfc.filter.hour = 'morning';
		tfc.hours.morning = true;
	}

	tfc.teskerErrorMsg = function () {
		toastr.error('Own job can\'t continue');
	};

	tfc.filter.categoryid = tfc.taskbaseinfo.SubCategoryInfo._id;
	tfc.getTaskerDetailsResponse = false;

	if (tfc.search.minRate == tfc.search.maxRate) {
		tfc.min = tfc.filter.minvalue = tfc.search.minRate || 0;
        tfc.max = tfc.filter.maxvalue = tfc.min + 200;
    }
    else {
        tfc.min = tfc.filter.minvalue = tfc.search.minRate || 0;
		tfc.max = tfc.filter.maxvalue = tfc.search.maxRate || 500;
    }

	tfc.slider = {
		minValue: tfc.min, maxValue: tfc.max, options: {
			floor: tfc.min, ceil: tfc.max, id: 'sliderA', onStart: function () {
				tfc.filter.minvalue = tfc.slider.minValue;
				tfc.filter.maxvalue = tfc.slider.maxValue;
			},
			onEnd: function () {
				tfc.filter.minvalue = tfc.slider.minValue;
				tfc.filter.maxvalue = tfc.slider.maxValue;

				tfc.getTaskerDetails();
			}
		}
	};


    tfc.getTaskerDetails = function () {
		tfc.TaskerDetails = [];
		tfc.getTaskerDetailsResponse = false;

		tfc.dummyarrayValue = [];
		if ($rootScope.currentState.name != 'hirestep2') {
			$state.go($rootScope.currentState, $rootScope.currentparams, { reload: false, inherit: true, notify: true });
		} else {

			TaskService.getTaskerByGeoFilter(tfc.filter, tfc.currentPage, tfc.itemsPerPage).then(function (response) {
				if (angular.isDefined(response.result)) {
					tfc.TaskerDetails = response.result;
					angular.forEach(tfc.TaskerDetails, function (value, key) {
						angular.forEach(value.taskerskills, function (value1, key1) {
							if (value1.childid == tfc.filter.categoryid) {
								tfc.dummyarrayValue.push(value1);
							}
						});
					});
					tfc.min = Math.min.apply(Math, tfc.dummyarrayValue.map(function (item) { return item.hour_rate; }));
					tfc.max = Math.max.apply(Math, tfc.dummyarrayValue.map(function (item) { return item.hour_rate; }));

				}
				/*
				if (angular.isDefined(response.count)) {
					tfc.totalItem = response.count;
				}
				*/
				//console.log(response);
				if (angular.isDefined(response.avgrating)) {
					tfc.avgtasker = response.avgrating;
				}

				if (angular.isDefined(response.taskercount)) {
					tfc.taskercount = response.taskercount;
				}
				angular.forEach(tfc.TaskerDetails, function (value, key) {
					angular.forEach(tfc.avgtasker, function (value1, key1) {
						if (value._id == value1._id) {
							tfc.TaskerDetails[key].avarating = parseInt(value1.avg);
							tfc.TaskerDetails[key].taskCount = parseInt(value1.datacount);
							tfc.TaskerDetails[key].recentReview = value1.documentData[value1.documentData.length - 1];
							if(value1.documentData[value1.documentData.length - 1].userdetails.avatar){
							tfc.TaskerDetails[key].userAvater = value1.documentData[value1.documentData.length - 1].userdetails.avatar;
							}
							console.log(">>>>>>>>>",value1.documentData[value1.documentData.length - 1]);
							console.log("<<<<<<<<<",value1.documentData[value1.documentData.length - 1].userdetails.avatar);
						}
					});
				});

				angular.forEach(tfc.TaskerDetails, function (value, key) {
					angular.forEach(tfc.taskercount, function (value1, key1) {
						if (value._id == value1._id) {
							tfc.TaskerDetails[key].taskercount = value1.induvidualcount;

						}
					});
				});
				tfc.getTaskerDetailsResponse = true;
			}, function (error) {

			});


		}
	};


	//
	tfc.timinglist = [{ value: "morning", time: "08am - 09am", data: "08:00" },
		{ value: "morning", time: "09am - 10am", data: "09:00" },
		{ value: "morning", time: "10am - 11am", data: "10:00" },
		{ value: "morning", time: "11am - 12pm", data: "11:00" },
		{ value: "afternoon", time: "12pm - 01pm", data: "12:00" },
		{ value: "afternoon", time: "01pm - 02pm", data: "13:00" },
		{ value: "afternoon", time: "02pm - 03pm", data: "14:00" },
		{ value: "afternoon", time: "03pm - 04pm", data: "15:00" },
		{ value: "evening", time: "04pm - 05pm", data: "16:00" },
		{ value: "evening", time: "05pm - 06pm", data: "17:00" },
		{ value: "evening", time: "06pm - 07pm", data: "18:00" },
		{ value: "evening", time: "07pm - 08pm", data: "19:00" }]

	//tfc.getTaskerDetails();
	tfc.filterDate = function () {

		tfc.filter.day = tfc.FullDate[tfc.WorkingDate.getDay()];
		tfc.filter.date = $filter('date')(tfc.WorkingDate, tfc.format, '');

		//time
		tfc.timeValue = new Date();
		tfc.ttvalue = tfc.timeValue.getHours();
		tfc.thisMonth = tfc.timeValue.getMonth();
		tfc.thisDate = tfc.timeValue.getDate();
		tfc.timeDisabledValue = tfc.ttvalue + ":00";
		//tfc.timeDisabledValue="19:00";
		tfc.selectedDate = tfc.WorkingDate.getDate();
		tfc.selectedMonth = tfc.WorkingDate.getMonth();
		tfc.filterTiming = "";
		if ((tfc.selectedDate == tfc.thisDate)) {
			if (tfc.selectedMonth == tfc.thisMonth) {
				tfc.filterTiming = [];
				for (var i = 0; i < tfc.timinglist.length; i++) {
					if (tfc.timinglist[i].data > tfc.timeDisabledValue) {
						tfc.filterTiming.push(tfc.timinglist[i]);
					}
				}
			} else {
				tfc.filterTiming = tfc.timinglist;
			}
		}
		else {
			tfc.filterTiming = tfc.timinglist;
		}

		//console.log(tfc.filterTiming.length);
		if (tfc.filterTiming.length) {
			tfc.dafaulttime = tfc.filterTiming[0].data;
		} else {
			tfc.WorkingDate.setDate(tfc.WorkingDate.getDate() + 1);
			tfc.filterTiming = tfc.timinglist;
			tfc.dafaulttime = tfc.filterTiming[0].data;
		}
		//console.log(tfc.dafaulttime);
		//time

		tfc.hourfilter(tfc.dafaulttime);

		//tfc.getTaskerDetails();

	}
	tfc.hourfilter = function (hour) {
		//	console.log("hours",hour);
		tfc.filter.time = hour;
		if (hour == "08:00" || hour == "09:00" || hour == "10:00" || hour == "11:00") {
			value = 'morning';
		} else if (hour == "12:00" || hour == "13:00" || hour == "14:00" || hour == "15:00") {
			value = 'afternoon';
		} else if (hour == "16:00" || hour == "17:00" || hour == "18:00" || hour == "19:00") {
			value = 'evening';
		}


		for (var i in tfc.hours) {
			if (value != i) {
				tfc.hours[i] = false;
			} else {
				tfc.hours[i] = true;
			}
		}
		if (tfc.filter.hour != value) {
			tfc.filter.hour = value;
			tfc.filter.time = hour;  // -- hour
		}
		tfc.getTaskerDetails();
	}

	tfc.registermodal = function (category) {
		var modalInstance = $modal.open({
			animation: true,
			templateUrl: 'app/site/modules/task-step/views/register.modal.tab.html',
			controller: 'RegisterModalInstanceCtrl',
			controllerAs: 'RCM'
		});
		modalInstance.result.then(function (userinfo) {
		}, function () {
		});
	};

	tfc.confirmatask = function confirmatask(message) {
		//console.log('message',message);
		//
		var date = {};
		date.currectdate = tfc.filter.date;
		date.time = tfc.filter.time;
		//console.log(date);

		tfc.taskinfo.tasker = message.tasker._id;
		/*tfc.taskinfo.billing_address = {
			'zipcode': tfc.currentUserData.addressList[0].zipcode || "",
			'country': tfc.currentUserData.addressList[0].country || "",
			'state': tfc.currentUserData.addressList[0].state || "",
			'city': tfc.currentUserData.addressList[0].street || "",
			'line2': tfc.currentUserData.addressList[0].line2 || "",
			'line1': tfc.currentUserData.addressList[0].line1 || ""
		};*/


		tfc.taskinfo.invoice = {
			'amount': {
				"minimum_cost": tfc.taskinfo.category.commision,
				"task_cost": tfc.taskinfo.category.commision,
				"total": tfc.taskinfo.category.commision,
				"grand_total": tfc.taskinfo.category.commision
			}
		};

		tfc.taskinfo.booking_information = {
			//'service_id': tfc.taskinfo.category.parent,
			'service_type': tfc.taskinfo.category.name,
			'work_type': tfc.taskinfo.category.name,
			'work_id': tfc.taskinfo.category._id,
			'instruction': tfc.taskinfo.task_description,
			'booking_date': '',
			'reach_date': '',
			'est_reach_date': '',
			'location': tfc.taskinfo.billing_address.line1 + "," + tfc.taskinfo.billing_address.line2 + "," + tfc.taskinfo.billing_address.city + "," + tfc.taskinfo.billing_address.state + "," + tfc.taskinfo.billing_address.country + "," + tfc.taskinfo.billing_address.zipcode
		};

		tfc.taskinfo.history = {};
		tfc.taskinfo.history.booking_date = new Date();
		tfc.taskinfo.history.est_reach_date = '';
		tfc.taskinfo.status = 1;

		angular.forEach(message.tasker.taskerskills, function (value, key) {
			if (value.childid == tfc.taskinfo.category._id)
				tfc.hour_rate = value.hour_rate;
		});

		tfc.taskinfo.hourly_rate = tfc.hour_rate || ""
		var taskdatetime = new Date();
		console.log("tfc.filter.time",tfc.filter.time);
		//tfc.taskinfo.task_date = taskdatetime.getDay() + "-" + taskdatetime.getMonth() + "-" + taskdatetime.getFullYear();
		//tfc.taskinfo.task_hour = taskdatetime.getHours() + ":" + taskdatetime.getMinutes() + ":" + taskdatetime.getSeconds();
		tfc.taskinfo.task_hour=tfc.filter.hour;
		//console.log("data", tfc.filter.time)
		if (tfc.filter.time) {
			TaskService.confirmtask(tfc.taskinfo, date).then(function (result) {
				$translate('REQUEST HAS BEEN SENT TO EMPLOYEE SUCCESSFULLY').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
				$state.go('landing', { reload: false });
			}, function (error) {
				toastr.error(error);
			});
		}
		else {
			$translate('CHOOSE YOUR JOB TIME').then(function (headline) { toastr.info(headline); }, function (translationId) { toastr.error(headline); });

		}
	};
}

/*
angular.module('quickRabbit.task').controller('RegisterModalInstanceCtrl', function ($modalInstance, $filter, toastr, $scope, AuthenticationService, $cookieStore, $state, $translate) {

    var rcm = this;

    rcm.registerUser = function () {
        $modalInstance.close(rcm);
    };

	rcm.registerUser = function (isValid, formData) {
        rcm.Error = '';
        var today = $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss');
        if (isValid) {
            rcm.UserDetails.today = today;
            rcm.UserDetails.role = rcm.type;
            rcm.UserDetails.location = $scope.location;
            AuthenticationService.Register(rcm.UserDetails, function (err, response) {
                if (err) {
                    for (var i = 0; i < err.length; i++) {
                        toastr.error('Your credentials are wrong ' + err[i].msg + '--' + err[i].param);
                    }
                } else {
                    if (response.user == rcm.UserDetails.username) {
                        AuthenticationService.SetCredentials(response.user, response.user_id, response.token, response.user_type, response.tasker_status);
                        $cookieStore.remove('TaskerData');
                        if (rcm.type == 'user') {
                            $location.path('/');
                        } else {
                            $state.reload();
                        }

                    } else {
						$translate('EMAIL ID ALREADY EXISTS OR USER NAME EXISTS').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
                    }
                }
            });
        } else {
			$translate('INVALID DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
            //toastr.error('Invalid Data')
        }
    };
});
*/

angular.module('quickRabbit.task').controller('changeCategoryModalInstanceCtrl', function ($uibModalInstance) {
	var ccm = this;
	ccm.ok = function () {
		$uibModalInstance.close('ok');
	};
	ccm.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});


angular.module('quickRabbit.task').controller('AddAddress', function ($uibModalInstance, toastr, user, $location, $state, $scope, $translate) {
	var ata = this;
	ata.editaddressdata = user;
	$scope.location = {};
	ata.addressList = {};
	ata.addressList.location = { lat: '', lng: '' };
	ata.placeChanged = function () {
		ata.place = this.getPlace();
		ata.addressList.location.lat = ata.place.geometry.location.lat();
		ata.addressList.location.lng = ata.place.geometry.location.lng();
		ata.availability = 2;
		var locationa = ata.place;
		ata.editaddressdata.line1='';
		ata.editaddressdata.street='';
		
		if(locationa.name){
			ata.editaddressdata.line1 =locationa.name;
		}

		for (var i = 0; i < locationa.address_components.length; i++) {
			for (var j = 0; j < locationa.address_components[i].types.length; j++) {
				if (locationa.address_components[i].types[j] == 'neighborhood') {
					if(ata.editaddressdata.line1 !=locationa.address_components[i].long_name){
					if(ata.editaddressdata.line1 !=''){
							ata.editaddressdata.line1 = ata.editaddressdata.line1+','+ locationa.address_components[i].long_name;
					}else{
							ata.editaddressdata.line1=locationa.address_components[i].long_name;
					}
					}			
				}
				if (locationa.address_components[i].types[j] == 'route' ) {
					if(ata.editaddressdata.line1 != locationa.address_components[i].long_name){
					if(ata.editaddressdata.street !='' ){
							ata.editaddressdata.street = ata.editaddressdata.street +','+locationa.address_components[i].long_name;
					}else{
							ata.editaddressdata.street=locationa.address_components[i].long_name;
					}
					}
					
				}
				if (locationa.address_components[i].types[j] == 'street_number') {
					if(ata.editaddressdata.street !=''){
							ata.editaddressdata.street = ata.editaddressdata.street +','+locationa.address_components[i].long_name;
					}else{
							ata.editaddressdata.street=locationa.address_components[i].long_name;
					}
					
				}
				if (locationa.address_components[i].types[j] == 'sublocality_level_1') {
					if(ata.editaddressdata.street !=''){
							ata.editaddressdata.street = ata.editaddressdata.street +','+locationa.address_components[i].long_name;
					}else{
							ata.editaddressdata.street=locationa.address_components[i].long_name;
					}
					
				}
				if (locationa.address_components[i].types[j] == 'locality') {

					ata.editaddressdata.city = locationa.address_components[i].long_name;
				}
				if (locationa.address_components[i].types[j] == 'country') {

					ata.editaddressdata.country = locationa.address_components[i].long_name;
				}
				if (locationa.address_components[i].types[j] == 'postal_code') {

					ata.editaddressdata.zipcode = locationa.address_components[i].long_name;
				}
				if (locationa.address_components[i].types[j] == 'administrative_area_level_1' || locationa.address_components[i].types[j] == 'administrative_area_level_2') {
					ata.editaddressdata.state = locationa.address_components[i].long_name;
				}
			}
		}


    };
	ata.ok = function (isValid) {
		//console.log("isValid", isValid);
		if (isValid == true) {
			//console.log("ata", ata);
			$uibModalInstance.close(ata);
		} else {
			$translate('INVALID DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
		}
	};
	ata.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});

angular.module('quickRabbit.task').controller('DeleteAddress', function ($uibModalInstance, user, $state) {
	var data = this;
	data.ok = function () {
		$uibModalInstance.close();
	};
	data.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});





angular.module('quickRabbit.task').directive('setClassWhenAtTop', function ($window) {
    var $win = angular.element($window); // wrap window object as jQuery object

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var topClass = attrs.setClassWhenAtTop, // get CSS class from directive's attribute value
                offsetTop = element.offset().top; // get element's top relative to the document

            $win.on('scroll', function (e) {
                if ($win.scrollTop() >= offsetTop) {
                    element.addClass(topClass);
                } else {
                    element.removeClass(topClass);
                }
            });
        }
    };
})
