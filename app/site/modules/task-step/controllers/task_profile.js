angular.module('quickRabbit.task').controller('taskProfileCtrl', taskProfileCtrl);
taskProfileCtrl.$inject = ['$scope', '$rootScope', '$location', '$stateParams', '$uibModal', 'TaskService', 'TaskProfileResolve', 'toastr', '$state', 'CurrentuserResolve', '$translate'];
function taskProfileCtrl($scope, $rootScope, $location, $stateParams, $uibModal, TaskService, TaskProfileResolve, toastr, $state, CurrentuserResolve, $translate) {
    var tpc = this;

    if (angular.isDefined($stateParams.taskerId)) {
        tpc.taskerId = $stateParams.taskerId;
    }
    if (angular.isDefined($stateParams.slug)) {
        tpc.slug = $stateParams.slug;
    }
    if (angular.isDefined($stateParams.task)) {
        tpc.task = $stateParams.task;
    }

    tpc.currentUserId = CurrentuserResolve.user_id;

    tpc.taskDetailInfo = {};
    tpc.availableSymbal = false;
    if (TaskProfileResolve) {
        tpc.taskDetailInfo = TaskProfileResolve;
        if (tpc.taskDetailInfo.availability == 0) {
            tpc.availabilityValue = "Employee is Unavailable";
            tpc.availableSymbal = false;
        } else {
            tpc.availabilityValue = "Employee is Available";
            tpc.availableSymbal = true;
        }
        tpc.questionValue = "Question is Unavailable";
        tpc.answerValue = "Answer is Unavailable";

        if (tpc.taskDetailInfo.profile_details[0]) {
            if (tpc.taskDetailInfo.profile_details[0].answer) {
                tpc.answerValue = tpc.taskDetailInfo.profile_details;
            }
            if (tpc.taskDetailInfo.profile_details[0].question) {
                tpc.questionValue = tpc.taskDetailInfo.profile_details;
            }
        }
    } else {
        $translate('WE ARE LOOKING FOR THIS TROUBLE SORRY UNABLE TO FETCH DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
    }


    TaskService.taskerreviews(tpc.taskerId).then(function (response) {

        tpc.taskprofile = response[0];
        tpc.taskertempid = tpc.taskprofile[0]._id;

        tpc.profilelength = tpc.taskprofile.length;
        tpc.overallRating = 0;
        angular.forEach(tpc.taskprofile, function (value, key) {
            if (value.rate) {
                tpc.overallRating = tpc.overallRating + value.rate.rating;
            }
        });
        tpc.overallrate = tpc.overallRating / tpc.profilelength;

        if (tpc.taskprofile[0].createdAt) {
            tpc.convertdate = new Date(tpc.taskprofile[0].createdAt);
        }
        tpc.dateConversion = tpc.convertdate.getFullYear();
        tpc.induvidualrating = parseInt(response[1]);

    }, function (err) {
        toastr.error(err);
    });

}
