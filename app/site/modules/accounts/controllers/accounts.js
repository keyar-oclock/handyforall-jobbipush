angular.module('quickRabbit.accounts').controller('accountsCtrl', accountsCtrl);

accountsCtrl.$inject = ['$scope', 'MainService', 'accountService', 'accountServiceResolve', '$filter', '$uibModal', '$location', 'toastr', '$timeout', 'Slug', '$state', '$window', '$anchorScroll', 'AuthenticationService', 'sweet', '$stateParams', '$translate'];
function accountsCtrl($scope, MainService, accountService, accountServiceResolve, $filter, $uibModal, $location, toastr, $timeout, Slug, $state, $window, $anchorScroll, AuthenticationService, sweet, $stateParams, $translate) {

	var acc = this;
	var stateparamcontent = $stateParams;
	acc.taskervariable = AuthenticationService.GetCredentials();
	var user = AuthenticationService.GetCredentials();
	if (accountServiceResolve[0]) {
		acc.user = accountServiceResolve[0] || {};
	}

	if (acc.user.role == 'tasker') {
		acc.radius = acc.user.radius;
		acc.unit = 'km';
		if (acc.user.availability == 1) {
			acc.availabilityvalue = true;
		} else {
			acc.availabilityvalue = false;
		}
		if (acc.user.location) {
			var latlng = new google.maps.LatLng(acc.user.location.lat, acc.user.location.lng);
			var geocoder = geocoder = new google.maps.Geocoder();
			geocoder.geocode({ 'latLng': latlng }, function (results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					if (results[1]) {
						acc.taskerareaaddress = results[1].formatted_address;
						acc.dummyAddress = 1;
					}
				}
			});
		}
	}
	accountService.getsettings().then(function (response) {
		acc.getsettings = response;
		acc.walletMinAmt = (response.settings.wallet.amount.minimum * $scope.DefaultCurrency[0].value).toFixed(2);
		acc.walletMaxAmt = (response.settings.wallet.amount.maximum * $scope.DefaultCurrency[0].value).toFixed(2);
		acc.walletMidAmt = ((response.settings.wallet.amount.maximum / 2) * $scope.DefaultCurrency[0].value).toFixed(2);
	});

	$scope.fileupload = function fileupload($files, $event, $rejectedFiles) {
		if ($files) {
			if ($files.length && $files[0].size < 2097152) {
				for (var i = 0; i < $files.length; i++) {
					$scope.avatar = $files[i];
					btc.data.avatar = $scope.avatar;
				}
			} else {
				$translate('IMAGE SIZE IS SHOULD NOT BE LARGER THEN 1 MB').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
			}
		}
	};

	acc.filter = $location.$$search;
	if (stateparamcontent.status == undefined) {
		if (acc.user.role == 'tasker') {
			acc.SettingsTab = [
				{
					"heading": "ACCOUNT",
					"template": "app/site/modules/accounts/views/account.tab.html",
					"active": true,
					"icon": "users"
				},
				{
					"heading": "PASSWORD",
					"template": "app/site/modules/accounts/views/password.tab.html",
					"active": false
				},
				{
					"heading": "Account Info",
					"template": "app/site/modules/accounts/views/accountinfo.tab.html",
					"active": false
				},
				{
					"heading": "CATEGORY",
					"template": "app/site/modules/accounts/views/category.tab.html",
					"active": false
				},
				{
					"heading": "AVAILABILITY",
					"template": "app/site/modules/accounts/views/availability.tab.html",
					"active": false
				},
				{
					"heading": "PROFILE DETAILS",
					"template": "app/site/modules/accounts/views/profileinfo.tab.html",
					"active": false
				},
				{
					"heading": "Questions",
					"template": "app/site/modules/accounts/views/questions.tab.html",
					"active": false
				},
				{
					"heading": "Documents",
					"template": "app/site/modules/accounts/views/documents.tab.html",
					"active": false
				},
				{
					"heading": "JOB DETAILS",
					"template": "app/site/modules/accounts/views/task-invitation.tab.html",
					"active": false
				},
				{
					"heading": "Transaction",
					"template": "app/site/modules/accounts/views/transaction.tab.html",
					"active": false
				},
				{
					"heading": "REVIEWS",
					"template": "app/site/modules/accounts/views/reviews.tab.html",
					"active": false
				},
				{
					"heading": "DEACTIVATE",
					"template": "app/site/modules/accounts/views/deactivate.tab.html",
					"active": false
				}
			];
		}
	} else {
		if (acc.user.role == 'tasker') {
			acc.SettingsTab = [
				{
					"heading": "ACCOUNT",
					"template": "app/site/modules/accounts/views/account.tab.html",
					"active": false
				},
				{

					"heading": "PASSWORD",
					"template": "app/site/modules/accounts/views/password.tab.html",
					"active": false
				},
				{
					"heading": "Account Information",
					"template": "app/site/modules/accounts/views/accountinfo.tab.html",
					"active": false
				},
				{
					"heading": "CATEGORY",
					"template": "app/site/modules/accounts/views/category.tab.html",
					"active": false
				},
				{
					"heading": "AVAILABILITY",
					"template": "app/site/modules/accounts/views/availability.tab.html",
					"active": false
				},
				{
					"heading": "PROFILE DETAILS",
					"template": "app/site/modules/accounts/views/profileinfo.tab.html",
					"active": false
				},
				{
					"heading": "JOB DETAILS",
					"template": "app/site/modules/accounts/views/task-invitation.tab.html",
					"active": true
				},
				{
					"heading": "TRANSACTION",
					"template": "app/site/modules/accounts/views/transaction.tab.html",
					"active": false
				},
				{
					"heading": "REVIEWS",
					"template": "app/site/modules/accounts/views/reviews.tab.html",
					"active": false
				},
				{
					"heading": "DEACTIVATE",
					"template": "app/site/modules/accounts/views/deactivate.tab.html",
					"active": false
				}
			];
		}
	}
	if (stateparamcontent.status == undefined) {
		if (acc.user.role == 'user') {
			if (acc.user.type == 'facebook') {
				acc.SettingsTab = [
					{
						"heading": "ACCOUNT",
						"template": "app/site/modules/accounts/views/account.tab.html",
						"active": true

					},
					{
						"heading": "JOB DETAILS",
						"template": "app/site/modules/accounts/views/task-details.tab.html",
						"active": false
					},
					{
						"heading": "INVITE FRIENDS",
						"template": "app/site/modules/accounts/views/invitefriend.tab.html",
						"active": false
					},
					{
						"heading": "WALLET",
						"template": "app/site/modules/accounts/views/wallet.tab.html",
						"active": false
					},
					{
						"heading": "REVIEWS",
						"template": "app/site/modules/accounts/views/reviews.tab.html",
						"active": false
					},
					{
						"heading": "TRANSACTION",
						"template": "app/site/modules/accounts/views/user-transaction.tab.html",
						"active": false
					},
					{
						"heading": "DEACTIVATE",
						"template": "app/site/modules/accounts/views/deactivate.tab.html",
						"active": false
					}
				];
			} else {
				acc.SettingsTab = [
					{
						"heading": "ACCOUNT",
						"template": "app/site/modules/accounts/views/account.tab.html",
						"active": true

					},
					{
						"heading": "PASSWORD",
						"template": "app/site/modules/accounts/views/password.tab.html",
						"active": false

					},
					{
						"heading": "JOB DETAILS",
						"template": "app/site/modules/accounts/views/task-details.tab.html",
						"active": false
					},
					{
						"heading": "INVITE FRIENDS",
						"template": "app/site/modules/accounts/views/invitefriend.tab.html",
						"active": false
					},
					{
						"heading": "WALLET",
						"template": "app/site/modules/accounts/views/wallet.tab.html",
						"active": false
					},
					{
						"heading": "REVIEWS",
						"template": "app/site/modules/accounts/views/reviews.tab.html",
						"active": false
					},
					{
						"heading": "TRANSACTION",
						"template": "app/site/modules/accounts/views/user-transaction.tab.html",
						"active": false
					},
					{
						"heading": "DEACTIVATE",
						"template": "app/site/modules/accounts/views/deactivate.tab.html",
						"active": false
					}
				];
			}
		}
	} else {
		if (acc.user.role == 'user') {
			if (acc.user.type == 'facebook') {
				acc.SettingsTab = [
					{
						"heading": "ACCOUNT",
						"template": "app/site/modules/accounts/views/account.tab.html",
						"active": false

					},
					{
						"heading": "JOB DETAILS",
						"template": "app/site/modules/accounts/views/task-details.tab.html",
						"active": true
					},
					{
						"heading": "INVITE FRIENDS",
						"template": "app/site/modules/accounts/views/invitefriend.tab.html",
						"active": false
					},
					{
						"heading": "WALLET",
						"template": "app/site/modules/accounts/views/wallet.tab.html",
						"active": false
					},
					{
						"heading": "REVIEWS",
						"template": "app/site/modules/accounts/views/reviews.tab.html",
						"active": false
					},
					{
						"heading": "TRANSACTION",
						"template": "app/site/modules/accounts/views/user-transaction.tab.html",
						"active": false
					},
					{
						"heading": "DEACTIVATE",
						"template": "app/site/modules/accounts/views/deactivate.tab.html",
						"active": false
					}
				];
			} else {
				acc.SettingsTab = [
					{
						"heading": "ACCOUNT",
						"template": "app/site/modules/accounts/views/account.tab.html",
						"active": false

					},
					{
						"heading": "PASSWORD",
						"template": "app/site/modules/accounts/views/password.tab.html",
						"active": false

					},
					{
						"heading": "JOB DETAILS",
						"template": "app/site/modules/accounts/views/task-details.tab.html",
						"active": true
					},
					{
						"heading": "INVITE FRIENDS",
						"template": "app/site/modules/accounts/views/invitefriend.tab.html",
						"active": false
					},
					{
						"heading": "WALLET",
						"template": "app/site/modules/accounts/views/wallet.tab.html",
						"active": false
					},
					{
						"heading": "REVIEWS",
						"template": "app/site/modules/accounts/views/reviews.tab.html",
						"active": false
					},
					{
						"heading": "TRANSACTION",
						"template": "app/site/modules/accounts/views/user-transaction.tab.html",
						"active": false
					},
					{
						"heading": "DEACTIVATE",
						"template": "app/site/modules/accounts/views/deactivate.tab.html",
						"active": false
					}
				];
			}
		}
	}

	acc.go = function go(route) {
		$state.go(route);
	};

	acc.accountMode = true;
	acc.saveAccount = function saveAccount(isValid) {
		if (isValid) {
			accountService.saveAccount(acc.user).then(function (response) {
				$translate('SAVED SUCCESSFULLY').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
				$location.hash('editaccountdiv');
				$anchorScroll();
				$location.url($location.path());
			}, function (err) {
				if (err.msg) {
					toastr.success(err.msg);
				} else {
					$translate('UNABLE TO SAVE YOUR DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
				}
			});
		} else {
			$translate('UNABLE TO SAVE YOUR DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
		}
	};

	acc.saveTaskerQuestion = function saveTaskerQuestion(isValid) {
		if (isValid) {
			if(acc.user.disability !== 'Other')
				acc.user.disability_reason = '';

			accountService.saveTaskerQuestion(acc.user).then(function (response) {
				$translate('SAVED SUCCESSFULLY').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
				$state.go('account');
			}, function (err) {
				if (err.msg) {
					toastr.success(err.msg);
				} else {
					$translate('UNABLE TO SAVE YOUR DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
				}
			});
		} else {
			$translate('UNABLE TO SAVE YOUR DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
		}
	};

	acc.saveTaskerDocument = function saveTaskerDocument(isValid) {
		if (isValid) {
			accountService.saveTaskerDocument(acc.user).then(function (response) {
				$translate('SAVED SUCCESSFULLY').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
				$state.go('account');
			}, function (err) {
				if (err.msg) {
					toastr.success(err.msg);
				} else {
					$translate('UNABLE TO SAVE YOUR DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
				}
			});
		} else {
			$translate('UNABLE TO SAVE YOUR DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
		}
	};

	// Lat Lon Calculation by venki
	acc.placeChanged = function () {
		acc.user.address.line1 = "";
		acc.user.address.line2 = "";
		acc.user.address.city = "";
		acc.user.address.state = "";
		acc.user.address.country = "";
		acc.user.address.zipcode = "";

		acc.place = this.getPlace();
		var UserDetails = {};
		UserDetails.location = {};
		UserDetails.location.lng = acc.place.geometry.location.lng();
		UserDetails.location.lat = acc.place.geometry.location.lat();
		acc.user.lat = UserDetails.location.lat;
		acc.user.lng = UserDetails.location.lng;

		var locationa = acc.place;
		acc.user.address.line1 = acc.place.formatted_address;

		if (locationa.name) {
			acc.user.address.line1 = locationa.name;
		}

		for (var i = 0; i < locationa.address_components.length; i++) {
			for (var j = 0; j < locationa.address_components[i].types.length; j++) {
				if (locationa.address_components[i].types[j] == 'neighborhood') {
					if (acc.user.address.line1 != locationa.address_components[i].long_name) {
						if (acc.user.address.line1 != '') {
							acc.user.address.line1 = acc.user.address.line1 + ',' + locationa.address_components[i].long_name;
						} else {
							acc.user.address.line1 = locationa.address_components[i].long_name;
						}
					}
				}
				if (locationa.address_components[i].types[j] == 'route') {
					if (acc.user.address.line1 != locationa.address_components[i].long_name) {
						if (acc.user.address.line2 != '') {
							acc.user.address.line2 = acc.user.address.line2 + ',' + locationa.address_components[i].long_name;
						} else {
							acc.user.address.line2 = locationa.address_components[i].long_name;
						}
					}

				}
				if (locationa.address_components[i].types[j] == 'street_number') {
					if (acc.user.address.line2 != '') {
						acc.user.address.line2 = acc.user.address.line2 + ',' + locationa.address_components[i].long_name;
					} else {
						acc.user.address.line2 = locationa.address_components[i].long_name;
					}

				}
				if (locationa.address_components[i].types[j] == 'sublocality_level_1') {
					if (acc.user.address.line2 != '') {
						acc.user.address.line2 = acc.user.address.line2 + ',' + locationa.address_components[i].long_name;
					} else {
						acc.user.address.line2 = locationa.address_components[i].long_name;
					}

				}
				if (locationa.address_components[i].types[j] == 'locality') {

					acc.user.address.city = locationa.address_components[i].long_name;
				}
				if (locationa.address_components[i].types[j] == 'country') {

					acc.user.address.country = locationa.address_components[i].long_name;
				}
				if (locationa.address_components[i].types[j] == 'postal_code') {

					acc.user.address.zipcode = locationa.address_components[i].long_name;
				}
				if (locationa.address_components[i].types[j] == 'administrative_area_level_1' || locationa.address_components[i].types[j] == 'administrative_area_level_2') {
					acc.user.address.state = locationa.address_components[i].long_name;
				}
			}
		}

		/*	for (var i = 0; i < locationa.address_components.length; i++) {
				for (var j = 0; j < locationa.address_components[i].types.length; j++) {

					if (locationa.address_components[i].types[j] == 'sublocality_level_2') {

					}
					if (locationa.address_components[i].types[j] == 'route' )  {
							acc.user.address.line2 = locationa.address_components[i].long_name;
					}
					if (locationa.address_components[i].types[j] == 'locality') {
						acc.user.address.city = locationa.address_components[i].long_name;

					}
					if (locationa.address_components[i].types[j] == 'administrative_area_level_1') {
						acc.user.address.state = locationa.address_components[i].long_name;

					}
					if (locationa.address_components[i].types[j] == 'country') {
						acc.user.address.country = locationa.address_components[i].long_name;
					}
					if (locationa.address_components[i].types[j] == 'postal_code') {
						acc.user.address.zipcode = parseInt(locationa.address_components[i].long_name);

					}
				}
		}*/
	};
	// End Lat Lon Calculation by venki

	// Password Tab
	acc.password = {};
	acc.password.userId = acc.user._id;

	acc.savePassword = function savePassword(isvalid, data) {
		if (isvalid) {
			if (data.newpassword == data.new_confirmed) {
				accountService.savePassword(data).then(function (response) {
					$translate('SAVED SUCCESSFULLY').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
					$state.go('account');
				}, function (err) {
					if (err.message) {
						toastr.error(err.message);
					} else {
						$translate('PLEASE TYPE A DIFFERENT PASSWORD').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
					}
				});
			} else {
				$translate('CONFIRM PASSWORD IS NOT MATCH').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });

			}
		} else {
			$translate('FORM IS INVALID').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
		}
	};

	acc.banking = {};
	acc.banking = acc.user.banking;
	acc.saveaccountinfo = function saveaccountinfo(isvalid, data) {
		acc.banking.userId = acc.user._id;
		if (isvalid) {
			accountService.saveaccountinfo(data).then(function (response) {
				$translate('SAVED SUCCESSFULLY').then(function (headline) {
					toastr.success(headline);
				}, function (translationId) {
					toastr.success(headline);
				});
				$state.go('account');
			}, function (err) {
				if (err.message) {
					//toastr.error(err.message);
				} else {
					//$translate('PLEASE TYPE A DIFFERENT PASSWORD').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
				}
			});
		} else {
			$translate('please fill all mandatory fileds').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
		}
	};


	acc.FacebookInviteFriends = function FacebookInviteFriends() {

		accountService.getsettings().then(function (response) {
			//console.log("<<<<<<<<<<",response);
			acc.getsettingdata = response;

			accountService.getseosetting().then(function (seoresponse) {
				acc.getseosetting = seoresponse;

				//console.log(">>>>>>>>>>>>",acc.getseosetting);
				var invite = {};
				invite.name = acc.getsettingdata.settings.site_title;
				invite.link = acc.getsettingdata.settings.site_url;
				invite.description = acc.getseosetting.settings.meta_description;
				invite.picture = acc.getsettingdata.settings.site_url + acc.getsettingdata.settings.logo;
				//console.log("invite",invite)
				if (acc.getsettingdata) {
					FB.ui({ method: 'send', name: invite.name, link: invite.link, description: invite.description, picture: invite.picture });
				}
			})
		})

	};

	acc.addReview = function addReview(taskdetails) {
		accountService.gettaskreview(taskdetails._id).then(function (response) {
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'app/site/modules/accounts/views/userreview.modal.tab.html',
				controller: 'addReviewModal',
				controllerAs: 'ARM',
				resolve: {
					TaskDetails: function () {
						return taskdetails;
					}
				}
			});

			modalInstance.result.then(function (data) {
				accountService.addUserReview(data).then(function (respo) {
					acc.reviewdata = respo;
					acc.GetTaskList("completed");
				});
			});

		});
	}


	$scope.$watchCollection('DefaultCurrency', function (newNames, oldNames) {
		if (acc.getsettings.settings) {
			acc.walletMinAmt = (acc.getsettings.settings.wallet.amount.minimum * newNames[0].value).toFixed(2);
			acc.walletMaxAmt = (acc.getsettings.settings.wallet.amount.maximum * newNames[0].value).toFixed(2);
			acc.walletMidAmt = ((acc.getsettings.settings.wallet.amount.maximum / 2) * newNames[0].value).toFixed(2);
		}
	});

	acc.changeWalletAmt = function changeWalletAmt(value) {
		acc.walletMinAmt = (acc.getsettings.settings.wallet.amount.minimum * $scope.DefaultCurrency[0].value).toFixed(2);
		acc.walletMaxAmt = (acc.getsettings.settings.wallet.amount.maximum * $scope.DefaultCurrency[0].value).toFixed(2);
		acc.walletMidAmt = ((acc.getsettings.settings.wallet.amount.maximum / 2) * $scope.DefaultCurrency[0].value).toFixed(2);
	}

	acc.changeWallet = function changeWallet(value) {
		acc.walletAamount = (parseFloat(value)).toFixed(2);
	}

	acc.savewallet = function savewallet(data, savewallet) {

		acc.walletMinAmt = (acc.getsettings.settings.wallet.amount.minimum * $scope.DefaultCurrency[0].value).toFixed(2);
		acc.walletMaxAmt = (acc.getsettings.settings.wallet.amount.maximum * $scope.DefaultCurrency[0].value).toFixed(2);
		acc.walletMidAmt = ((acc.getsettings.settings.wallet.amount.maximum / 2) * $scope.DefaultCurrency[0].value).toFixed(2);

		var detaileddata = {};
		detaileddata.data = data.amount;
		detaileddata.currencyvalue = savewallet;

		if (detaileddata.data) {
			if (!((parseFloat(data.amount) >= acc.walletMinAmt) && (parseFloat(data.amount) <= acc.walletMaxAmt))) {
				$translate('PLEASE ENTER THE AMOUNT TO ADD TO THE WALLET').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
			} else {
				var modalInstance = $uibModal.open({
					animation: true,
					templateUrl: 'app/site/modules/accounts/views/wallet.modal.tab.html',
					controller: 'WalletRechargeModal',
					controllerAs: 'WRM',
					resolve: {
						Rechargeamount: function () {
							return detaileddata;
						}
					}
				});

				modalInstance.result.then(function (data) {
					user = acc.user._id;
					accountService.updatewalletdata(data, user).then(function (response) {
						if (response.statusCode == 402) {
							toastr.error("Your card number is incorrect");
						} else {
							$translate('WALLET MONEY HAS BEEN UPDATED SUCCESSFULLY').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
							acc.getwalletdetails = response;
							wallet.amount = "";
						}
					}, function (err) {
					});
				}, function () { });
			}
		} else {
			$translate('PLEASE ENTER THE AMOUNT TO ADD TO THE WALLET').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
		}
	};

	//Category
	accountService.getCategories().then(function (respo) {
		acc.categories = respo;
	});

	accountService.getCategoriesofuser(acc.user._id).then(function (respo) {
		acc.usercategories = respo;
		acc.updatecat = function () {
			accountService.getCategoriesofuser(acc.user._id).then(function (respo) {
				acc.usercategories = respo;
			});
		}
		accountService.getExperience().then(function (respo) {
			acc.experiences = respo;
		});
	});

	// Payment
	acc.payment = function payment(isValid, formdata) {
		if (isValid) {
			accountService.confirmtask(acc.taskPayment).then(function (err, response) {
				$translate('SAVED SUCCESSFULLY').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
			}, function (err) {
				if (err.msg) {
					toastr.error(err.msg);
				} else {
					$translate('UNABLE TO SAVE YOUR DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
				}
			});
		} else {
			$translate('PLEASE ENTER THE VALID DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
		}
	};

	MainService.getDefaultCurrency().then(function (response) {
		acc.DefaultCurrency = response;
	});

	acc.categoryModal = function (category) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'app/site/modules/accounts/views/category.modal.tab.html',
			controller: 'CategoriesModalInstanceCtrl',
			controllerAs: 'ACM',
			resolve: {
				experiences: function () {
					return acc.experiences;
				},
				defaultcurrency: function () {
					return $scope.DefaultCurrency;
				},
				user: function (accountService) {
					if (category) {
						return accountService.edit(acc.user._id);
					} else {
						return acc.user;
					}
				},
				categories: function () {
					return acc.categories;
				},
				category: function () {
					return category;
				}
			}
		});

		modalInstance.result.then(function (selectedCategoryData, isValid) {
			selectedCategoryData.hour_rate = selectedCategoryData.hour_rate / $scope.DefaultCurrency[0].value;
			accountService.updateCategory(selectedCategoryData).then(function (response) {
				$translate('UPDATED SUCCESSFULLY').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
				acc.updatecat();
			}, function (err) {
				if (err.msg) {
					toastr.error(err.msg);
				} else {
					$translate('PLEASE ENTER THE VALID DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
				}
			});
		});
	}

	acc.deletecategory = function (category) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'app/site/modules/accounts/views/deletecategory.modal.tab.html',
			controller: 'DeleteCategoriesModalInstanceCtrl',
			controllerAs: 'DACM',
			resolve: {
				user: function () {
					return acc.user;
				},
				category: function () {
					return category;
				}
			}
		});

		modalInstance.result.then(function (deletecategorydata) {
			accountService.deleteCategory(deletecategorydata).then(function (response) {
				acc.updatecat();
			}, function () {
			});
		}, function () {
		});
	};

	accountService.getQuestion().then(function (respo) {
		acc.getQuestion = respo;
	});

	if (acc.user.profile_details) {
		if (acc.user.profile_details.length > 0) {
			acc.profileDetails = acc.user.profile_details.reduce(function (total, current) {
				total[current.question] = current.answer;
				return total;
			}, {});
		} else {
			acc.profileDetails = [];
			acc.user.profile_details = [];
		}
	}
	acc.saveProfile = function saveProfile() {
		var i = 0;
		for (var key in acc.profileDetails) {
			if (acc.user.profile_details.filter(function (obj) { return obj.question === key; })[0]) {
				acc.user.profile_details[i].answer = acc.profileDetails[key];
			} else {
				acc.user.profile_details.push({ 'question': key, 'answer': acc.profileDetails[key] });
			}
			i++;
		}
		accountService.saveProfile(acc.user).then(function (response) {
			$translate('UPDATED SUCCESSFULLY').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
		}, function (err) {
			if (err.msg) {
				$scope.addAlert(err.msg);
			} else {
				$translate('UNABLE TO SAVE YOUR DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
			}
		});
	}

	acc.taskitemsPerPage = 2;
	acc.tasktotalItem = 0;
	acc.taskInvitation = [];
	acc.getwalletdetails = {};
	acc.getsettings = {};
	acc.taskInvitationDetails = [];
	acc.tasker = [];
	acc.getTaskDetailsByStausResponse = false;

	acc.getTaskDetailsByStaus = function getTaskDetailsByStaus(status, page) {
		acc.taskInvitation = [];
		acc.getTaskDetailsByStausResponse = false;
		acc.tasktotalItem = 0;

		if (status == '6' || status == '7') {
			status = 'completed';
		}
		else if (status == '8') {
			status = 'cancelled';
		}
		else if (status == '2' || status == '3' || status == '4' || status == '5') {
			status = 'ongoing';
		}
		else if (status == '1') {
			status = 'assigned';
		}

		if (page == undefined) {
			acc.CurrentPage = 1;
		} else {
			acc.CurrentPage = page;
		}

		acc.currentStatus = status;
		accountService.getTaskDetailsByStaus(acc.user._id, acc.currentStatus, page, acc.taskitemsPerPage).then(function (response) {
			if (response.length > 0) {
				acc.taskInvitationDetails = response;
				acc.taskInvitation = response[0].TaskDetails;
				console.log("acc.taskInvitation",acc.taskInvitation);
				acc.tasktotalItem = response[0].count;
			}
			acc.getTaskDetailsByStausResponse = true;
		});
	}


	acc.getUserTaskDetailsByStaus = function getUserTaskDetailsByStaus(status, page) {
		acc.taskInvitation = [];
		acc.getUserTaskDetailsByStaus = false;
		acc.tasktotalItem = 0;
		if (status == '6' || status == '7') {
			status = 'completed';
		}
		else if (status == '8') {
			status = 'cancelled';
		}
		else if (status == '2' || status == '3' || status == '4' || status == '5') {
			status = 'ongoing';
		}
		else if (status == '1') {
			status = 'assigned';
		}
		acc.currentStatus = status;
		accountService.getUserTaskDetailsByStaus(acc.user._id, acc.currentStatus, page, acc.taskitemsPerPage).then(function (response) {
			if (response.length > 0) {
				acc.taskInvitationDetails = response;
				acc.taskInvitation = response[0].TaskDetails;
				acc.tasktotalItem = response[0].count;
			}
			acc.getTaskDetailsByStausResponse = true;
		});
	}



	accountService.getwalletdetails(acc.user._id).then(function (response) {
		if (response) {
			acc.getwalletdetails = response;
		} else {
			acc.getwalletdetails.total = 0;
		}
	});

	acc.getTasker = function getTasker() {
		acc.tasker = [];
		accountService.getTasker(acc.user._id).then(function (response) {
			if (response.length > 0) {
				acc.tasker = response[0].TaskDetails;
			}
		});
	}

	acc.taskertransitemsPerPage = 5;
	acc.taskertranstotalItem = 0;
	acc.taskertransCurrentPage = 1;
	acc.getTransactionHis = function getTransactionHis(page) {
		accountService.getTransactionHis(acc.user._id, page, acc.taskertransitemsPerPage).then(function (response) {
			if (response) {
				acc.transcationhis = response.result;
				acc.taskertranstotalItem = response.count;
			}
		});
	}
	acc.transitemsPerPage = 5;
	acc.transtotalItem = 0;
	acc.transCurrentPage = 1;
	acc.getUserTransaction = function getUserTransaction(page) {
		accountService.getUserTransaction(acc.user._id, page, acc.transitemsPerPage).then(function (response) {
			if (response) {
				acc.usertranscation = response.result;
				acc.transtotalItem = response.count;
			}
		});
	}

	accountService.getcancelreason(acc.user.role).then(function (response) {
		if (response.length > 0) {
			acc.getcancelreason = response;
		}
	});

	acc.updatetaskstatus = function updatetaskstatus(taskid, status) {
		var data = {};
		data.taskid = taskid;
		data.status = status;
		accountService.updatetaskstatus(data).then(function (response) {
			if (response.error) {
				$translate(response.error).then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
			} else {
				if (response.status == 3) {
					$translate('YOU START-OFF THE JOB').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
				}
				else if (response.status == 4) {
					$translate('YOU ARRIVED TO THE JOB LOCATION').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
				}
				else {
					$translate('YOU STARTED JOB').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
				}
			}
			acc.getTaskDetailsByStaus("ongoing");
			acc.currentPage = 1;
		});
	}

	acc.TaskTranscationViewModal = function (index) {
		var tasktranscationhis = acc.transcationhis[index];
		var transcation = {};
		transcation.date = tasktranscationhis.createdAt;
		transcation.invoice = tasktranscationhis.invoice;
		transcation.bookingid = tasktranscationhis._id;
		if (tasktranscationhis.transactions) {
			transcation.transcationid = tasktranscationhis.transactions[0];
		}
		transcation.categoryname = tasktranscationhis.category.name;
		var taskerskills = tasktranscationhis.tasker.taskerskills;
		angular.forEach(taskerskills, function (key, value) {
			if (key.childid == tasktranscationhis.category._id) {
				transcation.perHour = key.hour_rate;
			}
		});
		transcation.worked_hours = tasktranscationhis.invoice.worked_hours;
		transcation.username = tasktranscationhis.user.username;
		transcation.addresss = tasktranscationhis.billing_address.city;
		transcation.tasker_earn = tasktranscationhis.invoice.amount.admin_commission;
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'app/site/modules/accounts/views/model/transaction.modal.html',
			controller: 'TaskTranscationViewModal',
			controllerAs: 'TTEMS',
			size: 'lg',
			resolve: {
				TaskDetails: function () {
					return transcation;
				},
				task: function () {
					return acc.transcationhis[index];
				},
				defaultcurrency: function () {
					return $scope.DefaultCurrency;
				},
				getsettings: function () {
					return acc.getsettings;
				}
			}
		});

		modalInstance.result.then(function (data) {
		}, function () {
		});
	}


	acc.TaskUserTranscationViewModal = function (index) {
		var tasktranscationhis = acc.usertranscation[index];
		var transcation = {};
		transcation.date = tasktranscationhis.createdAt;
		transcation.invoice = tasktranscationhis.invoice;
		transcation.bookingid = tasktranscationhis._id;
		if (tasktranscationhis.transactions) {
			transcation.transcationid = tasktranscationhis.transactions[0];
		}
		transcation.categoryname = tasktranscationhis.category.name;
		var taskerskills = tasktranscationhis.tasker.taskerskills;
		angular.forEach(taskerskills, function (key, value) {
			if (key.childid == tasktranscationhis.category._id) {
				transcation.perHour = key.hour_rate;
			}
		});
		transcation.worked_hours = tasktranscationhis.invoice.worked_hours;
		transcation.username = tasktranscationhis.user.username;
		transcation.addresss = tasktranscationhis.billing_address.city;
		transcation.tasker_earn = tasktranscationhis.invoice.amount.admin_commission;
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'app/site/modules/accounts/views/model/usertransaction.modal.html',
			controller: 'TaskUserTranscationViewModal',
			controllerAs: 'TTEMS',
			size: 'lg',
			resolve: {
				TaskDetails: function () {
					return transcation;
				},
				task: function () {
					return acc.usertranscation[index];
				},
				defaultcurrency: function () {
					return $scope.DefaultCurrency;
				},
				getsettings: function () {
					return acc.getsettings;
				}
			}
		});

		modalInstance.result.then(function (data) {
		}, function () {
		});
	}


	acc.taskerareaChanged = function () {
		acc.place = this.getPlace();
		acc.user.location = {};
		acc.user.location.lng = acc.place.geometry.location.lng();
		acc.user.location.lat = acc.place.geometry.location.lat();
		var locationa = acc.place;
		var dummy = locationa.address_components.filter(function (value) {
			return value.types[0] == "locality";
		}).map(function (data) {
			return data;
		});
		acc.dummyAddress = dummy.length;

		var staticmap = '';
		if(acc.user.radius) {
			staticmap = acc.GMapCircle(acc.user.location.lat, acc.user.location.lng, acc.user.radius);
            var html = '<img src="'+staticmap+'"/>';
            document.getElementById('staticmap').innerHTML = html;
            document.getElementById('staticmap').style.display = 'block';
		}
		else {
			document.getElementById('staticmap').style.display = 'none';
            staticmap = '';
		}
	};

	acc.changeRadius = function () {
		if(acc.unit == 'km') {
			acc.user.radius = acc.radius;
		}
		else {
			acc.user.radius = acc.radius * 0.621371192;
		}

		var staticmap = '';
		if(acc.user.radius) {
			staticmap = acc.GMapCircle(acc.user.location.lat, acc.user.location.lng, acc.user.radius);
            var html = '<img src="'+staticmap+'"/>';
            document.getElementById('staticmap').innerHTML = html;
            document.getElementById('staticmap').style.display = 'block';
		}
		else {
			document.getElementById('staticmap').style.display = 'none';
            staticmap = '';
		}
	};

	acc.GMapCircle = function(lat,lng,rad,detail=8) {

        var uri = 'https://maps.googleapis.com/maps/api/staticmap?';
        var staticMapSrc = 'center=' + lat + ',' + lng;
        //staticMapSrc += '&zoom=' + zoom;
        staticMapSrc += '&maptype=roadmap';
        staticMapSrc += '&size=600x200';
        staticMapSrc += '&key=AIzaSyAPegjsMnS1sfSZPOX5bBW8QlxJLQ_NkLs';
        staticMapSrc += '&path=color:0xff0000ff:weight:1';

        var r    = 6371;
        //rad *= 1.6093; // Change mile to km

        var pi   = Math.PI;

        var _lat  = (lat * pi) / 180;
        var _lng  = (lng * pi) / 180;
        var d    = (rad) / r;

        var i = 0;

        for(i = 0; i <= 360; i+=detail) {
            var brng = i * pi / 180;

            var pLat = Math.asin(Math.sin(_lat) * Math.cos(d) + Math.cos(_lat) * Math.sin(d) * Math.cos(brng));
            var pLng = ((_lng + Math.atan2(Math.sin(brng) * Math.sin(d) * Math.cos(_lat), Math.cos(d) - Math.sin(_lat) * Math.sin(pLat))) * 180) / pi;
            pLat = (pLat * 180) / pi;

           staticMapSrc += "|" + pLat + "," + pLng;
        }
        return uri + encodeURI(staticMapSrc);
    }

	acc.taskerconfirmpay = function (taskid, status) {

		var data = {};
		data.taskid = taskid;
		data.status = status;
		accountService.updateTaskcompletion(data).then(function (response) {
			console.log(data);
			if (response.status == 6) {
				toastr.success('Job Completed');
			}
			else {
				$translate('UNABLE TO SAVE YOUR DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
			}
			acc.getTaskDetailsByStaus("ongoing");
		});

	}

	acc.updateModalTask = function (index, status) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'app/site/modules/accounts/views/task-dispute.modal.html',
			controller: 'DisputeReviewModalInstanceCtrl',
			controllerAs: 'DNM',
			resolve: {
				TaskDetails: function () {
					return acc.taskList[index];
				},
				status: function () {
					return status;
				}
			}
		});

		modalInstance.result.then(function (data) {
			if (acc.taskInvitation.length > 0 && angular.isDefined(acc.taskInvitation[index]._id)) {
				accountService.updateTask(acc.taskInvitation[index]._id, status).then(function (response) {
					acc.taskInvitation.splice(index, 1);
				}, function (err) {
				});
			}

		}, function () {
		});

	}

	acc.TaskReviewModalSave = function (index) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'app/site/modules/accounts/views/task-review.modal.tab.html',
			controller: 'TaskReviewModalSave',
			controllerAs: 'TREMS',
			resolve: {
				TaskDetails: function () {
					return acc.taskList[index];
				}
			}
		});


		var userdata = acc.taskList[index];

		modalInstance.result.then(function (data) {
			var reviewdata = {};
			reviewdata.rating = data.rating;
			reviewdata.comments = data.comments;
			reviewdata.user = userdata.user;
			reviewdata.tasker = userdata.tasker._id;
			reviewdata.task = userdata._id;
			reviewdata.type = 'tasker';

			accountService.setReview(reviewdata).then(function (response) {
				acc.getreviewdetails;
			});
		});

	}





	acc.TaskReviewModal = function (index) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'app/site/modules/accounts/views/task-review.modal.html',
			controller: 'TaskReviewModalCtrl',
			controllerAs: 'TREM',
			resolve: {
				TaskDetails: function () {
					return acc.taskInvitation[index];
				}
			}
		});

		var userdata = acc.taskInvitation[index];
		modalInstance.result.then(function (data) {

			var reviewdata = {};
			reviewdata.rating = data.rating;
			reviewdata.comments = data.comments;
			reviewdata.user = userdata.user._id;
			reviewdata.tasker = userdata.tasker;
			reviewdata.task = userdata._id;
			reviewdata.type = "tasker";

			accountService.inserttaskerreview(reviewdata).then(function (response) {
				accountService.getReview(acc.user._id, acc.reviewListCurrentPage, acc.reviewListitemsPerPage).then(function (respo) {
				});
				acc.getTaskDetailsByStaus("completed");
			}, function (err) {
			});

		}, function () {
		});

	}
	/*	console.log("acc.taskInvitationDetails",acc.taskInvitation[0]);
	console.log("acc.taskInvitationDetails",acc.tasktotalItem);
	*/

	acc.TaskInviteViewModal = function (index) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'app/site/modules/accounts/views/taskinvite.view.modal.tab.html',
			controller: 'TaskInviteViewModalInstanceCtrl',
			controllerAs: 'TVMI',
			resolve: {
				TaskInvite: function () {
					//	console.log("dadada",	acc.taskInvitation[index]);
					return acc.taskInvitation[index];
				},
				DefaultCurrency: function () {
					return $scope.DefaultCurrency;
				},
				getsettings: function () {
					return acc.getsettings;
				}
			}
		});
		modalInstance.result.then(function (data) {
		}, function () {
		});
	};

	//acc.taskListCurrentPage = 1;
	acc.taskListitemsPerPage = 2;
	acc.taskListtotalItem = 0;
	acc.taskList = [];
	acc.taskinfobyid = [];
	acc.getTaskListResponse = false;

	acc.taskinfobyid = function taskinfobyid(taskid) {
		accountService.gettaskinfobyid(taskid).then(function (response) {
			if (response.length > 0) {

			}
		});
	}

	acc.GetTaskList = function GetTaskList(status, page) {
		//console.log('status, page',status, page);
		acc.status = status;
		acc.taskList = [];
		acc.getTaskListResponse = false;
		acc.taskListtotalItem = 0;
		if (page == undefined) {
			//console.log('status, page');
			acc.taskListCurrentPage = 1;
		} else {
			acc.taskListCurrentPage = page;
		}
		//acc.taskListCurrentPage = page;
		accountService.taskListService(acc.user._id, status, acc.taskListCurrentPage, acc.taskListitemsPerPage).then(function (response) {
			if (response.length > 0) {
				acc.taskList = response[0].TaskDetails;
				acc.taskListtotalItem = response[0].count;
				accountService.getTaskDetails(acc.user._id).then(function (respo) {
					acc.taskList.review = respo;
				});
			}
			acc.getTaskListResponse = true;
		});
	}


	acc.updateTaskDetails = function (index, status) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'app/site/modules/accounts/views/task-confirm.modal.html',
			controller: 'TaskReviewModalInstanceCtrl',
			controllerAs: 'TRM',
			resolve: {
				TaskDetails: function () {
					return acc.taskList[index];
				}
			}
		});

		modalInstance.result.then(function (data) {
			user = acc.user._id;
			task = acc.taskList[index]._id;
			type = 'user';
			if (acc.taskList.length > 0 && angular.isDefined(acc.taskList[index]._id)) {

				accountService.updateTask(acc.taskList[index]._id, status).then(function (response) {
					acc.taskList.splice(index, 1);
				}, function (err) {
				});
			}

		}, function () {
		});
	}

	acc.TaskDetailsViewModal = function (index) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'app/site/modules/accounts/views/taskdetails.view.modal.tab.html',
			controller: 'TaskDetailsViewModalInstanceCtrl',
			controllerAs: 'TDVMI',
			resolve: {
				TaskDetails: function () {
					return acc.taskList[index];
				},
				DefaultCurrency: function () {
					return $scope.DefaultCurrency;
				}
			}
		});
		modalInstance.result.then(function (data) {
		}, function () {
		});
	};

	acc.TaskDetailsViewModalforstatus = function (index) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'app/site/modules/accounts/views/taskdetailsforstatus.modal.tab.html',
			controller: 'TaskDetailsViewforstatusModalInstanceCtrl',
			controllerAs: 'TDVSMI',
			resolve: {
				TaskDetails: function () {
					return acc.taskList[index];
				},
				defaultcurrency: function () {
					return $scope.DefaultCurrency;
				}

			}
		});
		modalInstance.result.then(function (data) {
		}, function () {
		});
	};

	acc.reviewModal = function (data, task) {

		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'app/site/modules/accounts/views/reviewdetails.view.modal.tab.html',
			controller: 'reviewModelCtrl',
			controllerAs: 'RMC',
			resolve: {
				data: function () {
					return data;
				},
				role: function () {
					return acc.user.role;
				}
			}
		});
	}

	acc.TaskDetailsViewModal = function (index) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'app/site/modules/accounts/views/taskdetails.view.modal.tab.html',
			controller: 'TaskDetailsViewModalInstanceCtrl',
			controllerAs: 'TDVMI',
			resolve: {
				TaskDetails: function () {
					return acc.taskList[index];
				},
				DefaultCurrency: function () {
					return $scope.DefaultCurrency;
				}

			}
		});
		modalInstance.result.then(function (data) {
		}, function () {
		});
	};

	acc.TaskDetailsIgnoreModal = function (id, status) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'app/site/modules/accounts/views/task-cancel.modal.tab.html',
			controller: 'TaskDetailsIgnoreModalInstanceCtrl',
			controllerAs: 'TDIMI',
			resolve: {
				userid: function () {
					return id;
				},
				status: function () {
					return status;
				},
				cancelreason: function () {
					return acc.getcancelreason;
				}

			}
		});

		modalInstance.result.then(function (taskignoredata) {
			accountService.usercanceltask(taskignoredata).then(function (response) {
				//acc.GetTaskList("assigned");
				acc.taskCurrentPage = 1;
				acc.GetTaskList('cancelled');
				acc.tabFourActive = true;
			}, function () {
				if (err.msg) {
					toastr.error(err.msg);
				} else {
					$translate('UNABLE TO SAVE YOUR DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
				}
			});
		}, function () {
		});

	};

	acc.ignoreTask = function (id, status) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'app/site/modules/accounts/views/task-cancel.modal.tab.html',
			controller: 'TaskDetailsIgnoreModalInstanceCtrl',
			controllerAs: 'TDIMI',
			resolve: {
				userid: function () {
					return id;
				},
				status: function () {
					return status;
				},
				cancelreason: function () {
					return acc.getcancelreason;
				}
			}
		});
		modalInstance.result.then(function (taskignoredata) {
			accountService.ignoreTask(taskignoredata).then(function (response) {
				acc.currentPage = 1;
				acc.getTaskDetailsByStaus('cancelled');
				acc.tabFourActive = true;
			}, function () {
				if (err.msg) {
					$scope.addAlert(err.msg);
				} else {
					$translate('UNABLE TO SAVE YOUR DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
				}
			});
		}, function () {
		});
	};

	acc.taskerconfirmtask = function (id, status) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'app/site/modules/accounts/views/tasker-taskconfirm.modal.tab.html',
			controller: 'TaskDetailsconfirmModalInstanceCtrl',
			controllerAs: 'TDCMI',
			resolve: {
				taskid: function () {
					return id;
				},
				status: function () {
					return status;
				}
			}
		});
		modalInstance.result.then(function (taskconfirmdata) {
			accountService.taskerconfirmTask(taskconfirmdata).then(function (response) {
				if (response.error) {
					toastr.error(response.error);
				}
				acc.getTaskDetailsByStaus("assigned");
			}, function (err) {
				if (err.msg) {
					toastr.error(err.msg);
				} else {
					$translate('UNABLE TO SAVE YOUR DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
				}
			});
		},
			function () {
			});
	};

	//Availability Tab
	if (acc.user.role == 'tasker') {
		acc.availability = {};
		acc.availability.days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		acc.workingDays = [{ day: "Sunday", hour: { "morning": false, "afternoon": false, "evening": false }, not_working: true }, { day: "Monday", hour: { "morning": false, "afternoon": false, "evening": false }, not_working: true }, { day: "Tuesday", hour: { "morning": false, "afternoon": false, "evening": false }, not_working: true }, { day: "Wednesday", hour: { "morning": false, "afternoon": false, "evening": false }, not_working: true }, { day: "Thursday", hour: { "morning": false, "afternoon": false, "evening": false }, not_working: true }, { day: "Friday", hour: { "morning": false, "afternoon": false, "evening": false }, not_working: true }, { day: "Saturday", hour: { "morning": false, "afternoon": false, "evening": false }, not_working: true }];
		angular.forEach(acc.workingDays, function (workingDays, key) {
			angular.forEach(acc.user.working_days, function (UserWorkingdays) {
				if (UserWorkingdays.day == workingDays.day) {
					UserWorkingdays.not_working = false;
					acc.workingDays[key] = UserWorkingdays;
				}
			})
		})

		acc.availabilityModal = function (day) {
			var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'app/site/modules/accounts/views/availability.modal.tab.html',
				controller: 'AvailabilityModalInstanceCtrl',
				controllerAs: 'AAM',
				resolve: {
					data: function () {
						return { 'day': day, 'days': acc.availability.days };
					},
					workingDays: function () {
						return acc.workingDays;
					}
				}
			});
			modalInstance.result.then(function (data) {
				acc.workingDays[data.index] = data.working_day;
				acc.user.working_days = $filter('filter')(acc.workingDays, { "not_working": false });
			}, function () {
			});
		};

		acc.saveAvailability = function () {
			accountService.saveAvailability(acc.user).then(function (response) {
				$translate('UPDATED SUCCESSFULLY').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
			}, function (err) {
				if (err.msg) {
					toastr.error(err.msg);
				} else {
					$translate('UNABLE TO SAVE YOUR DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
				}
			});
		}
	}


	acc.availabilityChange = function (value) {
		acc.data = {};
		if (value == false) {
			acc.data.availability = 0;
		} else {
			acc.data.availability = 1;
		}
		acc.data._id = acc.user._id;
		accountService.updateAvailability(acc.data).then(function (response) {
			$translate('EMPLOYEE AVAILABILITY UPDATED SUCCESSFULLY').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
		}, function (err) {
			if (err.msg) {
				toastr.error(err.msg);
			} else {
				$translate('UNABLE TO SAVE YOUR DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
			}
		});
	};

	acc.deactivate = function (deactivateAcc) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'app/site/modules/accounts/views/deactivate.modal.tab.html',
			controller: 'DeactivateCtrl',
			controllerAs: 'DECM',
			resolve: {
				user: function () {
					return acc.user;
				}
			}
		});

		modalInstance.result.then(function (userid) {
			accountService.deactivateAccount(userid).then(function (response) {
				$translate('UPDATED SUCCESSFULLY').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
			}, function () {
				if (err.msg) {
					toastr.error(err.msg);
				} else {
					$translate('UNABLE TO SAVE YOUR DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
				}
			});
		});

	};


	//review tab
	acc.reviewListCurrentPage = 1;
	acc.reviewListitemsPerPage = 5;
	acc.reviewListtotalItem = 0;
	acc.getreviewdetails = function (status) {
		acc.index = 0;
		accountService.getReview(acc.user._id, acc.reviewListCurrentPage, acc.reviewListitemsPerPage, acc.user.role).then(function (respo) {
			$scope.truefalse = "true";
			acc.reviewListtotalItem = respo.count;
			acc.getReview = respo.result;
			acc.finalResult = [];
			angular.forEach(acc.getReview, function (value, key) {
				if (value.task) {
					acc.finalResult.push(value);
				}
			});

		});
	}

	acc.getuserreviewdetails = function (status) {
		acc.index = 1;
		accountService.getuserReview(acc.user._id, acc.reviewListCurrentPage, acc.reviewListitemsPerPage, acc.user.role).then(function (respo) {
			$scope.truefalse = "true";
			acc.reviewListtotalItem = respo.count;
			acc.getReview = respo.result;
			acc.finalResult = [];
			angular.forEach(acc.getReview, function (value, key) {
				if (value.task) {
					acc.finalResult.push(value);
				}
			});
		});
	}

	//tasker table

	acc.accountMode = true;
	acc.saveTaskerAccount = function saveTaskerAccount(isValid) {
		if (isValid) {
			accountService.saveTaskerAccount(acc.user).then(function (response) {
				$translate('UPDATED SUCCESSFULLY').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
				$location.hash('editaccountdiv');
				$anchorScroll();
			}, function (err) {
				if (err.msg) {
					toastr.error(err.msg);
				} else {
					$translate('UNABLE TO SAVE YOUR DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });

				}
			});
		} else {
			$translate('PLEASE ENTER THE VALID DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
		}
	};

	acc.password = {};
	acc.password.userId = acc.user._id;
	acc.saveTaskerPassword = function saveTaskerPassword(isvalid) {
		if (isvalid) {
			accountService.saveTaskerPassword(acc.password).then(function (response) {
				$translate('UPDATED SUCCESSFULLY').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
				$state.go('account');
			}, function (err) {
				if (err.msg) {
					toastr.error(err.msg);
				} else {
					$translate('PLEASE TYPE A DIFFERENT PASSWORD').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
				}
			});
		} else {
			$translate('FORM IS INVALID').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
		}

	};
	acc.deactivateTasker = function (deactivateTaskerAcc) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'app/site/modules/accounts/views/deactivate.modal.tab.html',
			controller: 'DeactivateCtrl',
			controllerAs: 'DECM',
			resolve: {
				user: function () {
					return acc.user;
				}
			}
		});

		modalInstance.result.then(function (userid) {
			accountService.deactivateTaskerAccount(userid).then(function (response) {
				$translate('UPDATED SUCCESSFULLY').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
			}, function () {
				if (err.msg) {
					toastr.error(err.msg);
				} else {
					$translate('UNABLE TO SAVE YOUR DATA').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
				}
			});
		});
	};
}

angular.module('quickRabbit.accounts').controller('TaskPayModalInstanceCtrl', function ($scope, $filter, $uibModalInstance, Taskinfobyid, updatingstatus) {

	var tpm = this;
	var total = 0;
	tpm.user = Taskinfobyid.taskdata[0].user;
	tpm.taskid = Taskinfobyid.taskdata[0]._id;
	tpm.status = updatingstatus;
	tpm.bookingid = Taskinfobyid.taskdata[0].booking_id;
	tpm.taskname = Taskinfobyid.taskdata[0].category.name;
	tpm.admincommision = Taskinfobyid.settingsdata.settings.admin_commission;
	tpm.servicetax = Taskinfobyid.settingsdata.settings.service_tax;
	tpm.minimumamount = Taskinfobyid.settingsdata.settings.minimum_amount;
	tpm.taskdescription = Taskinfobyid.taskdata[0].task_description;
	tpm.hourlyrate = $filter('filter')(Taskinfobyid.taskdata[0].tasker.taskerskills, { "childid": Taskinfobyid.taskdata[0].category._id });

	tpm.totalhour1 = function () {

		tpm.total = tpm.hourlyrate[0].hour_rate;

		if (tpm.totalhour > 1) {
			tpm.newtotal = ((parseInt(tpm.minimumamount)) + (parseInt(tpm.total) * (parseInt(tpm.totalhour) - 1)));
			tpm.taxamount = parseInt(tpm.newtotal) * (parseInt(tpm.servicetax) / 100);
			tpm.adminamount = parseInt(tpm.newtotal) * (parseInt(tpm.admincommision) / 100);
			tpm.commisionamount = parseInt(tpm.adminamount) + parseInt(tpm.taxamount);
			tpm.grandtotal = parseInt(tpm.newtotal) + parseInt(tpm.commisionamount);
		}
		else if (tpm.totalhour <= 1) {
			tpm.newtotal = parseInt(tpm.minimumamount);
			tpm.taxamount = parseInt(tpm.minimumamount) * (parseInt(tpm.servicetax) / 100);
			tpm.adminamount = parseInt(tpm.minimumamount) * (parseInt(tpm.admincommision) / 100);
			tpm.commisionamount = parseInt(tpm.adminamount) + parseInt(tpm.taxamount);
			tpm.grandtotal = parseInt(tpm.minimumamount) + parseInt(tpm.commisionamount);
		} else {
			tpm.grandtotal = parseInt(tpm.total);
		}
	};

	tpm.ok = function () {
		$uibModalInstance.close(tpm);
	};

	tpm.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

});




angular.module('quickRabbit.accounts').controller('DeactivateCtrl', function ($uibModalInstance, user, $state) {
	var decm = this;
	decm.user = user;
	decm.userid = decm.user._id;
	decm.ok = function () {
		$uibModalInstance.close(decm.userid);
		$state.go('logout');
	};
	decm.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});

angular.module('quickRabbit.accounts').controller('CategoriesModalInstanceCtrl', function (accountService, $uibModalInstance, experiences, user, toastr, categories, category, defaultcurrency, $translate) {

	var acm = this;
	if (category) {
		acm.role = 'Edit';
	}
	else {
		acm.role = 'New';
	}
	acm.user = user;
	acm.defaultcurrency = defaultcurrency;

	acm.categories = categories;
	acm.experiences = experiences;
	acm.category = acm.categories.filter(function (obj) {

		return obj._id === category;
	})[0];

	acm.selectedCategoryData = {};
	acm.selectedCategoryData.skills = [];
	acm.selectedCategoryData.hour_rate = 0
	if (acm.category) {
		acm.mode = 'Edit';
	} else {
		acm.mode = 'Add';
	}

	for (var i = 0; i < acm.user.taskerskills.length; i++) {
		if (acm.user.taskerskills[i].childid == category) {
			acm.selectedCategoryData = acm.user.taskerskills[i];
		}
	}

	acm.selectedCategoryData.userid = acm.user._id;
	acm.onChangeCategory = function (category) {
		acm.category = acm.categories.filter(function (obj) {
			return obj._id === category;
		})[0];
	};

	acm.onChangeCategoryChild = function (category) {
		accountService.getChild(category).then(function (response) {
			acm.MinimumAmount = response.commision;
		});
		acm.category = acm.user.taskerskills.filter(function (obj) {
			if (obj.childid === category) {
				$translate('ALREADY THE CATEGORY IS EXISTS').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
			}
			else {
				return obj._id === category;
			}
		})[0];
	};

	if (acm.selectedCategoryData.childid) {
		accountService.getChild(acm.selectedCategoryData.childid).then(function (response) {
			acm.MinimumAmount = response.commision;
		});
	}


	acm.selectedCategoryData.hour_rate = parseFloat((acm.selectedCategoryData.hour_rate * acm.defaultcurrency[0].value).toFixed(2));
	acm.ok = function (valid) {
		if (valid) {
			$uibModalInstance.close(acm.selectedCategoryData);
		} else {
			$translate('FORM IS INVALID').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
		}
	};
	acm.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});

angular.module('quickRabbit.accounts').controller('AvailabilityModalInstanceCtrl', function ($uibModalInstance, data, workingDays) {
	var aam = this;
	aam.day = data.days[data.day];
	aam.index = data.day;
	aam.working_day = workingDays[data.day];

	aam.ok = function (working_day, index) {
		if (aam.working_day.hour.morning == true || aam.working_day.hour.afternoon == true || aam.working_day.hour.evening == true) {
			aam.working_day.not_working = false;
		} else {
			aam.working_day.not_working = true;

		}
		var data = { 'working_day': working_day, 'index': index };
		$uibModalInstance.close(data);
	};

	aam.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});

angular.module('quickRabbit.accounts').controller('TaskInviteViewModalInstanceCtrl', function ($uibModalInstance, TaskInvite, DefaultCurrency, getsettings) {
	var tvmi = this;
	tvmi.TaskInvite = TaskInvite;
	tvmi.DefaultCurrency = DefaultCurrency;
	tvmi.getsettings = getsettings;
	tvmi.ok = function (working_day, index) {
		var data = {};
		$uibModalInstance.close(data);
	};
	tvmi.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});

angular.module('quickRabbit.accounts').controller('TaskDetailsViewModalInstanceCtrl', function ($uibModalInstance, TaskDetails, DefaultCurrency) {
	var tdvmi = this;
	tdvmi.TaskDetails = TaskDetails;
	tdvmi.DefaultCurrency = DefaultCurrency;

	tdvmi.taskdescription = TaskDetails.task_description;
	tdvmi.ok = function () {
		$uibModalInstance.close();
	};
	tdvmi.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});

angular.module('quickRabbit.accounts').controller('TaskDetailsViewforstatusModalInstanceCtrl', function ($uibModalInstance, TaskDetails, defaultcurrency) {
	var tdvsmi = this;
	tdvsmi.defaultcurrency = defaultcurrency;
	tdvsmi.TaskDetails = TaskDetails;
	var a = TaskDetails.invoice.worked_hours;
	var hours = Math.trunc(a / 60);
	var minutes = a % 60;
	if (hours == 0) {
		tdvsmi.Task_time = minutes + " mins";
	} else {
		tdvsmi.Task_time = hours + " hours " + minutes + " mins";
	}

	tdvsmi.taskdescription = TaskDetails.task_description;
	tdvsmi.ok = function () {
		$uibModalInstance.close();
	};
	tdvsmi.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});

angular.module('quickRabbit.accounts').controller('TaskDetailsIgnoreModalInstanceCtrl', function ($translate, $uibModalInstance, $state, userid, status, toastr, cancelreason) {
	var tdimi = this;
	tdimi.userid = userid;
	tdimi.taskstatus = status;
	tdimi.cancelreason = cancelreason;
	tdimi.other = 0;
	tdimi.ok = function (data) {
		if (data.reason) {
			$uibModalInstance.close(tdimi);

		} else {
			$translate('REASON FIELD IS EMPTY').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
		}
	};
	tdimi.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
	tdimi.otherclick = function (other) {
		tdimi.other = 1;
	};
	tdimi.click = function (other) {
		tdimi.other = 0;
	};


});

angular.module('quickRabbit.accounts').controller('WalletRechargeModal', function ($uibModalInstance, $state, $translate, Rechargeamount, toastr) {
	var wrm = this;
	wrm.rechargeamount = Rechargeamount;
	var walletamount = wrm.rechargeamount.data.replace(/,/g, '');
	var currencyvalue = wrm.rechargeamount.currencyvalue;
	var result = parseFloat(walletamount) / parseFloat(currencyvalue);
	var walletamount = "";
	wrm.walletamount = result.toFixed(2);
	wrm.ok = function (isValid) {
		// console.log("data",isValid);
		if (isValid == true) {
			$uibModalInstance.close(wrm);
		}
		else {
			$translate('FORM IS INVALID').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });

		}

	};
	wrm.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});

angular.module('quickRabbit.accounts').controller('TaskDetailsconfirmModalInstanceCtrl', function ($uibModalInstance, $state, sweet, taskid, status) {
	var tdcmi = this;
	tdcmi.taskid = taskid;
	tdcmi.taskstatus = status;
	tdcmi.ok = function () {
		$uibModalInstance.close(tdcmi);
	};
	tdcmi.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

});

angular.module('quickRabbit.accounts').controller('DeleteCategoriesModalInstanceCtrl', function ($uibModalInstance, user, category) {
	var dacm = this;
	dacm.category = category;
	dacm.user = user;
	var categoryinfo = {};
	categoryinfo.userid = user._id;
	categoryinfo.categoryid = category;
	dacm.ok = function () {
		$uibModalInstance.close(categoryinfo);
	};
	dacm.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});

angular.module('quickRabbit.accounts').controller('DisputeReviewModalInstanceCtrl', function ($uibModalInstance, TaskDetails, accountService, status, $state) {
	var dnm = this;
	dnm.indexValue = TaskDetails._id;
	dnm.ststusvalue = status;
	dnm.ok = function () {
		accountService.disputeUpdateTask(dnm.indexValue, dnm.ststusvalue).then(function (response) {
			$state.reload();
		}, function (err) {
		});
		$uibModalInstance.close(dnm.review);
	};
	dnm.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

});

angular.module('quickRabbit.accounts').controller('TaskReviewModalCtrl', function ($uibModalInstance, TaskDetails) {
	var trem = this;
	trem.TaskDetails = TaskDetails;
	trem.ok = function () {
		$uibModalInstance.close(trem.review);
	};
	trem.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

});

angular.module('quickRabbit.accounts').controller('TaskReviewModalSave', function ($uibModalInstance) {
	var trem = this;
	trem.ok = function () {

		$uibModalInstance.close(trem.review);
	};
	trem.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

});

angular.module('quickRabbit.accounts').controller('TaskTranscationViewModal', function ($scope, task, $uibModalInstance, accountService, TaskDetails, defaultcurrency, getsettings) {
	var trems = this;
	trems.TaskDetails = TaskDetails;
	trems.task = task;

	trems.defaultcurrency = defaultcurrency;
	trems.getsettings = getsettings;
	trems.downloadPdf = function () {
		accountService.downloadPdf(trems.TaskDetails.bookingid).then(function (response) {
		});
	}
	trems.ok = function () {
		$uibModalInstance.close(trems.review);
	};
	trems.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

});


angular.module('quickRabbit.accounts').controller('TaskUserTranscationViewModal', function ($scope, task, $uibModalInstance, accountService, TaskDetails, defaultcurrency, getsettings) {
	var trems = this;
	trems.TaskDetails = TaskDetails;
	trems.task = task;
	trems.defaultcurrency = defaultcurrency;
	trems.getsettings = getsettings;
	trems.downloadPdf = function () {
		accountService.downloadPdf(trems.TaskDetails.bookingid).then(function (response) {
		});

	}
	trems.ok = function () {

		$uibModalInstance.close(trems.review);
	};
	trems.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

});


angular.module('quickRabbit.accounts').controller('addReviewModal', function ($uibModalInstance, TaskDetails, $scope, $state) {
	var arm = this;
	arm.user = TaskDetails.user;
	arm.tasker = TaskDetails.tasker._id;
	arm.task = TaskDetails._id;
	arm.type = 'user';
	arm.ok = function () {
		$uibModalInstance.close(arm);
		$state.go('account');
	};
	arm.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

});

angular.module('quickRabbit.accounts').controller('reviewModelCtrl', function ($uibModalInstance, data, role) {
	var rmc = this;
	rmc.reviewData = data;
	rmc.role = role;
	rmc.ok = function () {
		$uibModalInstance.close(rmc);
	};
	rmc.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});
