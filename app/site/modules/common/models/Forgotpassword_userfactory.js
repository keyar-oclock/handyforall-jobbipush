var app = angular.module('Authentication');
app.factory('ForgotpwduserService', ForgotpwduserService);
function ForgotpwduserService($http, $q) {
    var ForgotpwduserService = {
        saveUserInfopwd: saveUserInfopwd
    };
    return ForgotpwduserService;
    function saveUserInfopwd(data) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: '/site/saveforgotpwduser',
            data: { 'data': data }
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }
}
