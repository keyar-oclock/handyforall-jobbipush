angular.module('quickRabbit.messages').controller('chatCtrl', chatCtrl);

chatCtrl.$inject = ['ChatServiceResolve', '$state', 'toastr', '$filter', 'AuthenticationService', 'TaskProfileResolve', 'MainService', '$stateParams', 'socket', '$scope', '$rootScope', 'TaskServiceResolve', 'TaskService', 'CurrentuserResolve', '$translate'];
function chatCtrl(ChatServiceResolve, $state, toastr, $filter, AuthenticationService, TaskProfileResolve, MainService, $stateParams, socket, $scope, $rootScope, TaskServiceResolve, TaskService, CurrentuserResolve, $translate) {
    var chat = this;
    var user = AuthenticationService.GetCredentials();
    chat.currentusertype = CurrentuserResolve.user_type;

    if (chat.currentusertype == 'user') {
        MainService.getCurrentUsers(user.currentUser.username).then(function (result) {
            chat.currentUserData = result[0];
            //  console.log("format",chat.currentUserData);

        }, function (error) {
            $translate('INIT CURRENT DATA ERROR').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });
            //  toastr.error('init current data error');
        });
    }
    if (chat.currentusertype == 'tasker') {
        MainService.getCurrentTaskers(user.currentUser.username).then(function (result) {
            chat.currentUserData = result[0];
            //    console.log("format",chat.currentUserData);
        }, function (error) {
            //toastr.error('init current data error');
            $translate('INIT CURRENT DATA ERROR').then(function (headline) { toastr.error(headline); }, function (translationId) { toastr.error(headline); });

        });
    }

    chat.taskerDetails = TaskProfileResolve;
    chat.currentUser = user.currentUser.user_id;

    chat.data = ChatServiceResolve;

    if (chat.data) {
        var data = {};
        data.task = $stateParams.task;
        data.user = $stateParams.user;
        data.tasker = $stateParams.tasker;
        data.type = user.currentUser.user_type;
        socket.emit('message status', data);
    }

    chat.messages = ChatServiceResolve.messages || [];
    chat.taskinfo = TaskServiceResolve;
    console.log("taskinfo",chat.taskinfo);



    //  chat.taskinfo.task_date = chat.taskinfo.task_date

    $rootScope.$emit('notification', { user: $rootScope.userId, type: $rootScope.usertype });

    chat.typing = {};
    chat.typing.status = false;

    if (user.currentUser.user_type == 'tasker') {
        chat.typing.message = 'Employer is typing. . .';
    } else if (user.currentUser.user_type == 'user') {
        chat.typing.message = 'Employee is typing. . .';
    }

    chat.taskinfo.amount = $filter('filter')(chat.taskerDetails.taskerskills, { "childid": chat.taskinfo.category._id })[0].hour_rate;

    chat.send = function saveChat(message) {

        if (user.currentUser.user_type == 'tasker') {
            var data = { 'user': $stateParams.user, 'tasker': $stateParams.tasker, 'message': message, 'task': $stateParams.task, 'from': chat.currentUser };
        } else if (user.currentUser.user_type == 'user') {
            var data = { 'user': $stateParams.user, 'tasker': $stateParams.tasker, 'message': message, 'task': $stateParams.task, 'from': chat.currentUser };
        }
        if (message) {
            socket.emit('new message', data);
        }
        $scope.message = '';
    };

    chat.ontyping = function ontyping(message) {
        var data = {};
        if (user.currentUser.user_type == 'tasker') {
            data.from = $stateParams.tasker;
            data.to = $stateParams.user;
        } else if (user.currentUser.user_type == 'user') {
            data.from = $stateParams.user;
            data.to = $stateParams.tasker;
        }

        socket.emit('start typing', data);
        lastTypingTime = (new Date()).getTime();

        setTimeout(function () {
            var typingTimer = (new Date()).getTime();
            var timeDiff = typingTimer - lastTypingTime;
            if (timeDiff >= 400) {
                socket.emit('stop typing', data);
            }
        }, 400);
    };

    chat.confirmatask = function confirmatask(message) {
        console.log("message", chat);

        chat.taskinfo.status = message;
        chat.taskinfo.tasker = $stateParams.tasker;
        chat.taskinfo.hourly_rate = chat.taskinfo.amount;

        /*chat.taskinfo.billing_address = {
            'zipcode': chat.currentUserData.address.zipcode || "",
            'country': chat.currentUserData.address.country || "",
            'state': chat.currentUserData.address.state || "",
            'city': chat.currentUserData.address.city || "",
            'line2': chat.currentUserData.address.line2 || "",
            'line1': chat.currentUserData.address.line1 || ""
        };*/

        chat.taskinfo.invoice = {
            'amount': {
                "minimum_cost": chat.taskinfo.category.commision,
                "task_cost": chat.taskinfo.category.commision,
                "total": chat.taskinfo.category.commision,
                "grand_total": chat.taskinfo.category.commision
            }
        };

        chat.taskinfo.booking_information = {
            //'service_id': chat.taskinfo.category.parent,
            'service_type': chat.taskinfo.category.name,
            'work_type': chat.taskinfo.category.name,
            'work_id': chat.taskinfo.category._id,
            'instruction': chat.taskinfo.task_description,
            'booking_date': '',
            'reach_date': '',
            'est_reach_date': '',
            'job_email': chat.taskerDetails.email,
            'location': 'Chennai',
            'user_latlong': {
                'lon': chat.taskinfo.location.log,
                'lat': chat.taskinfo.location.lat
            }
        };
        chat.taskinfo.card = { number: '', exp_month: '', exp_year: '', cvc: '' };
        TaskService.confirmtask(chat.taskinfo).then(function (result) {
            //  toastr.success('Job created successfully...');
            console.log("chat.taskinfo", chat.taskinfo)
            console.log(chat.taskinfo.status)
            if (chat.taskinfo.status == 2) {
                $translate('JOB ACCEPTED SUCCESSFULLY').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
            }
            else {
                $translate('REQUEST HAS BEEN SENT TO EMPLOYEE SUCCESSFULLY').then(function (headline) { toastr.success(headline); }, function (translationId) { toastr.success(headline); });
            }
            $state.go('landing', { reload: false });
        }, function (error) {
            toastr.error(error);
        });
    };

    socket.on('webupdatechat', function (data) {
        /*
        if (data.messages[0].from != user.currentUser.user_id) {
            data.currentuserid = user.currentUser.user_id;
            data.usertype = chat.currentusertype;

            socket.emit('message status', data);
        }
        */

        chat.messages.push(data.messages[0]);
        setTimeout(function () {
            $("#chatscroll").scrollTop($("#chatscroll").scrollTop() + $("#chatscroll .chat-cnt:last").position().top)
        }, 0);
    });

    socket.on('single message status', function (data) {
        chat.messages = chat.messages.map(function (message) {
            if (message._id == data.messages[0]._id) {
                var usertype = chat.currentusertype;
                if (usertype == 'user') {
                    message.tasker_status = 2;
                } else if (usertype == 'tasker') {
                    message.user_status = 2;
                }
            }
            return message;
        });
    });

    socket.on('message status', function (data) {
        chat.messages = data;
    });

    socket.on('start typing', function (data) {
        chat.typing.status = true;
    });

    socket.on('stop typing', function (data) {
        chat.typing.status = false;
    });

}
