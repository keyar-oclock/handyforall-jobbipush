var app = angular.module('quickRabbit.category');
app.factory('PageService', PageService);

function PageService($http, $q) {
    var PageService = {
        getpage: getpage,
        // getcategoryList: getcategoryList
    };

    return PageService;

    function getpage(data) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: '/site/pages/getpage',
            data: data
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    // function getcategoryList(data, page, itemsPerPage) {
        // var deferred = $q.defer();
        // $http({
            // method: 'GET',
            // url: '/site/category/getCategoryList'
        // }).success(function (data) {
            // deferred.resolve(data);
        // }).error(function (err) {
            // deferred.reject(err);
        // });
        // return deferred.promise;
    // }

}
