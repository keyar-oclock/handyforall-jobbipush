angular.module('quickRabbit.page')
    .controller('pagesCtrl', pagesCtrl);

pagesCtrl.$inject = ['$scope', '$rootScope', '$location', '$stateParams', 'PagesserviceResolve', 'PageService', '$state', '$sce', '$translate', 'ngMeta'];
function pagesCtrl($scope, $rootScope, $location, $stateParams, PagesserviceResolve, PageService, $state, $sce, $translate, ngMeta) {
	var pac = this;

	if (PagesserviceResolve.seo) {
		ngMeta.setTitle(PagesserviceResolve.seo.title);
		ngMeta.setTag('description', PagesserviceResolve.seo.description);
		ngMeta.setTag('keyword', PagesserviceResolve.seo.keyword);
	}

	if (PagesserviceResolve) {
		pac.data = PagesserviceResolve;
	} else {
		$state.go('404');
		return;
	}

	$scope.html = pac.data.description;
	$scope.trustedHtml = $sce.trustAsHtml($scope.html);
	pac.getSubcategoryResponse = true;
	pac.getSubcategory = function () {
		pac.subcategory = [];
		if (angular.isDefined(pac.categoryDatails._id)) {
			pac.getSubcategoryResponse = false;
			PageService.getcategory(pac.categoryDatails.slug, pac.currentPage, pac.itemsPerPage).then(function (data) {
				if (data.response.ActiveCategory.subcategory.length > 0) {
					pac.subcategory = data.response.ActiveCategory.subcategory;

					pac.totalItems = data.response.ActiveCategory.totalsubcategory;
				}
			}, function (error) {
			});
		}
	};
}
