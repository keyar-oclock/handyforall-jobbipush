var app = angular.module('quickRabbit.tasks');

app.factory('TasksService',TasksService);

TasksService.$inject = ['$http','$q', 'Upload'];

function TasksService($http, $q, Upload){


    var TasksService = {
        getTasksList:getTasksList,
        getTasks:getTasks,
        getTasksExport:getTasksExport,
        save:save,
        getTransaction:getTransaction
    };

    return TasksService;

    function getTasksExport(){
        var deferred    = $q.defer();
        $http({
            method:'GET',
            url:'/tasks/taskexport'
        }).success(function(data){
            deferred.resolve(data);
        }).error(function(err){
            deferred.reject(err);
        });
        return deferred.promise;
    }

    function getTasksList(status,limit,skip,sort,search){
      var deferred = $q.defer();
      var data = {};
       data.status=status;
      data.sort = sort;
      data.search = search;
      data.limit = limit;
      data.skip = skip;

      $http({
          method: 'POST',
          url: '/tasks/list',
          data: data
      }).success(function (data) {
          deferred.resolve(data);
      }).error(function (err) {
          deferred.reject(err);
      });
      return deferred.promise;

    /*  var deferred    = $q.defer();
        $http({
            method:'GET',
            url:'/tasks/list/?sort='+sort+'&status='+status+'&search='+search+'&limit='+limit+'&skip='+skip
        }).success(function(data){
            deferred.resolve(data);
        }).error(function(err){
            deferred.reject(err);
        });
        return deferred.promise;*/
    }

    function getTasks(id){
		var data={id:id};
        var deferred    = $q.defer();
        $http({
            method:'POST',
            url:'/tasks/edit',
			data:data
        }).success(function(data){
            deferred.resolve(data);
        }).error(function(err){
            deferred.reject(err);
        });
        return deferred.promise;
    }

    function save(data){
        var deferred    = $q.defer();
		$http({
            method:'POST',
            url: '/tasks/save',
            data: data,
        }).then(function(data){
            deferred.resolve(data);
        },function(err){
            deferred.reject(err);
        });
        return deferred.promise;
    }
    function getTransaction(id){
        var deferred    = $q.defer();
        $http({
            method:'POST',
            url:'/tasks/getTransaction',
            data:{id:id}
        }).success(function(data){
            deferred.resolve(data);
        }).error(function(err){
            deferred.reject(err);
        });
        return deferred.promise;
    }

}
