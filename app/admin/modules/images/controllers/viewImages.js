angular.module('quickRabbit.images').controller('viewImagesCtrl', viewImagesCtrl);

viewImagesCtrl.$inject = ['ImagesServiceResolve', 'ImagesService', '$scope'];

function viewImagesCtrl(ImagesServiceResolve, ImagesService, $scope) {

    var tlc = this;
    tlc.permission = $scope.privileges.filter(function (menu) {
        return (menu.alias === "images");
    }).map(function (menu) {
        return menu.status;
    })[0];
    var layout = [
        {
            name: 'Image path name',
            variable: 'imagefor',
            template: '{{content.imagefor}}',
            sort: 1
        },
        {
            name: 'Image',
            template: '<img ng-src="{{content.image}}" alt="" class="size-50x50" style="border-radius: 0%;">'
        },
        {
            name: 'Status ',
            template: '<span ng-switch="content.status">' +
            '<span  ng-switch-when="1">Publish</span>' +
            '<span  ng-switch-when="2">UnPublish</span>' +
            '</span>'
        },
        {
            name: 'Actions',
            // template:'<button class="btn btn-info btn-rounded btn-ef btn-ef-5 btn-ef-5b" ng-if="options.permission.edit != false" ui-sref=app.images.addimage({action:"edit",id:content._id})><i class="fa fa-edit"></i> <span>Edit</span></button>'
            template: '<button class="btn btn-info btn-rounded btn-ef btn-ef-5 btn-ef-5b" ng-if="options.permission.edit != false" ui-sref=app.images.addimage({id:content._id})><i class="fa fa-edit"></i> <span>Edit</span></button>'
        }
    ];

    //var vsc = this;
    tlc.table = {};
    tlc.table.layout = layout;
    tlc.table.data = ImagesServiceResolve[0];
    tlc.table.count = ImagesServiceResolve[1] || 0;
    tlc.table.delete = {
        'permission': tlc.permission, service: '/images/deletebanner', getData: function (currentPage, itemsPerPage, sort, status, search) {
            var skip = (parseInt(currentPage) - 1) * itemsPerPage;
            ImagesService.getImagesList(itemsPerPage, skip, sort, status, search).then(function (respo) {
                tlc.table.data = respo[0];
                tlc.table.count = respo[1];
            });
        }
    };

}
