angular.module('quickRabbit.earnings').controller('viewearningsCtrl', viewearningsCtrl);

viewearningsCtrl.$inject = ['$scope', 'TaskerEarningReslove', 'toastr', 'EarningService', 'PaidEarningsServiceResolve', '$stateParams'];
function viewearningsCtrl($scope, TaskerEarningReslove, toastr, EarningService, PaidEarningsServiceResolve, $stateParams) {
    var encc = this;
    $scope.taskerId = $stateParams.id;

    encc.billingCycle = $stateParams.cycle;

    encc.eraninglist = TaskerEarningReslove[0];
    encc.payDetails = PaidEarningsServiceResolve;

    encc.amtWithTasker = encc.payDetails.cash.admin_commission + encc.payDetails.cash.servicetax;
    encc.amtWithAdmin = encc.payDetails.gateway.total - encc.payDetails.gateway.admin_commission;

    var amounttosettle = {};
    if (encc.amtWithAdmin > encc.amtWithTasker) {
        amounttosettle.by = "admin";
        amounttosettle.to = "tasker";
        amounttosettle.amount = encc.amtWithAdmin - encc.amtWithTasker;
    } else {
        amounttosettle.by = "tasker";
        amounttosettle.to = "admin";
        amounttosettle.amount = encc.amtWithTasker - encc.amtWithAdmin;
    }
    encc.amounttosettle = amounttosettle;

    var layout = [
        {
            name: 'Job id',
            template: '{{content.booking_id}}'
        },
        {
            name: 'Job',
            template: '{{content.booking_information.service_type }}'
        },
        {
            name: 'Total',
            template: '{{content.invoice.amount.total  | number:2}}'
        },
        {
            name: 'Service Tax',
            template: '{{content.invoice.amount.service_tax | number:2}}'
        },
        {
            name: 'Coupon Amount',
            template: '<span ng-if="content.invoice.amount.coupon"><span>{{content.invoice.amount.coupon  | number:2}}</span></span>' +
            '<span ng-if="!content.invoice.amount.coupon"><span> 0.00</span></span>'
        },
        {
            name: 'Grand Total',
            template: '{{content.invoice.amount.grand_total  | number:2}}'
        },
        {
            name: 'Admin Commission',
            template: '{{content.invoice.amount.admin_commission  | number:2}}'
        },
        {
            name: 'Employee',
            template: '{{content.invoice.amount.total - content.invoice.amount.admin_commission  | number:2}}'
        },
        {
            name: 'Payment Type',
            template: '{{content.payment_type}}'
        },
        {
            name: 'Paid',
            template: "{{content.payee_status == '1' ? 'Yes' : 'No' }}"
        },
        {
            name: 'Job Date',
            template: '{{content.booking_information.booking_date | clock : options.date}}'
        }
    ];
    encc.table = {};
    encc.table.module = 'earnings';
    encc.table.layout = layout;
    encc.table.data = TaskerEarningReslove[0];
    encc.table.count = encc.table.data.length;
    encc.table.delete = {
        'date': $scope.date,
        'permission': encc.permission,
        service: '/tasks/deletequestion',
        getData: function (currentPage, itemsPerPage, sort, search) {
            var skip = (parseInt(currentPage) - 1) * itemsPerPage;
            var data = {};
            data.tasker = $stateParams.id;
            data.cycle = $stateParams.cycle;
            EarningService.getTaskrearning(data, itemsPerPage, skip, sort, search).then(function (respo) {
                encc.table.data = respo[0];
            });
        }
    };

    encc.paytasker = function paytasker() {

        var data = {};
        data.tasker = encc.payDetails.tasker._id;
        data.invoice = {};
        data.invoice.cash = encc.payDetails.cash;
        data.invoice.gateway = encc.payDetails.gateway;
        data.invoice.total = encc.payDetails.total;
        data.task_count = encc.payDetails.count;
        data.payment = encc.amounttosettle;

        EarningService.updatepayee(data).then(function (response) {
            toastr.success('Payment Successfully Updated');
        }, function (err) {
            toastr.error('Unable to save your data');
        });
    };
}
