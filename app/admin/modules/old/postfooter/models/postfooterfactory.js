var app = angular.module('quickRabbit.postfooter');

app.factory('PostfooterService',PostfooterService);

PostfooterService.$inject = ['$http','$q', 'Upload'];

function PostfooterService($http, $q, Upload){


    var PostfooterService = {
        save:save,
        getPostfooter:getPostfooter,
        getPostFooterList:getPostFooterList
    };

    return PostfooterService;


    function getPostFooterList(limit,skip,sort,status,search){
        var deferred    = $q.defer();
        $http({
            method:'GET',
            url:'/postfooter/list/?sort='+sort+'&status='+status+'&search='+search+'&limit='+limit+'&skip='+skip
        }).success(function(data){
            deferred.resolve(data);
        }).error(function(err){
            deferred.reject(err);
        });
        return deferred.promise;
    }


    function getPostfooter(id){
        var data={id:id};
        var deferred    = $q.defer();
        $http({
            method:'POST',
            url:'/postfooter/edit',
            data:data
        }).success(function(data){
            deferred.resolve(data);
        }).error(function(err){
            deferred.reject(err);
        });
        return deferred.promise;
    }


    function save(data){
        var deferred    = $q.defer();
		Upload.upload({
            url: '/postfooter/save',
            data: data,
        }).then(function(data){
            deferred.resolve(data);
        },function(err){
            deferred.reject(err);
        });
        return deferred.promise;
    }


}
