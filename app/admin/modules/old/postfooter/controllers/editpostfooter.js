angular.module('quickRabbit.postfooter').controller('editPostfooterCtrl',editPostfooterCtrl);

editPostfooterCtrl.$inject = ['toastr','$state', 'PostfooterService', 'postfooterServiceResolve','$stateParams'];

function editPostfooterCtrl(toastr,$state, PostfooterService, postfooterServiceResolve,$stateParams){
    var epfc = this;

    epfc.editpostfooterData  = postfooterServiceResolve;

    if ($stateParams.id) {
        epfc.action = 'edit';
        epfc.breadcrumb = 'SubMenu.EDIT_POSTFOOTER';
    } else {
        epfc.action = 'add';
        epfc.breadcrumb = 'SubMenu.ADD_POSTFOOTER';
    }

    epfc.submit = function submit(isValid){
        if(isValid) {
            PostfooterService.save(epfc.editpostfooterData).then(function (response) {
                if (response.code == 11000) {
                    toastr.error('Postfooter Not Added Successfully');
                } else   if(response.data == "wrong"){
                    toastr.error('Your Credentials are gone please login again.....');
                }
                else {
                    toastr.success('Postfooter Added Successfully');
                    $state.go('app.postfooter.viewpostfooter');
                }
            }, function (err) {
                toastr.error('Your credentials are gone');
                for(var i=0;i<err.length;i++){
                    toastr.error('Your credentials are gone'+err[i].msg+'--'+err[i].param);
                }
            });
        }else{
            toastr.error('form is invalid');
        }

    };



}
