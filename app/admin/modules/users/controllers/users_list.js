angular.module('quickRabbit.users').controller('usersListCtrl', usersListCtrl);

usersListCtrl.$inject = ['usersServiceResolve', 'UsersService', '$scope', 'MainService'];

function usersListCtrl(usersServiceResolve, UsersService, $scope, MainService) {

    var tlc = this;
    tlc.permission = $scope.privileges.filter(function (menu) {
        return (menu.alias === "users");
    }).map(function (menu) {
        return menu.status;
    })[0];

    var layout = [
        {
            name: 'Username',
            template: '{{content.username}}',
            sort: 1,
            variable: 'username',
        },
        {
            name: 'Email',
            template: '{{content.email}}',
            sort: 1,
            variable: 'email',
        },
        {
            name: 'Last Login Date',
            template: '{{content.updatedAt | clock : options.date}}',
            sort: 1,
            variable: 'updatedAt'
        },
        {
            name: 'Actions',
            template: '<button class="btn btn-info btn-rounded btn-ef btn-ef-5 btn-ef-5b"  ng-if="options.permission.edit != false"  ui-sref="app.users.add({id:content._id})"><i class="fa fa-edit"></i> <span>Edit</span></button>' +
            '<button class="btn btn-danger btn-rounded btn-ef btn-ef-5 btn-ef-5b" ng-if="options.permission.delete != false"  ng-click="CCC.openDeleteModal(small, content, options)" ><i class="fa fa-trash"></i> <span>Delete</span></button>'
        }
    ];
    tlc.table = {};
    tlc.table.layout = layout;
    tlc.table.data = usersServiceResolve[0];
    tlc.table.count = usersServiceResolve[1] || 0;
    tlc.table.delete = {
        'permission': tlc.permission,
        'date': $scope.date,
        service: '/users/delete', getData: function (currentPage, itemsPerPage, sort, status, search) {
            if (currentPage >= 1) {
                var skip = (parseInt(currentPage) - 1) * itemsPerPage;
                UsersService.getAllUsers(itemsPerPage, skip, sort, status, search).then(function (respo) {
                    tlc.table.data = respo[0];
                    tlc.table.count = respo[1];

                });
            }
        }
    };
}
