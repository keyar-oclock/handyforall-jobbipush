# Handyforall Installation Guide

## Before You Begin

Before you begin we recommend you read about the basic building blocks that assemble a CodeIgniter application:
* AngularJS - Go through [AngularJS Official Website](https://angularjs.org) and proceed to their [Developer Guide](https://docs.angularjs.org/guide), which should help you understand AngularJS better.
* NodeJS - Go through [NodeJS Official Website](https://nodejs.org/en/) and need to leran it with this [DOCS](https://nodejs.org/en/docs/).
* MongoDB - Go through [MongoDB](https://www.mongodb.com/) and need to understand about NoSQL.

## Quick Installation on your Local

First, Install NodeJS(above v6.9.1), MongoDB(above v3.2) on your local. 
This scripts run under AngularJS v1.5.5.

If you installed old version, you need to update all it.  If not, you will get error messages.

Then, need to restore DB on MongoDB.  There is `db` folder on this script.

For instance, you can run this command on terminal.

For Windows

```
C:\mongodb\bin\mongorestore.exe -d handyforall C:\handyforall\db
```

For Ubuntu
```
mongorestore -d handyforall \var\www\handyforall\db
```
If you want to check DB with GUI, install [Robomongo](https://robomongo.org).

After that, update configration file(`config/config.json`) like this.

```
{
    "port": "3001",
    "mongodb": {
        "host": "localhost",
        "port": "27017",
        "database": "handyforall"
    },
    "directory": {
        "users": "./uploads/images/users/",
        "taskers": "./uploads/images/tasker/"
    },
    "default_image": {
        "users": "",
        "taskers": "",
        "category": ""
    }
}
```

Done. You can run this script by running `node app.js`.

If you want to run this script with PM2, you can do it easily.

```
sudo npm install -g pm2
pm2 start app.js
```

In your broswer, visit `http://localhost:3001`. Now ADMINISTRATOR credential is `admin/Admin1@#`(USERNAME/PASSWORD).

If you are using Ubuntu, or want to run this script on NGINX, you can run by following this article- [How To Set Up a Node.js Application for Production on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-ubuntu-16-04).


## License

OurnameShop